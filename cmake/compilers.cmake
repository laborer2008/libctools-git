cmtAssertVariableNotEmpty(CT_LANG)

if (CT_LANG STREQUAL "C")
	cmtAssertVariableNotEmpty(CMAKE_C_COMPILER_ID)

	if ("${CMAKE_C_COMPILER_ID}" STREQUAL "Clang")
		set(COMMON_OPTIONS "${COMMON_OPTIONS} -std=c99")
	elseif ("${CMAKE_C_COMPILER_ID}" STREQUAL "GNU")
		set(COMMON_OPTIONS "${COMMON_OPTIONS} -std=c99")
	elseif ("${CMAKE_C_COMPILER_ID}" STREQUAL "MSVC")
		# TODO
	elseif ("${CMAKE_C_COMPILER_ID}" STREQUAL "Intel")
		set(COMMON_OPTIONS "${COMMON_OPTIONS} -std=c99")
	elseif ("${CMAKE_C_COMPILER_ID}" STREQUAL "Borland" OR "${CMAKE_C_COMPILER_ID}" STREQUAL "Embarcadero")
		# TODO
	elseif ("${CMAKE_C_COMPILER_ID}" STREQUAL "SDCC")
		set(COMMON_OPTIONS "${COMMON_OPTIONS} --std-c99")
	else()
		message(FATAL_ERROR "Unsupported compiler: ${CMAKE_C_COMPILER_ID}")
	endif()

	set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${COMMON_OPTIONS}")
elseif (CT_LANG STREQUAL "CXX")
	cmtAssertVariableNotEmpty(CMAKE_CXX_COMPILER_ID)

	if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
		execute_process(COMMAND ${CMAKE_CXX_COMPILER} -dumpversion OUTPUT_VARIABLE CLANG_VERSION)

		# TODO: untested
		if (CLANG_VERSION VERSION_GREATER 2.9 OR CLANG_VERSION VERSION_EQUAL 2.9)
			set(COMMON_OPTIONS "${COMMON_OPTIONS} -std=c++11")
		else()
			set(COMMON_OPTIONS "${COMMON_OPTIONS} -std=c++0x")
		endif()
	elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
		execute_process(COMMAND ${CMAKE_CXX_COMPILER} -dumpversion OUTPUT_VARIABLE GCC_VERSION)

		if (GCC_VERSION VERSION_GREATER 4.7 OR GCC_VERSION VERSION_EQUAL 4.7)
			set(COMMON_OPTIONS "${COMMON_OPTIONS} -std=c++11")
		else()
			set(COMMON_OPTIONS "${COMMON_OPTIONS} -std=c++0x")
		endif()
	elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC")
		# TODO
	elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Intel")
		set(COMMON_OPTIONS "${COMMON_OPTIONS} -std=c++11")
	elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Borland" OR "${CMAKE_CXX_COMPILER_ID}" STREQUAL "Embarcadero")
		# TODO
	else()
		message(FATAL_ERROR "Unsupported compiler: ${CMAKE_CXX_COMPILER_ID}")
	endif()

	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${COMMON_OPTIONS}")
else()
	message(FATAL_ERROR "Unsupported language: ${CT_LANG}")
endif()

#message("COMMON_OPTIONS: ${COMMON_OPTIONS}")

include(${CMAKE_CURRENT_LIST_DIR}/cmake_tools/tools/compilers.cmake)
