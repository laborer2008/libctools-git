/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 * sdcc doesn't support sscanf(), strtol() and so on.
 * MS VC++ lacks of 'hh' length modifier support.
 * See https://connect.microsoft.com/VisualStudio/feedback/details/678386/c99-feature-hh-in-scanf-scanf-s
 * Because of that HEX functions are implemented here
 */

#pragma once

#include "ctools/predef/external_headers.h"

CT_BEGIN_EXTERNAL_HEADERS
//	size_t
#	include <stddef.h>
CT_END_EXTERNAL_HEADERS

#include "ctools/std/sys/types.h"
#include "ctools/std/stdbool.h"
#include "ctools/std/inttypes.h"
#include "ctools/predef/cxx11_attributes.h"
#include "ctools/namespace.h"


CT_BEGIN_NAMESPACE

/*
 * Converts hexadecimal character '0'-{'f'|'F'} to 0 - 15 integer value
 * or returns kInvalidHexCharacter if this conversion is not possible
 */
static const uint8_t kInvalidHexCharacter = 255;
uint8_t hexCharacterToUint(const char character) CT_NOEXCEPT;

/*
 * Converts pair of hexadecimal characters to byte value
 * or returns kInvalidHexPair if this conversion is not possible.
 * Example: "1F" => 31
 */
static const uint16_t kInvalidHexPair = UINT16_MAX;
uint16_t hexPairToByte(const char lsb, const char msb) CT_NOEXCEPT;

/*
 * @brief converts hexadecimal string to memory array. NULL at the end is not counted with @param hexStringSizeInBytes.
 * @return false if @param hexString was correct
 * @return @param array with converted data
 */
bool hexStringToArray(uint8_t* array, const char* const hexString, const size_t hexStringSizeInBytes) CT_NOEXCEPT;

CT_END_NAMESPACE
