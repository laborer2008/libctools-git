/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 * MS Visual C++ is a little buggy for constructions like "do {} while (false)".
 * See : http://stackoverflow.com/questions/1946445/c-c-how-to-use-the-do-while0-construct-without-compiler-warnings-like-c412
 * These 2 macros help to workaround such bugs for both quite new (version >= 2008) and old compilers
 */

#pragma once


#ifdef CT_TRUE
#	error CT_TRUE must be undefined
#endif

#ifdef CT_FALSE
#	error CT_FALSE must be undefined
#endif

/* *INDENT-OFF* */
#define CT_TRUE		((void)1, 1)
#define CT_FALSE	((void)0, 0)
/* *INDENT-ON* */
