/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 * This module is intended mainly for bare metal systems.
 * That's why packing is used here as well pure enums (not CT_ENUM_CLASS).
 * Any standalone enum variable can be accessed as quick as possible.
 * Otherwise PinID and PinDescription save as much RAM as possible
 */

#pragma once

#include "ctools/pack.h"


#if defined (CT_PACK_SUPPORTED)

#include "ctools/predef/compiler.h"
#include "ctools/std/sys/types.h"
#include "ctools/namespace.h"


CT_BEGIN_NAMESPACE

enum PinPulling
{
	ppUndefined		= 0,
	ppOff			= 1,
	ppDown			= 2,
	ppUp			= 3,
};

enum PinDirection
{
	pdOutput		= 0,
	pdInput			= 1,
};

/*
 * Inheritance is recommended here.
 * That allows to rename some alternative functions and have dualistic access to them
 */
enum PinFunction
{
	pfDefault		= 0,
	pfAlternate1	= 1,
	pfAlternate2	= 2,
	pfAlternate3	= 3,
	pfAlternate4	= 4,
	pfAlternate5	= 5,
	pfAlternate6	= 6,
	pfAlternate7	= 7

	// And so on, 32 bits is reserved here
};

enum PinState
{
	psLow = 0,
	psHigh = 1,
	psUndefined	= 2	// Z state
};

#if defined (CT_COMPL_MSVC)
	__pragma(warning(push, 0))
	__pragma(warning(disable:4214))
#endif

/* *INDENT-OFF* */
CT_PACK(
/* *INDENT-ON* */

// TODO: Some alternative is needed for complex MCUs
struct PinID__
{
	uint8_t	port : 3;
	uint8_t	number : 5;
});

typedef struct PinID__ PinID;

/* *INDENT-OFF* */
CT_PACK(
/* *INDENT-ON* */

struct PinDescription__
{
	uint8_t		pinPulling : 2;
	uint8_t		pinDirection : 1;
	uint8_t		pinFunction : 5;

	PinID		pinID;
});

#if defined (CT_COMPL_MSVC)
	__pragma(warning(pop))
#endif

typedef struct PinDescription__ PinDescription;

CT_END_NAMESPACE

#endif	// defined (CT_PACK_SUPPORTED)
