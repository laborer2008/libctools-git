/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#pragma once

#ifdef CT_OS_WINDOWS
#	error CT_OS_WINDOWS must be undefined
#endif

#ifdef CT_OS_UNIX
#	error CT_OS_UNIX must be undefined
#endif

#ifdef CT_OS_LINUX
#	error CT_OS_LINUX must be undefined
#endif

#ifdef CT_OS_ANDROID
#	error CT_OS_ANDROID must be undefined
#endif

#ifdef CT_OS_MACOS
#	error CT_OS_MACOS must be undefined
#endif


#ifndef CT_NO_DYNAMIC_MEMORY
#	if defined (_WIN32) || defined (__WIN32__) || defined (WIN32) || defined (_WIN64)
#		define CT_OS_WINDOWS
#	elif defined (unix) || defined (__unix) || defined (__unix__)
#		define CT_OS_UNIX
#		if defined (linux) || defined (__linux) || defined (__linux__)
#			define CT_OS_LINUX
#			if defined (__ANDROID__)
#				define CT_OS_ANDROID
#			endif
#		elif defined (__APPLE__) && defined (__MACH__)
#			define CT_OS_MACOS
#		endif
#	endif
#endif


#ifdef CT_NO_OS
#	error CT_NO_OS must be undefined
#endif

#if !defined (CT_OS_WINDOWS) && !defined (CT_OS_UNIX)
#	define CT_NO_OS
#endif
