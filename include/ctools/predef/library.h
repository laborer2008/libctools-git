/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#pragma once

#include "ctools/predef/external_headers.h"
#include "ctools/predef/compiler.h"

CT_BEGIN_EXTERNAL_HEADERS
/*
 *	This standard header can include <features.h> if it exists on the very platform.
 *	Afterwards non standard GNU macroses will be defined
 */
#	include <stdio.h>

#	if (defined (CT_COMPL_MSVC) && (_MSC_VER >= CT_COMPL_MSVC_STUDIO_2012)) || defined CT_COMPL_BCC
#		include <yvals.h>
#	endif
CT_END_EXTERNAL_HEADERS


#ifdef CT_LIBC_UCLIBC
#	error CT_LIBC_UCLIBC must be undefined
#endif

#ifdef CT_LIBC_GLIBC
#	error CT_LIBC_GLIBC must be undefined
#endif

#ifdef CT_LIBC_MINGW
#	error CT_LIBC_MINGW must be undefined
#endif

#ifdef CT_LIBC_MSVCRT
#	error CT_LIBC_MSVCRT must be undefined
#endif

#ifdef CT_LIBC_BIONIC
#	error CT_LIBC_BIONIC must be undefined
#endif

#ifdef CT_LIBC_SDCC
#	error CT_LIBC_SDCC must be undefined
#endif


#ifdef CT_LIBC_MSVCRT_2005
#	error CT_LIBC_MSVCRT_2005 must be undefined
#endif

#ifdef CT_LIBC_MSVCRT_2008
#	error CT_LIBC_MSVCRT_2008 must be undefined
#endif

#ifdef CT_LIBC_MSVCRT_2010
#	error CT_LIBC_MSVCRT_2010 must be undefined
#endif

#ifdef CT_LIBC_MSVCRT_2012
#	error CT_LIBC_MSVCRT_2012 must be undefined
#endif

#ifdef CT_LIBC_MSVCRT_2013
#	error CT_LIBC_MSVCRT_2013 must be undefined
#endif

#ifdef CT_LIBC_MSVCRT_2015
#	error CT_LIBC_MSVCRT_2015 must be undefined
#endif

#ifdef CT_LIBC_MSVCRT_2017
#	error CT_LIBC_MSVCRT_2017 must be undefined
#endif


#if defined (__UCLIBC__)
#	define CT_LIBC_UCLIBC

/*
 * CT_LIBC_UCLIBC_VERSION can be used in this situation:
 * #if CT_LIBC_UCLIBC_VERSION >= 929 that means "if uclibc has version greater or equal to 0.9.29"
 */
#	if defined (__UCLIBC_MAJOR__) && defined (__UCLIBC_MINOR__) && defined (__UCLIBC_SUBLEVEL__)
#		define CT_LIBC_UCLIBC_VERSION	(((__UCLIBC_MAJOR__) * 10000) + (__UCLIBC_MINOR__) * 100 + (__UCLIBC_SUBLEVEL__))
#	else
#		define CT_LIBC_UCLIBC_VERSION	(0)
#	endif

#elif defined (__GLIBC__)
#	define CT_LIBC_GLIBC
#elif defined (__MSVCRT__)
//	Something from gcc, something from Windows API
#	define CT_LIBC_MINGW
#elif defined (_CPPLIB_VER)
//	_CPPLIB_VER actually means Dinkumware's library shipped with VC++.
//	At least Embarcadero RAD Studio XE6 also is equipped with that library
#	define CT_LIBC_MSVCRT

//  Matching:
#	define CT_LIBC_MSVCRT_2005		405
#	define CT_LIBC_MSVCRT_2008		503
#	define CT_LIBC_MSVCRT_2008SP1	505
#	define CT_LIBC_MSVCRT_2010		520
#	define CT_LIBC_MSVCRT_2012		540
#	define CT_LIBC_MSVCRT_2013		610
#	define CT_LIBC_MSVCRT_2015		650

// TODO CT_LIBC_MSVCRT_2017

#elif defined (CT_COMPL_SDCC)
//	SDCC library detection is not excelled of compiler detection
#	define CT_LIBC_SDCC
#endif


#include "ctools/predef/os.h"

#if defined (CT_OS_ANDROID)

CT_BEGIN_EXTERNAL_HEADERS
#		include <sys/cdefs.h>
CT_END_EXTERNAL_HEADERS


#	ifdef __BIONIC__
#		define CT_LIBC_BIONIC
#	endif

#endif	// defined (CT_OS_ANDROID)
