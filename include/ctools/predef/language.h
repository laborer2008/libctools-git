/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 * Only common language definitions here. To get supported c++ features resort to cpptools
 */

#pragma once

#include "ctools/predef/compiler.h"


#ifdef CT_LANG_C94
#	error CT_LANG_C94 must be undefined
#endif

#if defined (__STDC_VERSION__) && (__STDC_VERSION__ >= 199409L)
//	Some compilers like MS VC++ doesn't support C99 completely but they are useful nevertheless
#	define CT_LANG_C94
#endif


#ifdef CT_LANG_C99
#	error CT_LANG_C99 must be undefined
#endif

#if defined (__STDC_VERSION__) && (__STDC_VERSION__ >= 199901L)
#	define CT_LANG_C99
#endif


#ifdef CT_LANG_C11
#	error CT_LANG_C11 must be undefined
#endif

#if defined (__STDC_VERSION__) && (__STDC_VERSION__ >= 201112L)
#	define CT_LANG_C11
#endif


#ifdef CT_LANG_CXX11
#	error CT_LANG_CXX11 must be undefined
#endif

#if defined (__cplusplus) && (__cplusplus >= 201103L)
#	define CT_LANG_CXX11
#endif

#if defined (CT_GCC_EMULATION) && defined (__GXX_EXPERIMENTAL_CXX0X__)
#	define CT_LANG_CXX11
#endif


#ifdef CT_LANG_CXX14
#	error CT_LANG_CXX14 must be undefined
#endif

#if defined (__cplusplus) && (__cplusplus >= 201402L)
#	define CT_LANG_CXX14
#endif


#ifdef CT_LANG_CXX17
#	error CT_LANG_CXX17 must be undefined
#endif

#if defined (__cplusplus) && (__cplusplus > 201402L)
#	define CT_LANG_CXX17
#endif


#ifdef CT_LANG_OBJC
#	error CT_LANG_OBJC must be undefined
#endif

#if defined (__OBJC__)
#	define CT_LANG_OBJC
#endif
