/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#pragma once

#ifdef CT_BT_RELEASE
#	error CT_BT_RELEASE must be undefined
#endif

#ifdef CT_BT_DEBUG
#	error CT_BT_DEBUG must be undefined
#endif

/*
 * Macro NDEBUG controls appearance of C99 asserts (see man 3 assert).
 * It seems that it is the only widely adopted define to switch between release/debug build.
 * Also MSVS has _DEBUG definition
 */

#if defined (NDEBUG)
#	define CT_BT_RELEASE
#else
#	define CT_BT_DEBUG
#endif
