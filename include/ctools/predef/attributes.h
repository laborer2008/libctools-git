/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 * Attention! CT_GCC_PACKED is a critical attribute but not portable. As well as CT_GCC_UNUSED.
 */

#pragma once

#include "ctools/predef/compiler.h"
#include "ctools/predef/language.h"


#ifdef CT_DLL_EXPORT
#	error CT_DLL_EXPORT must be undefined
#endif

#ifdef CT_DLL_IMPORT
#	error CT_DLL_IMPORT must be undefined
#endif

#ifdef CT_DLL_HIDDEN
#	error CT_DLL_HIDDEN must be undefined
#endif

#ifdef CT_FORCEINLINE
#	error CT_FORCEINLINE must be undefined
#endif

#ifdef CT_GCC_PACKED
#	error CT_GCC_PACKED must be undefined
#endif

#ifdef CT_GCC_UNUSED
#	error CT_GCC_UNUSED must be undefined
#endif

#ifdef CT_ALIGNED
#	error CT_ALIGNED must be undefined
#endif

#ifdef CT_NORETURN
#	error CT_NORETURN must be undefined
#endif

#ifdef CT_FUNCTION
#	error CT_FUNCTION must be undefined
#endif

#ifdef CT_STDCALL
#	error CT_STDCALL must be undefined
#endif

#ifdef CT_FASTCALL
#	error CT_FASTCALL must be undefined
#endif

#ifdef CT_CDECL
#	error CT_CDECL must be undefined
#endif

#ifdef CT_TLS
#	error CT_TLS must be undefined
#endif

#ifdef CT_PURE_FUNC
#	error CT_PURE_FUNC must be undefined
#endif

#ifdef CT_CONST_FUNC
#	error CT_CONST_FUNC must be undefined
#endif

#ifdef CT_DEPRECATED_FUNC
#	error CT_DEPRECATED_FUNC must be undefined
#endif


/* *INDENT-OFF* */
#if defined (CT_COMPL_MINGW)
#elif defined (CT_COMPL_CLANG)
#elif defined (CT_COMPL_ICC)
#elif defined (CT_COMPL_GCC)
#elif defined (CT_COMPL_ARMCC)
#elif defined (CT_COMPL_BCC)

#	define CT_DLL_EXPORT				__declspec(dllexport)
#	define CT_DLL_IMPORT				__declspec(dllimport)
#	define CT_DLL_HIDDEN

#	define CT_GCC_PACKED
#	define CT_GCC_UNUSED

#	define CT_FORCEINLINE				__inline
#	define CT_ALIGNED(x)				// TODO? alignof(x)
#	define CT_NORETURN					__declspec(noreturn)
#	define CT_FUNCTION					__FUNC__
#	define CT_STDCALL					__stdcall
#	define CT_FASTCALL					__fastcall
#	define CT_CDECL						__cdecl
#	define CT_TLS						__declspec(thread)
#	define CT_RESTRICT

#	define CT_PURE_FUNC
#	define CT_CONST_FUNC
#	define CT_DEPRECATED_FUNC

#elif defined (CT_COMPL_MSVC)

#	define CT_DLL_EXPORT				__declspec(dllexport)
#	define CT_DLL_IMPORT				__declspec(dllimport)
#	define CT_DLL_HIDDEN

#	define CT_GCC_PACKED
#	define CT_GCC_UNUSED

#	define CT_FORCEINLINE				__forceinline
#	define CT_ALIGNED(x)				__declspec(align(x))
#	define CT_NORETURN					__declspec(noreturn)
#	define CT_FUNCTION					__FUNCSIG__
#	define CT_STDCALL					__stdcall
#	define CT_FASTCALL					__fastcall
#	define CT_CDECL						__cdecl
#	define CT_TLS						__declspec(thread)
#	define CT_RESTRICT					__restrict

#	define CT_PURE_FUNC
#	define CT_CONST_FUNC
#	define CT_DEPRECATED_FUNC			__declspec(deprecated)

#else

#	define CT_DLL_EXPORT
#	define CT_DLL_IMPORT
#	define CT_DLL_HIDDEN

#	define CT_GCC_PACKED
#	define CT_GCC_UNUSED

#	define CT_FORCEINLINE
#	define CT_ALIGNED(x)

#	if defined (CT_LANG_C11)
#		define CT_NORETURN				_Noreturn
#		define CT_TLS					_Thread_local
#		define CT_ALIGNED(x)			_Alignas(x)
#	elif defined (CT_LANG_CXX11)
#		define CT_NORETURN				noreturn
#		define CT_TLS					thread_local
#		define CT_ALIGNED(x)			alignas(x)
#	else
#		define CT_NORETURN
#		define CT_TLS
#		define CT_ALIGNED(x)
#	endif

#	define CT_FUNCTION					""
#	define CT_STDCALL
#	define CT_FASTCALL
#	define CT_CDECL

#	define CT_RESTRICT

#	define CT_PURE_FUNC
#	define CT_CONST_FUNC
#	define CT_DEPRECATED_FUNC

#endif

#ifdef CT_COMPL_GCC_EMULATION

#	if defined (CT_COMPL_MINGW)
#		define CT_DLL_EXPORT			__declspec(dllexport)
#		define CT_DLL_IMPORT			__declspec(dllimport)
#		define CT_DLL_HIDDEN
#	else
#		define CT_DLL_EXPORT			__attribute__((visibility("default")))
#		define CT_DLL_IMPORT			__attribute__((visibility("default")))
#		define CT_DLL_HIDDEN			__attribute__((visibility("hidden")))
#	endif

//	For mingw 4.7 and above see https://gcc.gnu.org/bugzilla/show_bug.cgi?id=52991
#	define CT_GCC_PACKED				__attribute__((packed))

#	define CT_GCC_UNUSED				__attribute__((unused))

#	define CT_FORCEINLINE				inline __attribute__((always_inline))
#	define CT_ALIGNED(x)				__attribute__((aligned(x)))
#	define CT_NORETURN					__attribute__((noreturn))
#	define CT_FUNCTION					__PRETTY_FUNCTION__
#	define CT_STDCALL					__attribute__((stdcall))
#	define CT_FASTCALL					__attribute__((fastcall))
#	define CT_CDECL						__attribute__((cdecl))
#	define CT_TLS						__thread
#	define CT_RESTRICT					__restrict

#	define CT_PURE_FUNC					__attribute__((pure))
#	define CT_CONST_FUNC				__attribute__((const))
#	define CT_DEPRECATED_FUNC			__attribute__((deprecated))

#else
//	Just ignore unknown attribute
#	define __attribute__(x)				(x)
#endif
/* *INDENT-ON* */
