/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#pragma once

#include "ctools/predef/compiler.h"

#ifdef CT_COMPL_GCC_EMULATION
#	include "ctools/stringify.h"


#	ifdef CT_GCC_WARNING_HELPER1__
#		error CT_GCC_WARNING_HELPER1__ must be undefined
#	endif

#	ifdef CT_GCC_WARNING_HELPER2__
#		error CT_GCC_WARNING_HELPER2__ must be undefined
#	endif

#	ifdef CT_GCC_IGNORE_WARNING
#		error CT_GCC_IGNORE_WARNING must be undefined
#	endif

#	define CT_GCC_WARNING_HELPER1__(x)		CT_STRINGIFY2(GCC diagnostic ignored x)
#	define CT_GCC_WARNING_HELPER2__(y)		CT_GCC_WARNING_HELPER1__(#y)
#	define CT_GCC_IGNORE_WARNING(x)			_Pragma(CT_GCC_WARNING_HELPER2__(x))

#	if defined (__cplusplus)
#		define CT_GCC_IGNORE_WARNING_REORDER	CT_GCC_IGNORE_WARNING(-Wreorder)
#	else
#		define CT_GCC_IGNORE_WARNING_REORDER
#	endif
#endif

/* *INDENT-OFF* */
#if defined (CT_COMPL_MINGW)

/*
 * Mingw (at least 4.8.2 bundled with Qt 5.x) is too buggy. These definitions likely won't help.
 * See: https://gcc.gnu.org/bugzilla/show_bug.cgi?id=53431
 */

#	if CT_GCC_VERSION >= 40600
#		define CT_BEGIN_EXTERNAL_HEADERS\
			_Pragma("GCC diagnostic push")\
			CT_GCC_IGNORE_WARNING(-Wcast-qual)\
			CT_GCC_IGNORE_WARNING(-Winline)\
			CT_GCC_IGNORE_WARNING_REORDER\
			CT_GCC_IGNORE_WARNING(-Wunused-local-typedefs)\
			CT_GCC_IGNORE_WARNING(-Wunused-variable)\
			CT_GCC_IGNORE_WARNING(-Wbool-compare)\
			CT_GCC_IGNORE_WARNING(-Wunused-parameter)\
			CT_GCC_IGNORE_WARNING(-Wextra)\
			CT_GCC_IGNORE_WARNING(-Wuninitialized)\
			CT_GCC_IGNORE_WARNING(-Wmaybe-uninitialized)
#		define CT_END_EXTERNAL_HEADERS\
			_Pragma("GCC diagnostic pop")
#	else
#		define CT_BEGIN_EXTERNAL_HEADERS
#		define CT_END_EXTERNAL_HEADERS
#	endif

#elif defined (CT_COMPL_CLANG)

/*
 * Just for notice: clang 3.0 doesn't support -Wdeprecated-register and abuses abundantly
 */

#	define CT_BEGIN_EXTERNAL_HEADERS\
		_Pragma("GCC diagnostic push")\
		CT_GCC_IGNORE_WARNING(-Wdeprecated-register)\
		CT_GCC_IGNORE_WARNING_REORDER\
		CT_GCC_IGNORE_WARNING(-Wunused-parameter)
#	define CT_END_EXTERNAL_HEADERS\
		_Pragma("GCC diagnostic pop")

#elif defined (CT_COMPL_ICC)

//	TODO
#	define CT_BEGIN_EXTERNAL_HEADERS
#	define CT_END_EXTERNAL_HEADERS

#elif defined (CT_COMPL_GCC)

#	if CT_GCC_VERSION >= 40600
#		define CT_BEGIN_EXTERNAL_HEADERS\
			_Pragma("GCC diagnostic push")\
			CT_GCC_IGNORE_WARNING(-Wcast-qual)\
			CT_GCC_IGNORE_WARNING(-Winline)\
			CT_GCC_IGNORE_WARNING_REORDER\
			CT_GCC_IGNORE_WARNING(-Wunused-local-typedefs)\
			CT_GCC_IGNORE_WARNING(-Wunused-variable)\
			CT_GCC_IGNORE_WARNING(-Wbool-compare)\
			CT_GCC_IGNORE_WARNING(-Wunused-parameter)\
			CT_GCC_IGNORE_WARNING(-Wextra)\
			CT_GCC_IGNORE_WARNING(-Wuninitialized)\
			CT_GCC_IGNORE_WARNING(-Wmaybe-uninitialized)
#		define CT_END_EXTERNAL_HEADERS\
			_Pragma("GCC diagnostic pop")
#	else
#		define CT_BEGIN_EXTERNAL_HEADERS
#		define CT_END_EXTERNAL_HEADERS
#	endif

#elif defined (CT_COMPL_MSVC)

#	define CT_BEGIN_EXTERNAL_HEADERS\
		__pragma(warning(push, 0))\
		__pragma(warning(disable:4242))\
		__pragma(warning(disable:4265))\
		__pragma(warning(disable:4350))\
		__pragma(warning(disable:4355))\
		__pragma(warning(disable:4548))\
		__pragma(warning(disable:4996))
#	define CT_END_EXTERNAL_HEADERS\
		__pragma(warning(pop))

#else

#	define CT_BEGIN_EXTERNAL_HEADERS
#	define CT_END_EXTERNAL_HEADERS

#endif
/* *INDENT-ON* */
