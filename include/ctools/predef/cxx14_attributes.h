/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 * Only language-specific cxx14 attributes that can be met both in C and C++ code.
 * To get complete list of supported c++14 attributes resort to cpptools
 */

#pragma once

#include "ctools/predef/cxx11_attributes.h"


#ifdef CT_NOEXCEPT_UNTIL_CXX14
#	error CT_NOEXCEPT_UNTIL_CXX14 must be undefined
#endif

#ifdef CT_NOEXCEPT_SINCE_CXX14
#	error CT_NOEXCEPT_SINCE_CXX14 must be undefined
#endif

/* *INDENT-OFF* */
#ifdef CT_LANG_CXX14
#	define CT_NOEXCEPT_UNTIL_CXX14
#	define CT_NOEXCEPT_SINCE_CXX14		CT_NOEXCEPT
#else
#	define CT_NOEXCEPT_UNTIL_CXX14		CT_NOEXCEPT
#	define CT_NOEXCEPT_SINCE_CXX14
#endif

/* *INDENT-ON* */
