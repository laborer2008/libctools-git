/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#pragma once

#ifdef CT_COMPL_MINGW
#	error CT_COMPL_MINGW must be undefined
#endif

#ifdef CT_COMPL_CLANG
#	error CT_COMPL_CLANG must be undefined
#endif

#ifdef CT_COMPL_GCC
#	error CT_COMPL_GCC must be undefined
#endif

#ifdef CT_COMPL_MSVC
#	error CT_COMPL_MSVC must be undefined
#endif

#ifdef CT_COMPL_MSVC_RC
//	Check for redefinition is impossible
#endif

#ifdef CT_COMPL_ICC
#	error CT_COMPL_ICC must be undefined
#endif

#ifdef CT_COMPL_ARMCC
#	error CT_COMPL_ARMCC must be undefined
#endif

#ifdef CT_COMPL_SDCC
#	error CT_COMPL_SDCC must be undefined
#endif


#ifdef CT_COMPL_MSVC_5_0
#	error CT_COMPL_MSVC_5_0 must be undefined
#endif

#ifdef CT_COMPL_MSVC_6_0
#	error CT_COMPL_MSVC_6_0 must be undefined
#endif

#ifdef CT_COMPL_MSVC_7_0
#	error CT_COMPL_MSVC_7_0 must be undefined
#endif

#ifdef CT_COMPL_MSVC_7_1
#	error CT_COMPL_MSVC_7_1 must be undefined
#endif

#ifdef CT_COMPL_MSVC_8_0
#	error CT_COMPL_MSVC_8_0 must be undefined
#endif

#ifdef CT_COMPL_MSVC_9_0
#	error CT_COMPL_MSVC_9_0 must be undefined
#endif

#ifdef CT_COMPL_MSVC_10_0
#	error CT_COMPL_MSVC_10_0 must be undefined
#endif

#ifdef CT_COMPL_MSVC_11_0
#	error CT_COMPL_MSVC_11_0 must be undefined
#endif

#ifdef CT_COMPL_MSVC_12_0
#	error CT_COMPL_MSVC_12_0 must be undefined
#endif

#ifdef CT_COMPL_MSVC_14_0
#	error CT_COMPL_MSVC_14_0 must be undefined
#endif

#ifdef CT_COMPL_MSVC_STUDIO_2003
#	error CT_COMPL_MSVC_STUDIO_2003 must be undefined
#endif

#ifdef CT_COMPL_MSVC_STUDIO_2005
#	error CT_COMPL_MSVC_STUDIO_2005 must be undefined
#endif

#ifdef CT_COMPL_MSVC_STUDIO_2008
#	error CT_COMPL_MSVC_STUDIO_2008 must be undefined
#endif

#ifdef CT_COMPL_MSVC_STUDIO_2010
#	error CT_COMPL_MSVC_STUDIO_2010 must be undefined
#endif

#ifdef CT_COMPL_MSVC_STUDIO_2012
#	error CT_COMPL_MSVC_STUDIO_2012 must be undefined
#endif

#ifdef CT_COMPL_MSVC_STUDIO_2013
#	error CT_COMPL_MSVC_STUDIO_2013 must be undefined
#endif

#ifdef CT_COMPL_MSVC_STUDIO_2015
#	error CT_COMPL_MSVC_STUDIO_2015 must be undefined
#endif

#ifdef CT_COMPL_BCC
#	error CT_COMPL_BCC must be undefined
#endif


// All gcc-based compilers should be checked before gcc
#if defined (__MINGW32__)
#	define CT_COMPL_MINGW
#elif defined (__clang__)
#	define CT_COMPL_CLANG
#elif defined (__INTEL_COMPILER)
//	Keep in mind: Intel C++ can be in Visual C++ compatibility mode, See below
#	define CT_COMPL_ICC
#elif defined (__ARMCC_VERSION)
#	define CT_COMPL_ARMCC	// Keil

#elif defined (SDCC) || defined (__SDCC)
#	define CT_COMPL_SDCC
#elif defined (__CODEGEARC__) || defined (__BORLANDC__)
#	define CT_COMPL_BCC
#elif defined (__GNUC__)
#	define CT_COMPL_GCC
#elif defined (_MSC_VER)
#	define CT_COMPL_MSVC

//	Matching:
#	define CT_COMPL_MSVC_5_0				1100
#	define CT_COMPL_MSVC_6_0				1200
#	define CT_COMPL_MSVC_7_0				1300
#	define CT_COMPL_MSVC_7_1				1310
#	define CT_COMPL_MSVC_8_0				1400
#	define CT_COMPL_MSVC_9_0				1500
#	define CT_COMPL_MSVC_10_0				1600
#	define CT_COMPL_MSVC_11_0				1700
#	define CT_COMPL_MSVC_12_0				1800
#	define CT_COMPL_MSVC_14_0				1900

#	define CT_COMPL_MSVC_STUDIO_2003		CT_COMPL_MSVC_7_1
#	define CT_COMPL_MSVC_STUDIO_2005		CT_COMPL_MSVC_8_0
#	define CT_COMPL_MSVC_STUDIO_2008		CT_COMPL_MSVC_9_0
#	define CT_COMPL_MSVC_STUDIO_2010		CT_COMPL_MSVC_10_0
#	define CT_COMPL_MSVC_STUDIO_2012		CT_COMPL_MSVC_11_0
#	define CT_COMPL_MSVC_STUDIO_2013		CT_COMPL_MSVC_12_0
#	define CT_COMPL_MSVC_STUDIO_2015		CT_COMPL_MSVC_14_0
#endif

#if defined (RC_INVOKED)
//	Resource compiler. For Visual Studio 2012 at least RC_INVOKED is defined but _MSC_VER is not
#	define CT_COMPL_MSVC_RC
#endif


#ifdef CT_COMPL_GCC_EMULATION
#	error CT_COMPL_GCC_EMULATION must be undefined
#endif

#if defined (CT_COMPL_MINGW) || defined (CT_COMPL_CLANG) || defined (CT_COMPL_GCC)\
	|| (defined (CT_COMPL_ICC) && defined (__GNUC__)) || defined (CT_COMPL_ARMCC)
#		define CT_COMPL_GCC_EMULATION
#endif


#ifdef CT_GCC_VERSION
#	error CT_GCC_VERSION must be undefined
#endif

/*
 * CT_GCC_VERSION can be used in this situation:
 * #if CT_GCC_VERSION >= 40407 that means "if gcc has version greater or equal to 4.4.7"
 */
/* *INDENT-OFF* */
#if defined (CT_COMPL_GCC_EMULATION)
#	if defined (__GNUC_MINOR__) && defined (__GNUC_PATCHLEVEL__)
#		define CT_GCC_VERSION	(((__GNUC__) * 10000) + (__GNUC_MINOR__) * 100 + (__GNUC_PATCHLEVEL__))
#	else
#		define CT_GCC_VERSION	(0)
#	endif
#endif
/* *INDENT-ON* */


#ifdef CT_CLANG_VERSION
#	error CT_CLANG_VERSION must be undefined
#endif

/*
 * CT_CLANG_VERSION can be used in this situation:
 * #if CT_CLANG_VERSION >= 30200 that means "if clang has version greater or equal to 3.2"
 * Annoyingly, Apple makes the clang version defines match the version of Xcode, not the version of clang.
 * I.e. CT_CLANG_VERSION could be 60000 for clang 3.x This situation takes place when __APPLE__ is defined
 */
/* *INDENT-OFF* */
#if defined (CT_COMPL_CLANG)
#	if defined (__clang_major__) && defined (__clang_minor__) && defined (__clang_patchlevel__)
#		define CT_CLANG_VERSION	(((__clang_major__) * 10000) + (__clang_minor__) * 100 + (__clang_patchlevel__))
#	else
#		define CT_CLANG_VERSION	(0)
#	endif
#endif
/* *INDENT-ON* */


#ifdef CT_COMPL_MSVC_EMULATION
#	error CT_COMPL_MSVC_EMULATION must be undefined
#endif

#if (defined (CT_COMPL_ICC) && defined (_MSC_VER)) || defined (CT_COMPL_MSVC)
#		define CT_COMPL_MSVC_EMULATION
#endif
