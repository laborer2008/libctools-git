/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#pragma once

#include "ctools/predef/library.h"


/*
 * Since glibc 2.20 _BSD_SOURCE and _SVID_SOURCE definitions are deprecared in favor of _DEFAULT_SOURCE.
 * This macro provides a portable way to use one definition for both old and new systems
 */

#ifdef CT_GLIBC_BSD_DEFAULT_SOURCE_WAS_DEFINED
#	error CT_GLIBC_BSD_DEFAULT_SOURCE_WAS_DEFINED must be undefined
#endif

#ifdef CT_GLIBC_SVID_DEFAULT_SOURCE_WAS_DEFINED
#	error CT_GLIBC_SVID_DEFAULT_SOURCE_WAS_DEFINED must be undefined
#endif


#if defined (CT_LIBC_GLIBC)

#	if defined (CT_GLIBC_NEED_BSD_DEFAULT_SOURCE)
#		define CT_GLIBC_BSD_DEFAULT_SOURCE_WAS_DEFINED	1

#		if ((__GLIBC__ > 2) || ((__GLIBC__ == 2) && (__GLIBC_MINOR__ >= 20)))
#			ifndef _DEFAULT_SOURCE
#				define _DEFAULT_SOURCE	1
#			endif
#		else
#			ifndef _BSD_SOURCE
#				define _BSD_SOURCE		1
#				define __USE_BSD		1
#			endif
#		endif
#	endif

#	if defined (CT_GLIBC_NEED_SVID_DEFAULT_SOURCE)
#		define CT_GLIBC_SVID_DEFAULT_SOURCE_WAS_DEFINED	1

#		if ((__GLIBC__ > 2) || ((__GLIBC__ == 2) && (__GLIBC_MINOR__ >= 20)))
#			ifndef _DEFAULT_SOURCE
#				define _DEFAULT_SOURCE	1
#			endif
#		else
#			ifndef _SVID_SOURCE
#				define _SVID_SOURCE		1
#			endif
#		endif
#	endif

#endif
