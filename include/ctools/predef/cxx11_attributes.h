/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 * Only language-specific cxx11 attributes that can be met both in C and C++ code.
 * To get complete list of supported c++11 attributes resort to cpptools/predef/language.hpp
 */

#pragma once

#include "ctools/predef/language.h"
#include "ctools/predef/compiler.h"


#ifdef CT_COMPILER_NOEXCEPT
#	error CT_COMPILER_NOEXCEPT must be undefined
#endif

#ifdef CT_COMPILER_CLASS_ENUM
#	error CT_COMPILER_CLASS_ENUM must be undefined
#endif


#ifdef CT_NOEXCEPT
#	error CT_NOEXCEPT must be undefined
#endif

#ifdef CT_ENUM_CLASS
#	error CT_ENUM_CLASS must be undefined
#endif

/* *INDENT-OFF* */
#if defined CT_LANG_CXX11
#	if defined (CT_COMPL_ICC)
#		if __INTEL_COMPILER >= 1200
#			define CT_COMPILER_CLASS_ENUM
#		endif
#		if __INTEL_COMPILER >= 1300
#			define CT_COMPILER_NOEXCEPT
#		endif
#	elif defined (CT_COMPL_CLANG)
#		if CT_CLANG_VERSION >= 20900
#			define CT_COMPILER_CLASS_ENUM
#		endif
#		if CT_CLANG_VERSION >= 30000
#			define CT_COMPILER_NOEXCEPT
#		endif
#	elif defined (CT_COMPL_ARMCC)
#	elif defined (CT_COMPL_GCC) || defined (CT_COMPL_MINGW)
#		if CT_GCC_VERSION >= 40500
#			define CT_COMPILER_CLASS_ENUM
#		endif
/*		Note from Qt5.5.0/qtbase/src/corelib/global/qcompilerdetection.h
 *		GCC 4.6.x has problems dealing with noexcept expressions,
 *		so turn the feature on for 4.7 and above, only
 */
#		if CT_GCC_VERSION >= 40700
#			define CT_COMPILER_NOEXCEPT
#		endif
#	elif defined (CT_COMPL_MSVC)
#		if _MSC_VER >= CT_COMPL_MSVC_STUDIO_2012
#			define CT_COMPILER_CLASS_ENUM
#		endif
#		if _MSC_VER >= CT_COMPL_MSVC_STUDIO_2015
#			define CT_COMPILER_NOEXCEPT
#		endif
#	endif
#endif

#if defined (CT_COMPILER_NOEXCEPT)
#	define CT_NOEXCEPT				noexcept
#else
#	ifdef __cplusplus
#		define CT_NOEXCEPT			throw()
#	else
#		define CT_NOEXCEPT
#	endif
#endif	// defined (CT_COMPILER_NOEXCEPT)

#if defined (CT_COMPILER_CLASS_ENUM)
#	define CT_ENUM_CLASS			enum class
#else
#	define CT_ENUM_CLASS			enum
#endif	// defined (CT_COMPILER_CLASS_ENUM)
/* *INDENT-ON* */
