/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#pragma once

#ifdef CT_ARCH_X86
#	error  must be undefined
#endif

#ifdef CT_ARCH_X64
#	error CT_ARCH_X64 must be undefined
#endif

#ifdef CT_ARCH_ARM
#	error CT_ARCH_ARM must be undefined
#endif

#ifdef CT_ARCH_ARM64
#	error CT_ARCH_ARM64 must be undefined
#endif

#ifdef CT_ARCH_STM8
#	error CT_ARCH_STM8 must be undefined
#endif


/* *INDENT-OFF* */
#if defined (i386) || defined (__i386) || defined (__i386__) || defined (_X86_) || defined (_M_I86) || defined (_M_IX86)
#	define CT_ARCH_X86
#elif defined (__amd64__) || defined (__amd64) || defined (__x86_64__)\
		|| defined (__x86_64) || defined (_M_X64) || defined (_M_AMD64)
#	define CT_ARCH_X64
#elif defined (__arm__) || defined (__thumb__) || defined (_ARM) || defined (_M_ARM) || defined (_M_ARMT)
#	define CT_ARCH_ARM
#elif defined (__aarch64__)
#	define CT_ARCH_ARM64
#elif defined (SDCC_stm8) || defined (__SDCC_stm8)
#	define CT_ARCH_STM8
#endif
/* *INDENT-ON* */

#ifdef CT_ARCH_X86_BASED
#	error CT_ARCH_X86_BASED must be undefined
#endif

#if defined (CT_ARCH_X86) || defined (CT_ARCH_X64)
#	define CT_ARCH_X86_BASED
#endif
