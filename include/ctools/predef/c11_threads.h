/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 * Wrapper to detect c11 threads.h support.
 * It seems that as of 11/2014 no one from big players support that feature.
 * See: http://stackoverflow.com/questions/24557728/does-any-c-library-implement-c11-threads-for-gnu-linux
 */

#pragma once

#include "ctools/predef/compiler.h"
#include "ctools/predef/language.h"


#ifdef CT_C11_THREADES_H_AVAILABLE
#	error CT_C11_THREADES_H_AVAILABLE must be undefined
#endif

#if defined (CT_LANG_C11) && !defined (__STDC_NO_THREADS__)
// TODO: define CT_C11_THREADES_H_AVAILABLE when it implemented
#endif
