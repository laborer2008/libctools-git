/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#pragma once

#ifdef CT_STRINGIFY2
#	error CT_STRINGIFY2 must be undefined
#endif

#ifdef CT_STRINGIFY
#	error CT_STRINGIFY must be undefined
#endif

#ifdef CT_NUMBERIZE2
#	error CT_NUMBERIZE2 must be undefined
#endif

#ifdef CT_NUMBERIZE
#	error CT_NUMBERIZE must be undefined
#endif

/*
 * These two macros makes it possible to turn the builtin line expander
 * into a string literal
 */
#define CT_STRINGIFY2(x)			#x
#define CT_STRINGIFY(x)				CT_STRINGIFY2(x)

#define CT_NUMBERIZE2(number)		number
#define CT_NUMBERIZE(number)		CT_NUMBERIZE2(number)
