/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#pragma once

#ifdef CT_BEGIN_NAMESPACE
#	error CT_BEGIN_NAMESPACE must be undefined
#endif

#ifdef CT_END_NAMESPACE
#	error CT_END_NAMESPACE must be undefined
#endif


/* *INDENT-OFF* */

#ifdef __cplusplus
#	define	CT_BEGIN_NAMESPACE\
		namespace ctools {\
			extern "C" {
#else
#	define	CT_BEGIN_NAMESPACE
#endif	// ifdef __cplusplus

/* *INDENT-ON* */

/* *INDENT-OFF* */

#ifdef __cplusplus
#	define	CT_END_NAMESPACE\
			}\
		}	// namespace ctools
#else
#	define	CT_END_NAMESPACE
#endif	// ifdef __cplusplus

/* *INDENT-ON* */


#ifdef CT_NAMESPACED
#	error CT_NAMESPACED must be undefined
#endif

#ifdef CT_USING_CT_NAMESPACE
#	error CT_USING_CT_NAMESPACE must be undefined
#endif

#ifdef __cplusplus
#	define CT_NAMESPACED(x)			ctools::x
#	define CT_USING_CT_NAMESPACE	using namespace ctools;
#else
#	define CT_NAMESPACED(x)			x

/*	Don't use here trailing ;. It works inside a function's body.
 *	But outside some compilers (sdcc) complains
 */
#	define CT_USING_CT_NAMESPACE
#endif
