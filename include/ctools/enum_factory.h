/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 * Borrowed from http://stackoverflow.com/questions/147267/easy-way-to-use-variables-of-enum-types-as-string-in-c#202511
 * [29.10.2014].
 * Usage example:
 *
 * In the some header ("someEnum.h"):
 *
 * #include "enumFactory.h"
 * #define SOME_ENUM(XX)\
 *     XX(FirstValue,)\
 *     XX(SecondValue,)\
 *     XX(SomeOtherValue,=50)\
 *     XX(OneMoreValue,=100)\
 *
 * CT_DECLARE_ENUM(SomeEnum, SOME_ENUM)
 *
 * In the some .c / .cpp:
 *
 * #include "someEnum.h"
 * CT_DEFINE_ENUM(SomeEnum, SOME_ENUM)
 */

#pragma once

#include "ctools/predef/external_headers.h"

CT_BEGIN_EXTERNAL_HEADERS
#	include <string.h>
CT_END_EXTERNAL_HEADERS

#include "ctools/predef/language.h"
#include "ctools/namespace.h"


/*
 * These definitions belongs to externel namespace.
 *
 * C++ only:
 * It is recomenned to place them in the standalone namespace and not inside class/structure.
 *
 * C only:
 * enumToString and stringToEnum becomes named more complexly. I.e. for "enum Status" they become
 * enumStatusToString and stringToEnumStatus appropriately.
 */


#ifdef CT_ENUM_VALUE
#	error CT_ENUM_VALUE must be undefined
#endif

// Expansion macro for enum value definition
#define CT_ENUM_VALUE(name, assign)			name assign,


#ifdef CT_ENUM_CASE
#	error CT_ENUM_CASE must be undefined
#endif

// Expansion macro for enum to string conversion
#define CT_ENUM_CASE(name, assign)			case name: return #name;


#ifdef CT_ENUM_STRCMP
#	error CT_ENUM_STRCMP must be undefined
#endif

// Expansion macro for string to enum conversion
#define CT_ENUM_STRCMP(name, assign)		if (!strcmp(str, #name)) return name;


#ifdef CT_DECLARE_ENUM
#	error CT_DECLARE_ENUM must be undefined
#endif

/// Declare the access function and define enum values
/* *INDENT-OFF* */
#ifdef __cplusplus
#	define CT_DECLARE_ENUM(enumType, CT_ENUM_DEF)\
		enum enumType\
		{\
			CT_ENUM_DEF(CT_ENUM_VALUE)\
		};\
		const char* enumToString(const enum enumType dummy);\
		enum enumType stringToEnum(const char* const string);
#else
#	define CT_DECLARE_ENUM(enumType, CT_ENUM_DEF)\
		enum enumType\
		{\
			CT_ENUM_DEF(CT_ENUM_VALUE)\
		};\
		const char* enum ## enumType ## ToString(const enum enumType dummy);\
		enum enumType stringTo  ## enumType ## Enum(const char* const string);
#endif
/* *INDENT-ON* */


#ifdef CT_DEFINE_ENUM
#	error CT_DEFINE_ENUM must be undefined
#endif

/// Define the access function names
/* *INDENT-OFF* */
#ifdef __cplusplus
#	define CT_DEFINE_ENUM(enumType, CT_ENUM_DEF)\
		const char* enumToString(const enum enumType value)\
		{\
			switch (value)\
			{\
				CT_ENUM_DEF(CT_ENUM_CASE)\
				default: return "invalid enum" #enumType " value"; /* Handle input error */\
			}\
		}\
		enum enumType stringToEnum(const char* const str)\
		{\
			CT_ENUM_DEF(CT_ENUM_STRCMP)\
			return (enum enumType)0; /* Handle input error */\
		}
#else
#	define CT_DEFINE_ENUM(enumType, CT_ENUM_DEF)\
		const char* enum ## enumType ## ToString(const enum enumType value)\
		{\
			switch (value)\
			{\
				CT_ENUM_DEF(CT_ENUM_CASE)\
				default: return "invalid enum " #enumType " value"; /* Handle input error */\
			}\
		}\
		enum enumType stringTo  ## enumType ## Enum(const char* const str)\
		{\
			CT_ENUM_DEF(CT_ENUM_STRCMP)\
			return (enum enumType)0; /* Handle input error */\
		}
#endif
/* *INDENT-ON* */
