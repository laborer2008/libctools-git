/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#pragma once

/*
 * Actually this header is not required here. But users of this module
 * often can want true/false as a first parameter of CT_ASSERT()
 */
#include "ctools/std/stdbool.h"

#include "ctools/bool_consts.h"

#if defined (CT_USE_RUNTIME_ASSERTS)
#	include "ctools/trace.h"
#endif

#if !defined (CT_LANG_C11) && !defined (CT_LANG_CXX11)
#	include "ctools/predef/attributes.h"
#endif


/*
 * We don't use c99 asserts because they don't provide infinite loop and
 * don't signal hereby to watchdog timer
 */

#ifdef CT_ASSERT
#	error CT_ASSERT must be undefined
#endif

#ifdef CT_RETURN_WITHOUT_ASSERT
#	error CT_RETURN_WITHOUT_ASSERT must be undefined
#endif

/* *INDENT-OFF* */
#if defined (CT_USE_RUNTIME_ASSERTS)
/*
 *	In the pure C we can't use #if nested to #define.
 *	So, "if (constant)" cannot be optimized. The only way to get rid of
 *	"warning C4127: conditional expression is constant" (MS VC++) is to turn it off.
 *	TODO: Actually this pragma doesn't work
 */
#	if defined (CT_COMPL_MSVC)
#		pragma warning(push)
#		pragma warning(disable : 4127)
#	endif

/*	Use it when an assert should be triggered without calculations at one line.
 *	It's known constantly
 */
#	define CT_ASSERT_FALSE(format, ...)\
		do\
		{\
			CT_TRACE_PERM_WP("ASSERT failed: %s" format, ## __VA_ARGS__);\
			CT_NAMESPACED(traceFatalAction();)\
		}\
		while (CT_FALSE)

#	define CT_ASSERT(condition, format, ...)\
		do\
		{\
			if (!(condition))\
				CT_ASSERT_FALSE(format, ## __VA_ARGS__);\
		}\
		while (CT_FALSE)

#	if defined (CT_COMPL_MSVC)
#		pragma warning(pop)
#	endif

#	define CT_RETURN_WITHOUT_ASSERT(result)
#else
#	define CT_ASSERT(condition, format, ...)\
		do\
		{\
			((void)(condition));\
		}\
		while (CT_FALSE)
#	define CT_RETURN_WITHOUT_ASSERT(result)		return result
#endif
/* *INDENT-ON* */


#ifdef CT_UNIQUE_NAME
#	error CT_UNIQUE_NAME must be undefined
#endif

#ifdef CT_MAKE_NAME
#	error CT_MAKE_NAME must be undefined
#endif

#ifdef CT_MAKE_NAME2
#	error CT_MAKE_NAME2 must be undefined
#endif


/*
 * __COUNTER__ is unique for the whole compilation.
 * Whereas __LINE__ can repeat itself for different files
 */
#ifdef __COUNTER__
#	define CT_UNIQUE_NAME					CT_MAKE_NAME(__COUNTER__)
#else
#	define CT_UNIQUE_NAME					CT_MAKE_NAME(__LINE__)
#endif

#define CT_MAKE_NAME(unicity)		CT_MAKE_NAME2(unicity)
#define CT_MAKE_NAME2(unicity)		constraint_ ## unicity


#ifdef CT_COMPILE_TIME_ASSERT
#	error CT_COMPILE_TIME_ASSERT must be undefined
#endif

#if defined (CT_LANG_C11)

#	define CT_COMPILE_TIME_ASSERT(expression, message)\
		_Static_assert(expression, message)

#elif defined (CT_LANG_CXX11)

#	define CT_COMPILE_TIME_ASSERT(expression, message)\
		static_assert(expression, message)

#else

/* *INDENT-OFF* */
#	define CT_COMPILE_TIME_ASSERT(expression, message)\
		do\
		{\
			typedef struct CT_UNIQUE_NAME\
			{\
				unsigned int bf : expression;\
			} A CT_GCC_UNUSED;\
		}\
		while (CT_FALSE)
/* *INDENT-ON* */

#endif
