/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#pragma once

#include "ctools/predef/external_headers.h"

CT_BEGIN_EXTERNAL_HEADERS
//	size_t, from C99 7.17.1/2
#	include <stddef.h>
CT_END_EXTERNAL_HEADERS

#include "ctools/std/stdint.h"


#ifdef CT_STRLEN
#	error CT_STRLEN must be undefined
#endif

/**
 * Calculation of the string length in compile-time.
 * Null terminating symbol is not taken into account.
 * It is quite safe only for not localized strings.
 * @return 0 for empty string or number of characters in the string
 */
#define CT_STRLEN(string)				((size_t)(sizeof(string) <= 1 ? 0 : (sizeof(string) / sizeof(*(string)) - 1)))


#ifdef CT_PACK_STRING_TO_UINT_AVAILABLE
#	error CT_PACK_STRING_TO_UINT_AVAILABLE must be undefined
#endif

/*
 * There is compilation problems with MS VC++ 2012 in C++ (but not C) mode:
 * error C2057: expected constant expression.
 * TODO: MS VC++ 2015 is free of this. MS VC++ 2013 - unknown
 */

#ifdef __cplusplus
#	if defined (CT_COMPL_MSVC)
#		if (_MSC_VER >= CT_COMPL_MSVC_STUDIO_2013)
#			define CT_PACK_STRING_TO_UINT_AVAILABLE
#		endif
#	else
#		define CT_PACK_STRING_TO_UINT_AVAILABLE
#	endif
#else
#	define CT_PACK_STRING_TO_UINT_AVAILABLE
#endif

#if defined (CT_PACK_STRING_TO_UINT_AVAILABLE)

#	define CT_PACK_STRING_TO_UINT_EMPTY_STRING			0
#	define CT_PACK_STRING_TO_UINT_TOO_LONG_STRING		UINT64_MAX

/**
 * @brief Store up to 8 characters in the one unsigned integer value
 * CT_STRLEN(str) == 2U is strange because of gcc warning "signed and unsigned type in conditional expression".
 * Assert couldn't be used here - it will bring nested macro.
 *
 * @result 8 bytes long(Reason is unknown) for C compilers and 1/2/4/8 bytes long for C++
 */
#	define CT_PACK_STRING_TO_UINT(str)\
	(\
		CT_STRLEN(str) == 0 ? ((uint8_t)CT_PACK_STRING_TO_UINT_EMPTY_STRING) :\
		CT_STRLEN(str) == 1 ? ((uint8_t)str[0]) :\
		CT_STRLEN(str) == 2 ? ((uint16_t)(((uint16_t)str[1] << 8) | ((uint16_t)str[0]))) :\
		CT_STRLEN(str) == 3 ? (((uint32_t)str[2] << 16) | ((uint32_t)str[1] << 8) | ((uint32_t)str[0])) :\
		CT_STRLEN(str) == 4 ? (((uint32_t)str[3] << 24) | ((uint32_t)str[2] << 16) | ((uint32_t)str[1] << 8)\
							   | ((uint32_t)str[0])) :\
		CT_STRLEN(str) == 5 ? (((uint64_t)str[4] << 32) | ((uint64_t)str[3] << 24) | ((uint64_t)str[2] << 16)\
							   | ((uint64_t)str[1] << 8) | ((uint64_t)str[0])) :\
		CT_STRLEN(str) == 6 ? (((uint64_t)str[5] << 40) | ((uint64_t)str[4] << 32) | ((uint64_t)str[3] << 24)\
							   | ((uint64_t)str[2] << 16) | ((uint64_t)str[1] << 8) | ((uint64_t)str[0])) :\
		CT_STRLEN(str) == 7 ? (((uint64_t)str[6] << 48) | ((uint64_t)str[5] << 40) | ((uint64_t)str[4] << 32)\
							   | ((uint64_t)str[3] << 24) | ((uint64_t)str[2] << 16) | ((uint64_t)str[1] << 8)\
							   | ((uint64_t)str[0])) :\
		CT_STRLEN(str) == 8 ? ((((uint64_t)str[7]) << 56) | ((uint64_t)str[6] << 48) | ((uint64_t)str[5] << 40)\
							   | ((uint64_t)str[4] << 32) | ((uint64_t)str[3] << 24) | ((uint64_t)str[2] << 16)\
							   | ((uint64_t)str[1] << 8) | ((uint64_t)str[0])) :\
		((uint64_t)CT_PACK_STRING_TO_UINT_TOO_LONG_STRING)\
	)

#endif	// CT_PACK_STRING_TO_UINT_AVAILABLE
