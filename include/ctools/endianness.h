/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 * We can't detect endianness at compile time because there are systems with mixed endianness
 */

#pragma once

#include "ctools/predef/attributes.h"
#include "ctools/predef/cxx11_attributes.h"
#include "ctools/std/stdint.h"
#include "ctools/namespace.h"


CT_BEGIN_NAMESPACE

CT_ENUM_CLASS Endianness
{
	enUnknown = 0,
	enBig = 1,
	enLittle = 2,
	enBigWord = 3,		/* Middle-endian, Honeywell 316 style */
	enLittleWord = 4	/* Middle-endian, PDP-11 style */
};

// Borrowed from http://sourceforge.net/p/predef/wiki/Endianness
static CT_FORCEINLINE enum Endianness getRuntimeSystemEndianness(void) CT_NOEXCEPT
{
	union TestNumber
	{
		uint32_t value;
		uint8_t byteArray[sizeof(uint32_t)];
	} testNumber;
	enum Endianness endianness;

	testNumber.byteArray[0] = 0x00;
	testNumber.byteArray[1] = 0x01;
	testNumber.byteArray[2] = 0x02;
	testNumber.byteArray[3] = 0x03;

	switch (testNumber.value)
	{
		case UINT32_C(0x00010203):
			endianness = enBig;
			break;

		case UINT32_C(0x03020100):
			endianness = enLittle;
			break;

		case UINT32_C(0x02030001):
			endianness = enBigWord;
			break;

		case UINT32_C(0x01000302):
			endianness = enLittleWord;
			break;

		default:
			endianness = enUnknown;
	}

	return endianness;
}

CT_END_NAMESPACE
