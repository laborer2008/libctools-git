/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 * This file was taken from the at91lib_20100901_softpack_1_9_v_1_0_svn_v15011.zip file
 * (latest available at91lib from Atmel's site). Many things are not actual for new projects.
 * Because of this file was completely reworked. C++ support is added.
 * Now it requires C99/C++98 compalible compiler.
 */

/* ---------------------------------------------------------------------------------------------------------------------
 *         ATMEL Microcontroller Software Support
 * ---------------------------------------------------------------------------------------------------------------------
 * Copyright (c) 2008, Atmel Corporation
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer below.
 *
 * Atmel's name may not be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * DISCLAIMER: THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * ---------------------------------------------------------------------------------------------------------------------
 */

#pragma once

// ---------------------------------------------------------------------------------------------------------------------
/// \unit
///
/// !Purpose
///
/// Standard output methods for reporting debug information, warnings and
/// errors, which can be easily be turned on/off.
///
/// !Usage
/// -# Uses the CT_TRACE_DEBUG(), CT_TRACE_INFO(), CT_TRACE_WARNING(), CT_TRACE_ERROR()
///    CT_TRACE_FATAL() macros to output traces throughout the program.
/// -# Each type of trace has a level : Debug 5, Info 4, Warning 3, Error 2
///    and Fatal 1. Disable a group of traces by changing the value of
///    CT_TRACE_LEVEL during compilation; traces with a level bigger than CT_TRACE_LEVEL
///    are not generated. To generate no trace, use the reserved value 0.
/// -# Trace disabling can be static or dynamic. If dynamic disabling is selected
///    the trace level can be modified in runtime. If static disabling is selected
///    the disabled traces are not compiled.
///
/// !Trace level description
/// -# CT_TRACE_DEBUG (5): Traces whose only purpose is for debugging the program,
///    and which do not produce meaningful information otherwise.
/// -# CT_TRACE_INFO (4): Informational trace about the program execution. Should
///    enable the user to see the execution flow.
/// -# CT_TRACE_WARNING (3): Indicates that a minor error has happened. In most case
///    it can be discarded safely; it may even be expected.
/// -# CT_TRACE_ERROR (2): Indicates an error which may not stop the program execution,
///    but which indicates there is a problem with the code.
/// -# CT_TRACE_FATAL (1): Indicates a major error which prevents the program from going
///    any further.
///
/// Traces compile time setup
///    CT_TRACE_LEVEL_NO_TRACE can be defined to turn off all the traces.
///    CT_TRACE_SYSLOG or CT_TRACE_TERMINAL or CT_TRACE_SYSLOG_AND_TERMINAL must be defined to point the traces direction.
///    CT_TRACE_PLACEMENT can be defined if trace location (file name and line) is needed.
///    CT_TRACE_PRECEDING_ENDL can be defined if CT_ENDL before message is more convenient
///      (i.e output source is singular and previous output could be lost as in RS232).
///    CT_DYN_TRACES=1 can be defined to use runtime trace level instead of compile-time.
///    CT_TRACE_LEVEL_NO_TRACE and CT_DYN_TRACES=1 are mutually exclusive (both shoudn't be passed simulteniously).
///    CT_TRACE_LEVEL can be defined if neither CT_TRACE_LEVEL_NO_TRACE nor CT_DYN_TRACES=1 are defined.

/*
 * TODO: Something should be done with \n and \r characters output to syslog.
 * rsyslog allows to turn off escaping of these characters
 * ($EscapeControlCharactersOnReceive in the /etc/rsyslog.conf).
 * New line will appear but for syslog it looks ugly.
 */

/*
 * TODO: syslog buffer (on linux at least) gets overflown when a lot of data is written.
 * Need to find something like fflush() or increase size of that buffer
 */

/*
 * TODO: Add output to files support
 */

#include "ctools/predef/external_headers.h"
#include "ctools/predef/os.h"

CT_BEGIN_EXTERNAL_HEADERS
#	include <errno.h>

#	ifdef CT_OS_UNIX
#		include <syslog.h>
#	endif
CT_END_EXTERNAL_HEADERS

#	if defined (CT_OS_WINDOWS)
//		UNICODE is needed here for FormatMessage()/WideCharToMultiByte()
#		define UNICODE
#			include "ctools/os/windows/windows.h"
#		undef UNICODE
#	endif

#include "ctools/predef/attributes.h"
#include "ctools/predef/cxx11_attributes.h"

#include "ctools/std/sys/types.h"
#include "ctools/std/stdio.h"
#include "ctools/std/string.h"

#include "ctools/bool_consts.h"
#include "ctools/common_errors.h"
#include "ctools/likely.h"
#include "ctools/namespace.h"
#include "ctools/warning.h"


#if defined (CT_DYN_TRACES) && (CT_DYN_TRACES != 1)
#	error Invalid CT_DYN_TRACES value
#endif

#ifdef CT_TRACE_LEVEL_NO_TRACE
#	if (CT_DYN_TRACES == 1)
#		error CT_TRACE_LEVEL_NO_TRACE and CT_DYN_TRACES == 1 are mutually exclusive (both shoud not be passed simulteniously)
#	endif

#	if defined (CT_TRACE_LEVEL) && defined (CT_COMPILE_WARNING_AVAILABLE)
		CT_COMPILE_WARNING("CT_TRACE_LEVEL is not required in this configuration")
#	endif

#	if defined (CT_TRACE_SYSLOG) && defined (CT_COMPILE_WARNING_AVAILABLE)
		CT_COMPILE_WARNING("CT_TRACE_SYSLOG is not required in this configuration")
#	endif

#	if defined (CT_TRACE_TERMINAL) && defined (CT_COMPILE_WARNING_AVAILABLE)
		CT_COMPILE_WARNING("CT_TRACE_TERMINAL is not required in this configuration")
#	endif

#	if defined (CT_TRACE_SYSLOG_AND_TERMINAL) && defined (CT_COMPILE_WARNING_AVAILABLE)
		CT_COMPILE_WARNING("CT_TRACE_SYSLOG_AND_TERMINAL is not required in this configuration")
#	endif

#	if defined (CT_TRACE_PLACEMENT) && defined (CT_COMPILE_WARNING_AVAILABLE)
		CT_COMPILE_WARNING("CT_TRACE_PLACEMENT is not required in this configuration")
#	endif
#else
#	if !defined (CT_TRACE_SYSLOG) && !defined (CT_TRACE_TERMINAL) && !defined (CT_TRACE_SYSLOG_AND_TERMINAL)
#		error CT_TRACE_SYSLOG or CT_TRACE_TERMINAL or CT_TRACE_SYSLOG_AND_TERMINAL must be defined
#	endif

#	if defined (CT_TRACE_SYSLOG) && defined (CT_TRACE_TERMINAL)
#		error CT_TRACE_SYSLOG or CT_TRACE_TERMINAL must be defined, but not both
#	endif

#	if defined (CT_TRACE_SYSLOG_AND_TERMINAL) && defined (CT_TRACE_TERMINAL)
#		error CT_TRACE_SYSLOG_AND_TERMINAL or CT_TRACE_TERMINAL must be defined, but not both
#	endif

#	if defined (CT_TRACE_SYSLOG_AND_TERMINAL) && defined (CT_TRACE_SYSLOG)
#		error CT_TRACE_SYSLOG_AND_TERMINAL or CT_TRACE_SYSLOG must be defined, but not both
#	endif

#	if (defined (CT_TRACE_SYSLOG_AND_TERMINAL) || defined (CT_TRACE_SYSLOG)) && (!defined (CT_OS_UNIX))
#		error Definition of CT_TRACE_SYSLOG_AND_TERMINAL or CT_TRACE_SYSLOG makes sense only on unix os
#	endif
#endif

#if defined (CT_DYN_TRACES) && (CT_DYN_TRACES == 1)
#	if defined (CT_TRACE_LEVEL) && defined (CT_COMPILE_WARNING_AVAILABLE)
		CT_COMPILE_WARNING("CT_TRACE_LEVEL is not required when CT_DYN_TRACES is defined")
#	endif
#endif

CT_BEGIN_NAMESPACE

extern void traceFatalAction(void) CT_NOEXCEPT;

#define CT_ENDL "\r\n"

/*
 * CT_ENDL can be printed before or after any message.
 */
#if defined (CT_TRACE_PRECEDING_ENDL)
/*
 *	It's more convenient for bare metal systems
 *	where RS232 terminal can be diconnected at any time (Previous CT_ENDL could be lost).
 */
#	define CT_ENDL_LEFT__ CT_ENDL
#	define CT_ENDL_RIGHT__
#else
#	define CT_ENDL_LEFT__
#	define CT_ENDL_RIGHT__ CT_ENDL
#endif

// By default, all traces are output except the debug one.
#if !defined (CT_TRACE_LEVEL) && !defined (CT_TRACE_LEVEL_NO_TRACE)
#	define CT_TRACE_LEVEL CT_TRACE_LEVEL_INFO
#endif

#define CT_TRACE_LEVEL_DEBUG		5
#define CT_TRACE_LEVEL_INFO			4
#define CT_TRACE_LEVEL_WARNING		3
#define CT_TRACE_LEVEL_ERROR		2
#define CT_TRACE_LEVEL_FATAL		1

#ifndef CT_TRACE_LEVEL_NO_TRACE
#	define CT_TRACE_LEVEL_NO_TRACE	0
#endif

// By default, trace level is static (not dynamic)
#if !defined (CT_DYN_TRACES)
#	define CT_DYN_TRACES 0
#endif

#if defined (CT_NOTRACE)
#	error "Error: CT_NOTRACE has to be not defined!"
#endif

#if (CT_DYN_TRACES == 0)
#	if (CT_TRACE_LEVEL == CT_TRACE_LEVEL_NO_TRACE)
#		define CT_NOTRACE
#	endif
#endif

// System error retrieving method
enum SystemErrCode
{
	secNo = 0,					// Don't use system error code
	secErrno = 1				// Use standard errno variable
#ifdef CT_OS_WINDOWS
	, secGetLastError = 2		// Use result of GetLastError() call
#endif
};

/*
 * Both errno and GetLastErro() result can be converted to this type
 * According to http://msdn.microsoft.com/en-us/library/ms681381%28v=VS.85%29.aspx range of values
 * for GetLastError() return value is 0..15999, 0xFFFFFFFF(-1), user defined(with bit 29).
 * We don't use here user-defined codes. So, int can represent value of both errno and GetLastError().
 */
typedef int CommonSystemErrorCode;

extern CommonSystemErrorCode getSystemErrorCode(const enum SystemErrCode systemErrCode) CT_NOEXCEPT;
extern char* getErrorMsg(const enum SystemErrCode systemErrCode, const CommonSystemErrorCode upErrorCode) CT_NOEXCEPT;
extern void freeErrorMsgBuf(const enum SystemErrCode systemErrCode, char* message) CT_NOEXCEPT;

extern const char* getCurrentFile(const char* fileName) CT_NOEXCEPT;
#define CT_GET_CURRENT_FILE()	CT_NAMESPACED(getCurrentFile(__FILE__))

/*
 * There are linking troubles when fprintf, flush and other such functions are used with Keil armcc
 */
#if defined (CT_ARCH_ARM) && defined (CT_NO_OS)
#	define CT_TRACE_FATAL_STREAM
#	define CT_TRACE_ERROR_STREAM
#	define CT_TRACE_WARNING_STREAM
#	define CT_TRACE_INFO_STREAM
#	define CT_TRACE_DEBUG_STREAM
#	define CT_TRACE_PERM_STREAM
#else
/*	!(defined (CT_ARCH_ARM) && defined (CT_NO_OS)) */
#	define CT_TRACE_FATAL_STREAM					stderr
#	define CT_TRACE_ERROR_STREAM					stderr
#	define CT_TRACE_WARNING_STREAM					stderr
#	define CT_TRACE_INFO_STREAM						stdout
#	define CT_TRACE_DEBUG_STREAM					stdout
#	define CT_TRACE_PERM_STREAM						stdout
#endif

extern void streamedPrintf(FILE* stream, const char* format, ...) CT_NOEXCEPT;

#ifdef CT_OS_WINDOWS
#	define CT_SYS_ERR_MSG__		", syscode=%d, system: \"%s\""
#else
#	define CT_SYS_ERR_MSG__		", errno=%d, system: \"%s\""
#endif

/*
 * Wrapper over the printf().
 * One gcc extension is used here.
 * It seems that armcc is also supports it.
 * CT_DPRINTFS == CT_DPRINTF Simple
 * TODO: It seems that http://stackoverflow.com/questions/5365440/variadic-macro-trick can help to port it to another compilers
 */
#ifdef CT_TRACE_PLACEMENT
#	define CT_DPRINTF(stream, sec, curSysCode, widePrint, format, ...)\
		do\
		{\
			char* printfMessage = CT_NAMESPACED(getErrorMsg(sec, curSysCode));\
			CT_USING_CT_NAMESPACE\
			streamedPrintf(stream, CT_ENDL_LEFT__ widePrint "%s:%d " format, CT_GET_CURRENT_FILE(), __LINE__, ## __VA_ARGS__);\
			streamedPrintf(stream, CT_SYS_ERR_MSG__ CT_ENDL_RIGHT__, curSysCode, printfMessage);\
			freeErrorMsgBuf(sec, printfMessage);\
		}\
		while (CT_FALSE)

#	define CT_DPRINTFS(stream, widePrint, format, ...)\
		do\
		{\
			CT_USING_CT_NAMESPACE\
			streamedPrintf(stream, CT_ENDL_LEFT__ widePrint "%s:%d " format, CT_GET_CURRENT_FILE(), __LINE__, ## __VA_ARGS__);\
			streamedPrintf(stream, "%s", CT_ENDL_RIGHT__);\
		}\
		while (CT_FALSE)
#else
#	define CT_DPRINTF(stream, sec, curSysCode, widePrint, format, ...)\
		do\
		{\
			char* printfMessage = CT_NAMESPACED(getErrorMsg(sec, curSysCode));\
			CT_USING_CT_NAMESPACE\
			streamedPrintf(stream, CT_ENDL_LEFT__ widePrint format, ## __VA_ARGS__);\
			streamedPrintf(stream, CT_SYS_ERR_MSG__ CT_ENDL_RIGHT__, curSysCode, printfMessage);\
			freeErrorMsgBuf(sec, printfMessage);\
		}\
		while (CT_FALSE)

#	define CT_DPRINTFS(stream, widePrint, format, ...)\
		do\
		{\
			CT_USING_CT_NAMESPACE\
			streamedPrintf(stream, CT_ENDL_LEFT__ widePrint format, ## __VA_ARGS__);\
			streamedPrintf(stream, "%s", CT_ENDL_RIGHT__);\
		}\
		while (CT_FALSE)
#endif

/**
 * It is supposed that only daemons should write to syslog (if they want).
 * This feature is available in POSIX.
 * @param level: one of LOG_EMERG, LOG_ALERT, LOG_CRIT, LOG_ERR, LOG_WARNING, LOG_NOTICE, LOG_INFO, LOG_DEBUG.
 * Associations:
 * CT_TRACE_DEBUG		~ LOG_DEBUG
 * CT_TRACE_INFO		~ LOG_INFO
 * CT_TRACE_WARNING		~ LOG_WARNING
 * CT_TRACE_ERROR		~ LOG_ERR
 * CT_TRACE_FATAL		~ LOG_EMERG
 * CT_TRACE_PERM		~ LOG_NOTICE
 *
 * DSYSLOGS == DSYSLOG simple
 * TODO: add associations checking
 */
#if (defined (CT_TRACE_SYSLOG) || defined (CT_TRACE_SYSLOG_AND_TERMINAL)) && defined (CT_OS_UNIX)
#	ifdef CT_TRACE_PLACEMENT
#		define CT_DSYSLOG(level, sec, curSysCode, format, ...)\
			do\
			{\
				char* message = CT_NAMESPACED(getErrorMsg(sec, curSysCode));\
				syslog(LOG_DAEMON | level, "%s:%d " format CT_SYS_ERR_MSG__, CT_GET_CURRENT_FILE(), __LINE__, ## __VA_ARGS__, curSysCode, message);\
				CT_NAMESPACED(freeErrorMsgBuf(sec, message));\
			}\
			while (CT_FALSE)

#		define CT_DSYSLOGS(level, format, ...)\
			do\
			{\
				syslog(LOG_DAEMON | level, "%s:%d " format, CT_GET_CURRENT_FILE(), __LINE__, ## __VA_ARGS__);\
			}\
			while (CT_FALSE)
#	else
#		define CT_DSYSLOG(level, sec, curSysCode, format, ...)\
			do\
			{\
				char* message = CT_NAMESPACED(getErrorMsg(sec, curSysCode));\
				syslog(LOG_DAEMON | level, format CT_SYS_ERR_MSG__, ## __VA_ARGS__, curSysCode, message);\
				CT_NAMESPACED(freeErrorMsgBuf(sec, message));\
			}\
			while (CT_FALSE)

#		define CT_DSYSLOGS(level, format, ...)\
			do\
			{\
				syslog(LOG_DAEMON | level, format, ## __VA_ARGS__);\
			}\
			while (CT_FALSE)
#	endif
#else
#	define CT_DSYSLOG(level, curSysCode, format, ...)		do {} while (CT_FALSE)
#	define CT_DSYSLOGS(level, format, ...)					do {} while (CT_FALSE)
#endif

/*
 * This macro helps to overcome MS VC++ __VA_ARGS__ trait.
 * See http://stackoverflow.com/a/5134656/4155055
 * Without it macro arguments are incorrect(shifted)
 */
#define CR_TRACE_EXPAND__(x)		x


#ifdef CT_NOTRACE_DEBUG
#	error CT_NOTRACE_DEBUG mist be undefind
#endif

#ifdef CT_NOTRACE_INFO
#	error CT_NOTRACE_INFO mist be undefind
#endif

#ifdef CT_NOTRACE_WARNING
#	error CT_NOTRACE_WARNING mist be undefind
#endif

#ifdef CT_NOTRACE_ERROR
#	error CT_NOTRACE_ERROR mist be undefind
#endif

#ifdef CT_NOTRACE_FATAL
#	error CT_NOTRACE_FATAL mist be undefind
#endif

// ---------------------------------------------------------------------------------------------------------------------
/// Outputs a formatted string using <printf> if the log level is high
/// enough. Can be disabled by defining CT_TRACE_LEVEL_NO_TRACE during compilation.
/// \param format  Formatted string to output.
/// \param ...  Additional parameters depending on formatted string.
// ---------------------------------------------------------------------------------------------------------------------
#if defined (CT_NOTRACE)

//	Empty macros
#	define CT_TRACE_DEBUG(...)				do {} while (CT_FALSE)
#	define CT_TRACE_INFO(...)				do {} while (CT_FALSE)
#	define CT_TRACE_WARNING(...)			do {} while (CT_FALSE)
#	define CT_TRACE_ERROR(sec, ...)			do {} while (CT_FALSE)
#	define CT_TRACE_FATAL(...)				do {CT_NAMESPACED(traceFatalAction());} while (CT_FALSE)
#	define CT_TRACE_PERM(...)				do {} while (CT_FALSE)

#	define CT_TRACE_DEBUG_WP(...)			do {} while (CT_FALSE)
#	define CT_TRACE_INFO_WP(...)			do {} while (CT_FALSE)
#	define CT_TRACE_WARNING_WP(...)			do {} while (CT_FALSE)
#	define CT_TRACE_ERROR_WP(sec, ...)		do {} while (CT_FALSE)
#	define CT_TRACE_FATAL_WP(...)			do {CT_NAMESPACED(traceFatalAction());} while (CT_FALSE)
#	define CT_TRACE_PERM_WP(...)			do {} while (CT_FALSE)

#	define CT_TRACE_FUNC(arg1)				do {} while (CT_FALSE)

#else
//	!defined (CT_NOTRACE)

#	if (CT_DYN_TRACES == 1)
//	Trace output depends on CT_NAMESPACED(traceLevel) value

#		define CT_TRACE_DEBUG_BASE(widePrint, ...)			do {if (CT_NAMESPACED(traceLevel) >= CT_TRACE_LEVEL_DEBUG)		{CT_DSYSLOGS(LOG_DEBUG, ## __VA_ARGS__); CR_TRACE_EXPAND__(CT_DPRINTFS(CT_TRACE_DEBUG_STREAM, widePrint, ## __VA_ARGS__));}} while (CT_FALSE)
#		define CT_TRACE_INFO_BASE(widePrint, ...)			do {if (CT_NAMESPACED(traceLevel) >= CT_TRACE_LEVEL_INFO)		{CT_DSYSLOGS(LOG_INFO, ## __VA_ARGS__); CR_TRACE_EXPAND__(CT_DPRINTFS(CT_TRACE_INFO_STREAM, widePrint, ## __VA_ARGS__));}} while (CT_FALSE)
#		define CT_TRACE_WARNING_BASE(widePrint, ...)		do {if (CT_NAMESPACED(traceLevel) >= CT_TRACE_LEVEL_WARNING)	{CT_DSYSLOGS(LOG_WARNING, ## __VA_ARGS__); CR_TRACE_EXPAND__(CT_DPRINTFS(CT_TRACE_WARNING_STREAM, widePrint, ## __VA_ARGS__));}} while (CT_FALSE)
#		define CT_TRACE_ERROR_BASE(sec, widePrint, ...)		do {if (CT_NAMESPACED(traceLevel) >= CT_TRACE_LEVEL_ERROR)		{const CT_NAMESPACED(CommonSystemErrorCode) kCurSysCode = CT_NAMESPACED(getSystemErrorCode(sec)); CR_TRACE_EXPAND__(CT_DSYSLOG(LOG_ERR, sec, kCurSysCode, ## __VA_ARGS__); CT_DPRINTF(CT_TRACE_ERROR_STREAM, sec, kCurSysCode, widePrint, ## __VA_ARGS__));}} while (CT_FALSE)
#		define CT_TRACE_FATAL_BASE(widePrint, ...)			do {if (CT_NAMESPACED(traceLevel) >= CT_TRACE_LEVEL_FATAL)		{CT_DSYSLOGS(LOG_EMERG, ## __VA_ARGS__); CR_TRACE_EXPAND__(CT_DPRINTFS(CT_TRACE_FATAL_STREAM, widePrint, ## __VA_ARGS__)); CT_NAMESPACED(traceFatalAction());}} while (CT_FALSE)
#		define CT_TRACE_PERM_BASE(widePrint, ...)			do {CT_DSYSLOGS(LOG_NOTICE, ## __VA_ARGS__); CR_TRACE_EXPAND__(CT_DPRINTFS(CT_TRACE_PERM_STREAM, widePrint, ## __VA_ARGS__));} while (CT_FALSE)

#		define CT_TRACE_FUNC(arg1)							do {if (CT_NAMESPACED(traceLevel) >= CT_TRACE_LEVEL_DEBUG)		{CT_DSYSLOGS(LOG_DEBUG, "%s: %s", CT_FUNCTION, arg1); CT_NAMESPACED(streamedPrintf(CT_TRACE_DEBUG_STREAM, CT_ENDL_LEFT__ "%s: %s" CT_ENDL_RIGHT__, CT_FUNCTION, arg1));}} while (CT_FALSE)

#	else
//	!(CT_DYN_TRACES == 1)
//	Trace compilation depends on CT_TRACE_LEVEL value

#		if (CT_TRACE_LEVEL >= CT_TRACE_LEVEL_DEBUG)
#			define CT_TRACE_DEBUG_BASE(widePrint, ...)		do {CT_DSYSLOGS(LOG_DEBUG, ## __VA_ARGS__); CR_TRACE_EXPAND__(CT_DPRINTFS(CT_TRACE_DEBUG_STREAM, widePrint, ## __VA_ARGS__));} while (CT_FALSE)
#			define CT_TRACE_FUNC(arg1)						do {CT_DSYSLOGS(LOG_DEBUG, "%s: %s", CT_FUNCTION, arg1); CT_NAMESPACED(streamedPrintf(CT_TRACE_DEBUG_STREAM, CT_ENDL_LEFT__ "%s: %s" CT_ENDL_RIGHT__, CT_FUNCTION, arg1));} while (CT_FALSE)
#		else
#			define CT_NOTRACE_DEBUG
#			define CT_TRACE_DEBUG_BASE(widePrint, ...)		do {} while (CT_FALSE)
#			define CT_TRACE_FUNC(arg1)						do {} while (CT_FALSE)
#		endif

#		if (CT_TRACE_LEVEL >= CT_TRACE_LEVEL_INFO)
#			define CT_TRACE_INFO_BASE(widePrint, ...)		do {CT_DSYSLOGS(LOG_INFO, ## __VA_ARGS__); CR_TRACE_EXPAND__(CT_DPRINTFS(CT_TRACE_INFO_STREAM, widePrint, ## __VA_ARGS__));} while (CT_FALSE)
#		else
#			define CT_NOTRACE_INFO
#			define CT_TRACE_INFO_BASE(widePrint, ...)		do {} while (CT_FALSE)
#		endif

#		if (CT_TRACE_LEVEL >= CT_TRACE_LEVEL_WARNING)
#			define CT_TRACE_WARNING_BASE(widePrint, ...)	do {CT_DSYSLOGS(LOG_WARNING, ## __VA_ARGS__); CR_TRACE_EXPAND__(CT_DPRINTFS(CT_TRACE_WARNING_STREAM, widePrint, ## __VA_ARGS__));} while (CT_FALSE)
#		else
#			define CT_NOTRACE_WARNING
#			define CT_TRACE_WARNING_BASE(widePrint, ...)	do {} while (CT_FALSE)
#		endif

#		if (CT_TRACE_LEVEL >= CT_TRACE_LEVEL_ERROR)
#			define CT_TRACE_ERROR_BASE(sec, widePrint, ...)	do {const CT_NAMESPACED(CommonSystemErrorCode) kCurSysCode = CT_NAMESPACED(getSystemErrorCode(sec)); CT_DSYSLOG(LOG_ERR, sec, kCurSysCode, ## __VA_ARGS__); CR_TRACE_EXPAND__(CT_DPRINTF(CT_TRACE_ERROR_STREAM, sec, kCurSysCode, widePrint, ## __VA_ARGS__));} while (CT_FALSE)
#		else
#			define CT_NOTRACE_ERROR
#			define CT_TRACE_ERROR_BASE(sec, widePrint, ...)	do {} while (CT_FALSE)
#		endif

#		if (CT_TRACE_LEVEL >= CT_TRACE_LEVEL_FATAL)
#			define CT_TRACE_FATAL_BASE(widePrint, ...)		do {CT_DSYSLOGS(LOG_EMERG, ## __VA_ARGS__); CR_TRACE_EXPAND__(CT_DPRINTFS(CT_TRACE_FATAL_STREAM, widePrint, ## __VA_ARGS__); CT_NAMESPACED(traceFatalAction()));} while (CT_FALSE)
#		else
#			define CT_NOTRACE_FATAL
#			define CT_TRACE_FATAL_BASE(widePrint, ...)		do {CT_NAMESPACED(traceFatalAction());} while (CT_FALSE)
#		endif

#		define CT_TRACE_PERM_BASE(widePrint, ...)			do {CT_DSYSLOGS(LOG_NOTICE, ## __VA_ARGS__); CR_TRACE_EXPAND__(CT_DPRINTFS(CT_TRACE_PERM_STREAM, widePrint, ## __VA_ARGS__));} while (CT_FALSE)
#	endif

//	BCC doesn't support '## __VA_ARGS__' extention
#	if defined (CT_COMPL_BCC)
#		define CT_TRACE_DEBUG(...)							CT_TRACE_DEBUG_BASE("-D- ", "%s", __VA_ARGS__)
#		define CT_TRACE_DEBUG_WP(...)						CT_TRACE_DEBUG_BASE("", "%s", __VA_ARGS__)

#		define CT_TRACE_INFO(...)							CT_TRACE_INFO_BASE("-I- ", "%s", __VA_ARGS__)
#		define CT_TRACE_INFO_WP(...)						CT_TRACE_INFO_BASE("", "%s", __VA_ARGS__)

#		define CT_TRACE_WARNING(...)						CT_TRACE_WARNING_BASE("-W- ", "%s", __VA_ARGS__)
#		define CT_TRACE_WARNING_WP(...)						CT_TRACE_WARNING_BASE("","%s", __VA_ARGS__)

#		define CT_TRACE_ERROR(sec, ...)						CT_TRACE_ERROR_BASE(sec, "-E- ", "%s", __VA_ARGS__)
#		define CT_TRACE_ERROR_WP(sec, ...)					CT_TRACE_ERROR_BASE(sec, "", "%s", __VA_ARGS__)

#		define CT_TRACE_FATAL(...)							CT_TRACE_FATAL_BASE("-F- ", "%s", __VA_ARGS__)
#		define CT_TRACE_FATAL_WP(...)						CT_TRACE_FATAL_BASE("", "%s", __VA_ARGS__)

#		define CT_TRACE_PERM(...)							CT_TRACE_PERM_BASE("-P- ", "%s", __VA_ARGS__)
#		define CT_TRACE_PERM_WP(...)						CT_TRACE_PERM_BASE("", "%s", __VA_ARGS__)
#	else
#		define CT_TRACE_DEBUG(...)							CT_TRACE_DEBUG_BASE("-D- ", ## __VA_ARGS__)
#		define CT_TRACE_DEBUG_WP(...)						CT_TRACE_DEBUG_BASE("", ## __VA_ARGS__)

#		define CT_TRACE_INFO(...)							CT_TRACE_INFO_BASE("-I- ", ## __VA_ARGS__)
#		define CT_TRACE_INFO_WP(...)						CT_TRACE_INFO_BASE("", ## __VA_ARGS__)

#		define CT_TRACE_WARNING(...)						CT_TRACE_WARNING_BASE("-W- ", ## __VA_ARGS__)
#		define CT_TRACE_WARNING_WP(...)						CT_TRACE_WARNING_BASE("", ## __VA_ARGS__)

#		define CT_TRACE_ERROR(sec, ...)						CT_TRACE_ERROR_BASE(sec, "-E- ", ## __VA_ARGS__)
#		define CT_TRACE_ERROR_WP(sec, ...)					CT_TRACE_ERROR_BASE(sec, "", ## __VA_ARGS__)

#		define CT_TRACE_FATAL(...)							CT_TRACE_FATAL_BASE("-F- ", ## __VA_ARGS__)
#		define CT_TRACE_FATAL_WP(...)						CT_TRACE_FATAL_BASE("", ## __VA_ARGS__)

#		define CT_TRACE_PERM(...)							CT_TRACE_PERM_BASE("-P- ", ## __VA_ARGS__)
#		define CT_TRACE_PERM_WP(...)						CT_TRACE_PERM_BASE("", ## __VA_ARGS__)
#	endif
#endif

// Trace error with "No" code
#define CT_TRACE_ERRORN(...)							CT_TRACE_ERROR(CT_NAMESPACED(secNo), ## __VA_ARGS__)
#define CT_TRACE_ERRORN_WP(...)							CT_TRACE_ERROR_WP(CT_NAMESPACED(secNo), ## __VA_ARGS__)

// Trace error with "C" (errno) code
#define CT_TRACE_ERRORC(...)							CT_TRACE_ERROR(CT_NAMESPACED(secErrno), ## __VA_ARGS__)
#define CT_TRACE_ERRORC_WP(...)							CT_TRACE_ERROR_WP(CT_NAMESPACED(secErrno), ## __VA_ARGS__)

#ifdef CT_OS_WINDOWS
//	Trace error with "Windows" (GetLastError) code
#	define CT_TRACE_ERRORW(...)							CT_TRACE_ERROR(CT_NAMESPACED(secGetLastError), ## __VA_ARGS__)
#	define CT_TRACE_ERRORW_WP(...)						CT_TRACE_ERROR_WP(CT_NAMESPACED(secGetLastError), ## __VA_ARGS__)
#endif


#define CT_TRACE_DEBUG_IF(expression, ...)				do {if (expression) CT_TRACE_DEBUG(__VA_ARGS__);} while (CT_FALSE)
#define CT_TRACE_DEBUG_WP_IF(expression, ...)			do {if (expression) CT_TRACE_DEBUG_WP(__VA_ARGS__);} while (CT_FALSE)

#define CT_TRACE_INFO_IF(expression, ...)				do {if (expression) CT_TRACE_INFO(__VA_ARGS__);} while (CT_FALSE)
#define CT_TRACE_INFO_WP_IF(expression, ...)			do {if (expression) CT_TRACE_INFO_WP(__VA_ARGS__);} while (CT_FALSE)

#define CT_TRACE_WARNING_IF(expression, ...)			do {if (CT_UNLIKELY(expression)) CT_TRACE_WARNING(__VA_ARGS__);} while (CT_FALSE)
#define CT_TRACE_WARNING_WP_IF(expression, ...)			do {if (CT_UNLIKELY(expression)) CT_TRACE_WARNING_WP(__VA_ARGS__);} while (CT_FALSE)

#define CT_TRACE_ERRORN_IF(expression, ...)				do {if (CT_UNLIKELY(expression)) CT_TRACE_ERRORN(__VA_ARGS__);} while (CT_FALSE)
#define CT_TRACE_ERRORN_WP_IF(expression, ...)			do {if (CT_UNLIKELY(expression)) CT_TRACE_ERRORN_WP(__VA_ARGS__);} while (CT_FALSE)

#define CT_TRACE_ERRORC_IF(expression, ...)				do {if (CT_UNLIKELY(expression)) CT_TRACE_ERRORC(__VA_ARGS__);} while (CT_FALSE)
#define CT_TRACE_ERRORC_WP_IF(expression, ...)			do {if (CT_UNLIKELY(expression)) CT_TRACE_ERRORC_WP(__VA_ARGS__);} while (CT_FALSE)

#ifdef CT_OS_WINDOWS
//	Trace error with "Windows" (GetLastError) code
#	define CT_TRACE_ERRORW_IF(expression, ...)			do {if (CT_UNLIKELY(expression)) CT_TRACE_ERRORW(__VA_ARGS__);} while (CT_FALSE)
#	define CT_TRACE_ERRORW_WP_IF(expression, ...)		do {if (CT_UNLIKELY(expression)) CT_TRACE_ERRORW_WP(__VA_ARGS__);} while (CT_FALSE)
#endif

#define CT_TRACE_FATAL_IF(expression, ...)				do {if (CT_UNLIKELY(expression)) CT_TRACE_FATAL(__VA_ARGS__);} while (CT_FALSE)
#define CT_TRACE_FATAL_WP_IF(expression, ...)			do {if (CT_UNLIKELY(expression)) CT_TRACE_FATAL_WP(__VA_ARGS__);} while (CT_FALSE)

#define CT_TRACE_PERM_IF(expression, ...)				do {if (expression) CT_TRACE_PERM(__VA_ARGS__);} while (CT_FALSE)
#define CT_TRACE_PERM_WP_IF(expression, ...)			do {if (expression) CT_TRACE_PERM_WP(__VA_ARGS__);} while (CT_FALSE)

// ---------------------------------------------------------------------------------------------------------------------
// Exported variables
// ---------------------------------------------------------------------------------------------------------------------
// Depending on CT_DYN_TRACES, traceLevel is a modifiable runtime variable
// or a define CT_TRACE_LEVEL
#if !defined (CT_NOTRACE) && (CT_DYN_TRACES == 1)
extern uint traceLevel;
#endif

extern enum CommonError initializeTraces(void) CT_NOEXCEPT;
extern enum CommonError finalizeTraces(void) CT_NOEXCEPT;

/**
 * Displays the content of the given frame on the Trace interface.
 * \param pBuffer  Pointer to the frame to dump.
 * \param size  Buffer size in bytes.
 */
extern void traceDumpFrame(const unsigned char* const pFrame, const uint size) CT_NOEXCEPT;

/**
 * Displays the content of the given buffer on the Trace interface.
 * \param pBuffer  Pointer to the buffer to dump.
 * \param size     Buffer size in bytes.
 * \param address  Start address to display
 */
extern void traceDumpMemory(unsigned char* pBuffer, const uint size, const uint address) CT_NOEXCEPT;

CT_END_NAMESPACE
