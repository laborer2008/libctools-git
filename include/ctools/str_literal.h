/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 * This header can be used for utf8 constants definition.
 * Everything is ok if all the sources have utf8 encoding.
 * Otherwise on Microsoft's platforms problems are possible.
 * That means CT_U8 definition is not portable. Use it with caution!
 */

#pragma once

#include "ctools/predef/compiler.h"
#include "ctools/predef/language.h"
#include "ctools/warning.h"


#ifdef CT_U8
#	error CT_U8 must be undefined
#endif

#if defined (CT_COMPL_MSVS)

// See https://connect.microsoft.com/VisualStudio/feedback/details/773186/pragma-execution-character-set-utf-8-didnt-support-in-vc-2012
#	if _MSC_VER == CT_COMPL_MSVC_STUDIO_2012
		CT_COMPILE_WARNING("CT_U8 macro may not work")
#	endif

// From definition point and till the file's end
__pragma(execution_character_set("utf-8"))

#	define CT_U8(string)		string

#elif defined (CT_LANG_C11) || defined (CT_LANG_CXX11)
#	define CT_U8(string)		u8 ## string
#else
#	error Don't use std_literal.h without compiler's unicode support
#endif
