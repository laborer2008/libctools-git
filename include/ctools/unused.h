/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#pragma once

#include "ctools/bool_consts.h"


#ifdef CT_UNUSED
#	error CT_UNUSED must be undefined
#endif

#define CT_UNUSED(variable)\
	do\
	{\
		(void)variable;\
	}\
	while (CT_FALSE)
