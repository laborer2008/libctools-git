/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#pragma once


#ifdef CT_STRUCT_MEMBER_SIZE
#	error CT_STRUCT_MEMBER_SIZE must be undefined
#endif

/**
 * @return size of field @param member from structure @param type in bytes
 * Borrowed from http://stackoverflow.com/a/3553321
 */
#define CT_STRUCT_MEMBER_SIZE(type, member)	sizeof(((type*)0)->member)
