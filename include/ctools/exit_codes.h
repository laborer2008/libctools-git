/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 * This module contains some codes that can be returned from the main() function
 */

#pragma once

#include "ctools/predef/cxx11_attributes.h"
#include "ctools/namespace.h"


CT_BEGIN_NAMESPACE

/*
 * It's not recommended to exceed 120.
 * See https://en.wikipedia.org/wiki/Exit_status
 */
CT_ENUM_CLASS ExitCode
{
	ecSuccess = 0,					// No error

	// Use this value to inherit the enum and extend the range of available values
	ecMaxExitCode = ecSuccess
};

// Use it within libraries-descendants
union InheritedExitCode
{
	enum ExitCode c;
	int value;
};

CT_END_NAMESPACE
