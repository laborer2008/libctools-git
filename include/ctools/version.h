/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#pragma once

#include "ctools/stringify.h"

#ifdef CT_VER_STR_DOT
#	error CT_VER_STR_DOT must be undefined
#endif


#define CT_VER_STR_DOT(hi, lo, build)\
	CT_STRINGIFY(hi) "." CT_STRINGIFY(lo) "." CT_STRINGIFY(build)
