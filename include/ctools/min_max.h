/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 * Calculation of minimum/maximum value from 2 arguments.
 * See http://graphics.stanford.edu/~seander/bithacks.html#IntegerMinOrMax
 * for jumpless macros. But it seems that modern compilers can do it:
 * http://stackoverflow.com/a/7475388
 * So we shouldn't care
 */

#pragma once

#include "ctools/predef/compiler.h"


#ifdef CT_MAX2
#	error CT_MAX2 must be undefined
#endif

#ifdef CT_MIN2
#	error CT_MIN2 must be undefined
#endif


#if defined (CT_COMPL_GCC_EMULATION) && !defined (CT_COMPL_ARMCC)

/*
 * See:
 * https://gcc.gnu.org/onlinedocs/gcc/Typeof.html
 * https://gcc.gnu.org/onlinedocs/cpp/Duplication-of-Side-Effects.html
 */

#	ifdef _a
#		error _a must be undefined
#	endif

#	ifdef _b
#		error _b must be undefined
#	endif

/* *INDENT-OFF* */
#	define CT_MAX2(a, b)\
		({\
			__typeof__(a) _a = (a);\
			__typeof__(b) _b = (b);\
			_a >= _b ? _a : _b;\
		})

#	define CT_MIN2(a, b)\
		({\
			__typeof__(a) _a = (a);\
			__typeof__(b) _b = (b);\
			_a < _b ? _a : _b;\
		})
/* *INDENT-ON* */

#else

#	define CT_MAX2(a, b)		((a) >= (b) ? (a) : (b))
#	define CT_MIN2(a, b)		((a) < (b) ? (a) : (b))

#endif	// defined (CT_COMPL_GCC_EMULATION)
