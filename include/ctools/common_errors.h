/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#pragma once

#include "ctools/predef/cxx11_attributes.h"
#include "ctools/namespace.h"


CT_BEGIN_NAMESPACE

CT_ENUM_CLASS CommonError
{
	ceOk = 0,					// No error
	ceNullPointer,
	ceOutOfMemory,
	ceIllegalArgument,
	ceIllegalState,
	ceIndexOutOfBounds,
	ceUnknownSwitchValue,
	ceInitializationError,
	ceNotImplemented = 10,

	// Use this value to inherit the enum and extend the range of available values
	ceMaxCommonError = ceNotImplemented
};

CT_END_NAMESPACE
