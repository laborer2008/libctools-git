/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 * Based on http://stackoverflow.com/questions/47981/how-do-you-set-clear-and-toggle-a-single-bit-in-c
 * [01.10.2013] (with modifications)
 */


#pragma once

#include "ctools/predef/cxx11_attributes.h"
#include "ctools/std/sys/types.h"
#include "ctools/namespace.h"


#ifdef CT_BIT_SET
#	error CT_BIT_SET must be undefined
#endif

#ifdef CT_BIT_CLEAR
#	error CT_BIT_CLEAR must be undefined
#endif

#ifdef CT_BIT_FLIP
#	error CT_BIT_FLIP must be undefined
#endif

#ifdef CT_BIT_CHECK
#	error CT_BIT_CHECK must be undefined
#endif


/* a = target variable (8, 16 or 32 bit), b = bit number to act upon 0-n */
#define CT_BIT_SET(a, b)			((a) |= (1U << (b)))
#define CT_BIT_CLEAR(a, b)			((a) &= ~(1U << (b)))
#define CT_BIT_FLIP(a, b)			((a) ^= (1U << (b)))
#define CT_BIT_CHECK(a, b)			((a) & (1U << (b)))


#ifdef CT_BITMASK_SET
#	error CT_BITMASK_SET must be undefined
#endif

#ifdef CT_BITMASK_CLEAR
#	error CT_BITMASK_CLEAR must be undefined
#endif

#ifdef CT_BITMASK_FLIP
#	error CT_BITMASK_FLIP must be undefined
#endif

#ifdef CT_BITMASK_CHECK
#	error CT_BITMASK_CHECK must be undefined
#endif

/* a = target variable, b = mask */
#define CT_BITMASK_SET(a, b)		((a) |= (b))
#define CT_BITMASK_CLEAR(a, b)		((a) &= (~(b)))
#define CT_BITMASK_FLIP(a, b)		((a) ^= (b))
#define CT_BITMASK_CHECK(a, b)		((a) & (b))

#ifdef CT_BITS_REPLACE
#	error CT_BITS_REPLACE must be undefined
#endif

/* a = target variable, b = mask, c = new bits */
#define CT_BITS_REPLACE(a, b, c)\
	(((a) & (~(b))) | ((c) & (b)))


CT_BEGIN_NAMESPACE

/// @return the number of 1-bits in @param x
int popCount(const uint x) CT_NOEXCEPT;

CT_END_NAMESPACE
