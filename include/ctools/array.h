/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#pragma once


#ifdef CT_ARRAY_SIZE
#	error CT_ARRAY_SIZE must be undefined
#endif

/**
 * @return Number of items in the @param array
 */
#define CT_ARRAY_SIZE(array)				(sizeof(array) / sizeof(array[0]))
