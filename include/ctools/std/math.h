/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#pragma once

#include "ctools/predef/external_headers.h"

CT_BEGIN_EXTERNAL_HEADERS
#	include <math.h>
CT_END_EXTERNAL_HEADERS

#include "ctools/predef/cxx11_attributes.h"
#include "ctools/predef/library.h"
#include "ctools/namespace.h"
#include "ctools/simple_round.h"


/* *INDENT-OFF* */
#ifndef M_PI
//	ARMCC
#	define M_PI		3.14159265358979323846
#endif
/* *INDENT-ON* */

#ifdef CT_STD_MATH_USE_SIMPLE_ROUND__
#	error CT_STD_MATH_USE_SIMPLE_ROUND__ must be undefined
#endif

#if defined (CT_LIBC_UCLIBC)
#	if CT_LIBC_UCLIBC_VERSION < 929
#		define CT_STD_MATH_USE_SIMPLE_ROUND__
#	endif
#elif defined (CT_LIBC_MSVCRT)
#	if _CPPLIB_VER < CT_LIBC_MSVCRT_2013
#		define CT_STD_MATH_USE_SIMPLE_ROUND__
#	endif
#endif

#if defined (CT_STD_MATH_USE_SIMPLE_ROUND__)

static inline long int lround(double x) CT_NOEXCEPT
{
	return CT_SIMPLE_LROUND(x);
}

static inline long int lroundf(float x) CT_NOEXCEPT
{
	return CT_SIMPLE_LROUNDF(x);
}

static inline long int lroundl(long double x) CT_NOEXCEPT
{
	return CT_SIMPLE_LROUNDL(x);
}

static inline long long int llround(double x) CT_NOEXCEPT
{
	return CT_SIMPLE_LLROUND(x);
}

static inline long long int llroundf(float x) CT_NOEXCEPT
{
	return CT_SIMPLE_LLROUNDF(x);
}

static inline long long int llroundl(long double x) CT_NOEXCEPT
{
	return CT_SIMPLE_LLROUNDL(x);
}

#endif

CT_BEGIN_NAMESPACE

static inline int8_t roundf8(float x) CT_NOEXCEPT
{
	return CT_SIMPLE_ROUNDF8(x);
}

static inline uint8_t uroundf8(float x) CT_NOEXCEPT
{
	return CT_SIMPLE_UROUNDF8(x);
}

static inline int16_t roundf16(float x) CT_NOEXCEPT
{
	return CT_SIMPLE_ROUNDF16(x);
}

static inline uint16_t uroundf16(float x) CT_NOEXCEPT
{
	return CT_SIMPLE_UROUNDF16(x);
}

static inline int32_t roundf32(float x) CT_NOEXCEPT
{
	return CT_SIMPLE_ROUNDF32(x);
}

static inline uint32_t uroundf32(float x) CT_NOEXCEPT
{
	return CT_SIMPLE_UROUNDF32(x);
}

static inline int64_t roundf64(float x) CT_NOEXCEPT
{
	return CT_SIMPLE_ROUNDF64(x);
}

static inline uint64_t uroundf64(float x) CT_NOEXCEPT
{
	return CT_SIMPLE_UROUNDF64(x);
}


static inline int8_t round8(double x) CT_NOEXCEPT
{
	return CT_SIMPLE_ROUND8(x);
}

static inline uint8_t uround8(double x) CT_NOEXCEPT
{
	return CT_SIMPLE_UROUND8(x);
}

static inline int16_t round16(double x) CT_NOEXCEPT
{
	return CT_SIMPLE_ROUND16(x);
}

static inline uint16_t uround16(double x) CT_NOEXCEPT
{
	return CT_SIMPLE_UROUND16(x);
}

static inline int32_t round32(double x) CT_NOEXCEPT
{
	return CT_SIMPLE_ROUND32(x);
}

static inline uint32_t uround32(double x) CT_NOEXCEPT
{
	return CT_SIMPLE_UROUND32(x);
}

static inline int64_t round64(double x) CT_NOEXCEPT
{
	return CT_SIMPLE_ROUND64(x);
}

static inline uint64_t uround64(double x) CT_NOEXCEPT
{
	return CT_SIMPLE_UROUND64(x);
}

CT_END_NAMESPACE
