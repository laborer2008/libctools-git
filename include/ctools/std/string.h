/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#pragma once

#include "ctools/predef/external_headers.h"
#include "ctools/predef/os.h"


// strndup() on unix
#undef CT__USE_XOPEN2K8_WAS_DEFINED
#if !defined (__USE_XOPEN2K8)
#	define __USE_XOPEN2K8
#	define CT__USE_XOPEN2K8_WAS_DEFINED	1
#endif

#undef CT_GNU_SOURCE_WAS_DEFINED
#if !defined (_GNU_SOURCE)
#	define _GNU_SOURCE
#	define CT_GNU_SOURCE_WAS_DEFINED	1
#endif

CT_BEGIN_EXTERNAL_HEADERS
#	include <string.h>
CT_END_EXTERNAL_HEADERS

#if CT__USE_XOPEN2K8_WAS_DEFINED
#	undef __USE_XOPEN2K8
#	undef CT__USE_XOPEN2K8_WAS_DEFINED
#endif

#if CT_GNU_SOURCE_WAS_DEFINED
#	undef _GNU_SOURCE
#	undef CT_GNU_SOURCE_WAS_DEFINED
#endif

#include "ctools/predef/attributes.h"
#include "ctools/predef/cxx11_attributes.h"
#include "ctools/namespace.h"

#if defined (CT_COMPL_SDCC)

extern char* strerror(int errnum) CT_NOEXCEPT;

#endif

CT_BEGIN_NAMESPACE

#if defined (CT_OS_WINDOWS)
#	define strdup(str)		_strdup(str)

extern char* strndup(const char* s, size_t n) CT_NOEXCEPT;

#endif

/**
 * "Safe" strlen with checking for NULL pointer.
 * @return the number of characters that precede the terminating '\0', or 0 if str is 0
 */
static CT_FORCEINLINE size_t ctStrLen(const char* str) CT_NOEXCEPT
{
	return str ? strlen(str) : 0;
}

CT_END_NAMESPACE
