/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#pragma once

#include "ctools/predef/compiler.h"
#include "ctools/predef/external_headers.h"

#if defined (CT_COMPL_MSVC)

CT_BEGIN_EXTERNAL_HEADERS
#	include "ctools/std/msinttypes/inttypes.h"
CT_END_EXTERNAL_HEADERS

#elif defined (CT_COMPL_SDCC) || defined (CT_COMPL_BCC)
//	Emulate msvc here.
#	define _MSC_VER		999

#	include "ctools/std/stdint.h"

CT_BEGIN_EXTERNAL_HEADERS
#		include "ctools/std/msinttypes/inttypes.h"
CT_END_EXTERNAL_HEADERS

#	undef _MSC_VER
#else

CT_BEGIN_EXTERNAL_HEADERS
#	include <inttypes.h>
CT_END_EXTERNAL_HEADERS

#endif	// defined (CT_COMPL_MSVC)
