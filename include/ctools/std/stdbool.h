/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#pragma once

#include "ctools/predef/compiler.h"


#ifdef CT_STD_NEED_INCLUDE_STDBOOL__
#	error CT_STD_NEED_INCLUDE_STDBOOL__ must be undefined
#endif

#if defined (CT_COMPL_MSVC) || defined (CT_COMPL_MSVC_RC)
#	if defined (CT_COMPL_MSVC_RC) || (_MSC_VER < CT_COMPL_MSVC_STUDIO_2013)

//		stdbool.h/cstdbool are not supported at all

#		if defined (__cplusplus)
//			All constants are built-in in the C++ language.
#		else

/* *INDENT-OFF* */
#			define false	0
#			define true		1

//			typedef here may not work
#			define bool		int
/* *INDENT-ON* */

#		endif
#	else
#		define CT_STD_NEED_INCLUDE_STDBOOL__
#	endif
#else
#	define CT_STD_NEED_INCLUDE_STDBOOL__
#endif	// defined (CT_COMPL_MSVC)

#if defined (CT_STD_NEED_INCLUDE_STDBOOL__)

#	include "ctools/predef/external_headers.h"

CT_BEGIN_EXTERNAL_HEADERS
#		include <stdbool.h>
CT_END_EXTERNAL_HEADERS

#endif	// defined (CT_STD_NEED_INCLUDE_STDBOOL__)
