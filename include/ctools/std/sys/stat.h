/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#pragma once

#include "ctools/predef/os.h"

#if !defined (CT_NO_OS)

#	include "ctools/predef/external_headers.h"


//	Help mode_t values to be defined in c99 mode on unix
#	undef CT__USE_BSD_WAS_DEFINED
#	ifndef __USE_BSD
#		define __USE_BSD
#		define CT__USE_BSD_WAS_DEFINED	1
#	endif

//	Help mode_t values to be defined in c99 mode on unix
#	undef CT_USE_XOPEN_WAS_DEFINED
#	ifndef __USE_XOPEN
#		define __USE_XOPEN
#		define CT_USE_XOPEN_WAS_DEFINED	1
#	endif

CT_BEGIN_EXTERNAL_HEADERS
#		include <sys/stat.h>
CT_END_EXTERNAL_HEADERS

//	mode_t. Its values defined in stat.h and depends on additional definitions, e.g. _USE_BSD
#	include "ctools/std/sys/types.h"

#	ifdef CT__USE_BSD_WAS_DEFINED
#		undef __USE_BSD
#		undef CT__USE_BSD_WAS_DEFINED
#	endif

#	ifdef CT__USE_XOPEN_WAS_DEFINED
#		undef __USE_XOPEN
#		undef CT__USE_XOPEN_WAS_DEFINED
#	endif


#	include "ctools/predef/attributes.h"
#	include "ctools/predef/cxx11_attributes.h"
#	include "ctools/std/stdbool.h"
#	include "ctools/namespace.h"


#	ifdef CT_OS_WINDOWS
//		For both struct and function
#		define stat	_stat
#	endif

CT_BEGIN_NAMESPACE

#	ifdef CT_COMPL_MSVC
typedef unsigned int mode_t;
#	endif

enum {kChModOkResult = 0};
extern int chmodWrapper(const char* path, mode_t mode) CT_NOEXCEPT;

enum {kMkDirOkResult = 0};
extern int mkDirWrapper(const char* path, mode_t mode, const bool informAboutError) CT_NOEXCEPT;

enum {kStatOkResult = 0};
extern int statWrapper(const char* CT_RESTRICT path, struct stat* CT_RESTRICT buf) CT_NOEXCEPT;

CT_END_NAMESPACE

#endif	// !defined (CT_NO_OS)
