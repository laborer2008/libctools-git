/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 * Adds:
 *  - definition of uint type
 *  - definition of ssize_t type
 */

#pragma once

#include "ctools/predef/compiler.h"

#if !defined (CT_COMPL_ARMCC) && !defined (CT_COMPL_SDCC)

#	include "ctools/predef/external_headers.h"

CT_BEGIN_EXTERNAL_HEADERS
#		include <sys/types.h>
CT_END_EXTERNAL_HEADERS

#endif	// !defined (CT_COMPL_ARMCC) && !defined (CT_COMPL_SDCC)

#include "ctools/predef/os.h"


#ifdef CT_UINT_AVAILABLE
#	error CT_UINT_AVAILABLE must be undefined
#endif

#if defined (CT_COMPL_GCC_EMULATION) && !defined (CT_COMPL_ARMCC)
//	_DEFAULT_SOURCE replaces _SVID_SOURCE since glibc 2.19
#	if defined (_SVID_SOURCE) || defined (_DEFAULT_SOURCE)
#		define CT_UINT_AVAILABLE
#	endif
#endif


#ifdef CT_SSIZE_T_AVAILABLE
#	error CT_SSIZE_T_AVAILABLE must be undefined
#endif

#if defined (CT_COMPL_GCC_EMULATION) && !defined (CT_COMPL_ARMCC)
#	if defined (__ssize_t_defined) || defined (_SSIZE_T_DEFINED_) /*Android NDK*/ || defined (CT_COMPL_MINGW)
#		define CT_SSIZE_T_AVAILABLE
#	endif
#elif defined (CT_COMPL_BCC)
//	ssize_t is supported at least by RAD Studio XE6
#	define CT_SSIZE_T_AVAILABLE
#endif


#ifdef CT_UINT_AVAILABLE
#	undef CT_UINT_AVAILABLE
#else
typedef unsigned int uint;
#endif

#ifdef CT_SSIZE_T_AVAILABLE
#	undef CT_SSIZE_T_AVAILABLE
#else
typedef long ssize_t;
#endif

#if defined (CT_OS_LINUX)
#	include "ctools/predef/cxx11_attributes.h"
#	include "ctools/namespace.h"


CT_BEGIN_NAMESPACE

/*
 * Accroding to "man 3 gettid" that function is unimplemented in the glibc.
 * See also: https://sourceware.org/bugzilla/show_bug.cgi?id=6399
 * Perhaps gettid() should be out of cttools namespace. It was added in the first place for the android NDK's sake.
 * TODO: Should be tested there
 */
pid_t gettid(void) CT_NOEXCEPT;

CT_END_NAMESPACE

#endif	// defined (CT_OS_LINUX)
