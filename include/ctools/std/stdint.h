/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#pragma once

#include "ctools/predef/compiler.h"
#include "ctools/predef/external_headers.h"


#ifndef __STDC_LIMIT_MACROS
	#define __STDC_LIMIT_MACROS
#endif

#ifndef __STDC_CONSTANT_MACROS
	#define __STDC_CONSTANT_MACROS
#endif


#if defined (CT_COMPL_MSVC)

CT_BEGIN_EXTERNAL_HEADERS
#	include "ctools/std/msinttypes/stdint.h"
CT_END_EXTERNAL_HEADERS

#else

CT_BEGIN_EXTERNAL_HEADERS
#	include <stdint.h>
CT_END_EXTERNAL_HEADERS

#endif	// defined (CT_COMPL_MSVC)
