/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#pragma once

#include "ctools/predef/os.h"

#if !defined (CT_NO_OS)

#	ifdef CT_OS_UNIX

#		include "ctools/predef/external_headers.h"

CT_BEGIN_EXTERNAL_HEADERS
#			include <unistd.h>
CT_END_EXTERNAL_HEADERS

/*
 * We could include unistd.h for symlink() but on some systems(i.e. Gentoo 2015)
 * symlink woudn't get linked
 */
extern int symlink(const char* __from, const char* __to) __THROW __nonnull((1, 2)) __wur;

#	endif

#	include "ctools/predef/cxx11_attributes.h"
#	include "ctools/namespace.h"


CT_BEGIN_NAMESPACE

enum {kRmDirOkStatus = 0};
extern int rmdirWrapper(const char* path) CT_NOEXCEPT;

#	ifdef CT_OS_UNIX
enum {kSymlinkOkStatus = 0};
extern int symlinkWrapper(const char* path1, const char* path2) CT_NOEXCEPT;
#	else
// TODO: use CreateSymbolicLink()
#	endif

CT_END_NAMESPACE

#endif	// !defined (CT_NO_OS)
