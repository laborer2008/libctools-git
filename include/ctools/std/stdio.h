/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#pragma once

#include "ctools/predef/external_headers.h"

CT_BEGIN_EXTERNAL_HEADERS
#	include <stdio.h>
CT_END_EXTERNAL_HEADERS

#include "ctools/predef/cxx11_attributes.h"
#include "ctools/predef/os.h"
#include "ctools/std/stdbool.h"
#include "ctools/namespace.h"


#if defined (CT_COMPL_SDCC)

// Not supported, placeholder is instead here
struct CT_IO_FILE__
{
	// We can't have pointer to a non-allocated structure. So it should contain some fields with  non-zero length
	char field;
};

typedef struct CT_IO_FILE__ FILE;

// Both should point to one output stream
extern FILE* stdout;
extern FILE* stderr;

extern FILE* stdin;

#endif


#if !defined (CT_NO_OS)

CT_BEGIN_NAMESPACE

extern FILE* fopenWrapper(const char* path, const char* mode) CT_NOEXCEPT;

enum {kFcloseOkResult = 0};
extern int fcloseWrapper(FILE* fp) CT_NOEXCEPT;

enum {kRemoveOkResult = 0};
extern int removeWrapper(const char* pathName, const bool informAboutError) CT_NOEXCEPT;

enum {kRenameOkResult = 0};
extern int renameWrapper(const char* oldName, const char* newName) CT_NOEXCEPT;

CT_END_NAMESPACE

#endif	// !defined (CT_NO_OS)
