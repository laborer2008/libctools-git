/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 * Borrowed from OpenCV. CF_FUNC_DEFAULT can be used for setting
 * a default value for any C++ function but not a C function.
 */

#pragma once

#ifdef CT_FUNC_DEFAULT
#	error CT_FUNC_DEFAULT must be undefined
#endif

#ifdef __cplusplus
#	define CT_FUNC_DEFAULT(val)		= (val)
#else
#	define CT_FUNC_DEFAULT(val)
#endif
