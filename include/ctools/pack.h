/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 * Taken from http://stackoverflow.com/questions/1537964/visual-c-equivalent-of-gccs-attribute-packed
 * (17.10.2014).
 *
 * Usage example:
 * CT_PACK(
 * struct myStruct
 * {
 *     int a;
 *     int b;
 * });
 */

#pragma once

#include "ctools/predef/compiler.h"


#ifdef CT_PACK
#	error CT_PACK must be undefined
#endif

#ifdef CT_PACK_SUPPORTED
#	error CT_PACK_SUPPORTED must be undefined
#endif


/* *INDENT-OFF* */
#if defined (CT_COMPL_MSVC)
#	define CT_PACK(...)\
		__pragma(pack(push, 1))\
			__VA_ARGS__\
		__pragma(pack(pop))
#	define CT_PACK_SUPPORTED
#elif defined (CT_COMPL_GCC_EMULATION)
#	define CT_PACK(...)\
			__VA_ARGS__ __attribute((packed))
#	define CT_PACK_SUPPORTED
#elif defined (CT_COMPL_SDCC)
/*
 * Good comment about structures packing in SDCC:
 * Structures are packed by default by sdcc. Why should they not be? We are
 * talking about 8-bit micro, aligning data items to anything other than 1
 * byte boundaries is (usually) useless.
 * See: https://sourceforge.net/p/sdcc/mailman/message/21449458/
 */
#	define CT_PACK(...)		__VA_ARGS__
#	define CT_PACK_SUPPORTED
#elif defined (CT_COMPL_BCC)
//	Not supported
#else
#	define CT_PACK(...)\
		_Pragma("pack(push, 1)")\
			__VA_ARGS__\
		_Pragma("pack(pop)")
#	define CT_PACK_SUPPORTED
#endif
/* *INDENT-ON* */
