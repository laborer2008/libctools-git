/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 * This file was taken from the at91lib_20100901_softpack_1_9_v_1_0_svn_v15011.zip file
 * (latest available at91lib from Atmel's site). Many things are not actual for new projects.
 * Because of this file was completely reworked. C++ support is added.
 * Now it requires C99/C++98 compalible compiler.
 */

/* ---------------------------------------------------------------------------------------------------------------------
 *         ATMEL Microcontroller Software Support
 * ---------------------------------------------------------------------------------------------------------------------
 * Copyright (c) 2008, Atmel Corporation
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer below.
 *
 * Atmel's name may not be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * DISCLAIMER: THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * ---------------------------------------------------------------------------------------------------------------------
 */

#pragma once

#include "ctools/predef/cxx11_attributes.h"
#include "ctools/std/sys/types.h"
#include "ctools/std/stdbool.h"
#include "ctools/namespace.h"


CT_BEGIN_NAMESPACE

// Reads an integer
extern bool consoleGetInteger(uint* const pValue) CT_NOEXCEPT;

// Reads an integer and check the value
extern bool consoleGetIntegerMinMax(uint* const pValue, const uint min, const uint max) CT_NOEXCEPT;

// Reads an hexadecimal number
extern bool consoleGetHexa32(uint* const pValue) CT_NOEXCEPT;

CT_END_NAMESPACE
