/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 * Portable compiler warnings.
 * Old compilers supports them poorly.
 * But it is incorrectly to ignore compiler messages. That means: "Don't use the module with old compilers"
 */

#pragma once

#include "ctools/predef/compiler.h"
#include "ctools/stringify.h"


#ifdef CT_COMPILE_MESSAGE
#	error CT_COMPILE_MESSAGE must be undefined
#endif

#ifdef CT_COMPILE_MESSAGE_AVAILABLE
#	error CT_COMPILE_MESSAGE_AVAILABLE must be undefined
#endif

#ifdef CT_COMPILE_WARNING
#	error CT_COMPILE_WARNING must be undefined
#endif

#ifdef CT_COMPILE_WARNING_AVAILABLE
#	error CT_COMPILE_WARNING_AVAILABLE must be undefined
#endif


/* *INDENT-OFF* */
#ifdef CT_COMPL_GCC_EMULATION
#	if CT_GCC_VERSION >= 40400
#		define CT_COMPILE_MESSAGE_AVAILABLE
#		define CT_COMPILE_MESSAGE(text)\
			_Pragma(CT_STRINGIFY2(message #text))
#	endif
#elif defined (CT_COMPL_MSVC)
#	if _MSC_VER >= CT_COMPL_MSVC_STUDIO_2008
#		define CT_COMPILE_MESSAGE_AVAILABLE
#		define CT_COMPILE_MESSAGE(text)\
			__pragma(message(#text))
#	endif
#endif
/* *INDENT-ON* */

/* *INDENT-OFF* */
#if defined (CT_COMPL_ICC)
#	define CT_COMPILE_WARNING_AVAILABLE
#	define CT_COMPILE_WARNING(text)\
		CT_COMPILE_MESSAGE(text)
#elif defined (CT_COMPL_GCC_EMULATION)
#	if CT_GCC_VERSION >= 40800
#		define CT_COMPILE_WARNING_AVAILABLE
#		define CT_COMPILE_WARNING(text)\
			_Pragma(CT_STRINGIFY2(GCC warning #text))
#	elif CT_GCC_VERSION >= 40400
#		define CT_COMPILE_WARNING_AVAILABLE
#		define CT_COMPILE_WARNING(text)\
			CT_COMPILE_MESSAGE(text)
#	endif
#elif defined (CT_COMPL_MSVC)
#	ifdef CT_COMPILE_MESSAGE_AVAILABLE
#		define CT_COMPILE_WARNING_AVAILABLE
#		define CT_COMPILE_WARNING(text)\
			CT_COMPILE_MESSAGE(text)
#	endif
#endif
/* *INDENT-ON* */
