/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 * Borrowed from http://efesx.com/2010/07/17/variadic-macro-to-count-number-of-arguments (by kcb)
 * and from https://github.com/donmccaughey/va_args_count/blob/master/va_args_count.h
 *
 * Main macro is CT_VA_POSITIVE_ARGS_COUNT - returns number of passed arguments.
 * There should be passed at least one argument
 */

#pragma once

#include "ctools/predef/compiler.h"


#ifdef CT_VA_ARGS_COUNT_VALUES
#	error CT_VA_ARGS_COUNT_VALUES must be undefined
#endif

#ifdef CT_VA_ARGS_SELECT_COUNT_VALUES
#	error CT_VA_ARGS_SELECT_COUNT_VALUES must be undefined
#endif

#ifdef CT_VA_POSITIVE_ARGS_COUNT
#	error CT_VA_POSITIVE_ARGS_COUNT must be undefined
#endif

#ifdef CT_VA_ARGS_COUNT_IMPL__
#	error CT_VA_ARGS_COUNT_IMPL__ must be undefined
#endif


#define CT_VA_ARGS_COUNT_VALUES\
							63, 62, 61, 60,\
	59, 58, 57, 56, 55, 54, 53, 52, 51, 50,\
	49, 48, 47, 46, 45, 44, 43, 42, 41, 40,\
	39, 38, 37, 36, 35, 34, 33, 32, 31, 30,\
	29, 28, 27, 26, 25, 24, 23, 22, 21, 20,\
	19, 18, 17, 16, 15, 14, 13, 12, 11, 10,\
	 9,  8,  7,  6,  5,  4,  3,  2,  1

#define CT_VA_ARGS_SELECT_COUNT_VALUES(\
			 _1,  _2,  _3,  _4,  _5,  _6,  _7,  _8,  _9, _10,\
			_11, _12, _13, _14, _15, _16, _17, _18, _19, _20,\
			_21, _22, _23, _24, _25, _26, _27, _28, _29, _30,\
			_31, _32, _33, _34, _35, _36, _37, _38, _39, _40,\
			_41, _42, _43, _44, _45, _46, _47, _48, _49, _50,\
			_51, _52, _53, _54, _55, _56, _57, _58, _59, _60,\
			_61, _62, _63,\
			N, ...\
	)	N

#define CT_VA_POSITIVE_ARGS_COUNT(...)		(CT_VA_ARGS_COUNT_IMPL__((__VA_ARGS__, CT_VA_ARGS_COUNT_VALUES)))
#define CT_VA_ARGS_COUNT_IMPL__(arg)		CT_VA_ARGS_SELECT_COUNT_VALUES arg

#ifndef CT_COMPL_MSVC
#	define CT_VA_ARGS_COUNT(...)\
		(\
			sizeof(# __VA_ARGS__) == sizeof("")\
			?\
				0\
			:\
				CT_VA_POSITIVE_ARGS_COUNT(__VA_ARGS__)\
		)
#endif
