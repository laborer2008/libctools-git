/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 * Crossplatform mutex realization
 */

#pragma once

#include "ctools/predef/cxx11_attributes.h"
#include "ctools/predef/c11_threads.h"
#include "ctools/predef/os.h"
#include "ctools/namespace.h"


#ifdef CT_THREAD_AVAILABLE
#	error CT_THREAD_AVAILABLE must be undefined
#endif

#if defined (CT_C11_THREADES_H_AVAILABLE)

#	include "ctools/predef/external_headers.h"

CT_BEGIN_EXTERNAL_HEADERS
#		include <threads.h>
CT_END_EXTERNAL_HEADERS


#	define CT_THREAD_AVAILABLE

CT_BEGIN_NAMESPACE

typedef thrd_t* ThreadPointer;

CT_END_NAMESPACE

#else
#	if defined (CT_COMPL_MSVC)

//		TODO: Processthreadsapi.h on Windows 8 and Windows Server 2012
#		include "ctools/os/windows/windows.h"


#		define CT_THREAD_AVAILABLE
#		define CT_KILL_THREAD_AVAILABLE

CT_BEGIN_NAMESPACE

typedef HANDLE ThreadPointer;	// void*

CT_END_NAMESPACE

#	elif defined (CT_COMPL_GCC_EMULATION)

#		include "ctools/predef/external_headers.h"

CT_BEGIN_EXTERNAL_HEADERS
#		include <pthread.h>
CT_END_EXTERNAL_HEADERS


#		define CT_THREAD_AVAILABLE
#		define CT_KILL_THREAD_AVAILABLE

CT_BEGIN_NAMESPACE

typedef pthread_t* ThreadPointer;

CT_END_NAMESPACE

#	endif	// defined (CT_COMPL_MSVC)
#endif	// defined (CT_C11_THREADS_H_AVAILABLE)

#if defined (CT_KILL_THREAD_AVAILABLE)

#	include "ctools/std/stdbool.h"


CT_BEGIN_NAMESPACE

// Random DWORD value
enum {kKilledThreadExitCode = 11238};

/**
 * Be cautious with it. Use it only as a backup solution.
 * Any acquired resources will not be released.
 * ThreadPointer on Windows is not a true pointer. Therefore double pointer is used here.
 * TODO: return system error code. Perhaps a structure(system error code + status) should be returned here.
 * @return true if operation was successful and nullified threadDoublePointer
 */
extern bool killThread(ThreadPointer* threadDoublePointer) CT_NOEXCEPT;

CT_END_NAMESPACE

#endif	// defined (CT_KILL_THREAD_AVAILABLE)
