/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#pragma once

#include "ctools/predef/c11_threads.h"
#include "ctools/predef/os.h"

#ifdef CT_YIELD_AVAILABLE
#	error CT_YIELD_AVAILABLE must be undefined
#endif

#if defined (CT_C11_THREADES_H_AVAILABLE)
#	define CT_YIELD_AVAILABLE
#elif defined (CT_OS_UNIX)
#	define CT_YIELD_AVAILABLE
#elif defined (CT_OS_WINDOWS)
#	define CT_YIELD_AVAILABLE
#endif

#if defined (CT_YIELD_AVAILABLE)

#	include "ctools/predef/cxx11_attributes.h"
#	include "ctools/namespace.h"


CT_BEGIN_NAMESPACE

extern void yield(void) CT_NOEXCEPT;

CT_END_NAMESPACE

#endif	// defined (CT_YIELD_AVAILABLE)
