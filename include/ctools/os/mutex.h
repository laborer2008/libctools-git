/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 * Crossplatform mutex realization
 */

#pragma once

#include "ctools/predef/cxx11_attributes.h"
#include "ctools/predef/c11_threads.h"
#include "ctools/predef/external_headers.h"
#include "ctools/predef/os.h"
#include "ctools/namespace.h"


#ifdef CT_MUTEX_AVAILABLE
#	error CT_MUTEX_AVAILABLE must be undefined
#endif

#if defined (CT_C11_THREADES_H_AVAILABLE)
#	define CT_MUTEX_AVAILABLE

CT_BEGIN_EXTERNAL_HEADERS
#	include <threads.h>
CT_END_EXTERNAL_HEADERS


CT_BEGIN_NAMESPACE

typedef mtx_t* MutexPointer;

CT_END_NAMESPACE

#else
#	if defined (CT_COMPL_MSVC)
#		define CT_MUTEX_AVAILABLE

#		include "ctools/os/windows/windows.h"


CT_BEGIN_NAMESPACE

typedef HANDLE MutexPointer;	// void*

CT_END_NAMESPACE

#	elif defined (CT_COMPL_GCC_EMULATION)
#		define CT_MUTEX_AVAILABLE

CT_BEGIN_EXTERNAL_HEADERS
#		include <pthread.h>
CT_END_EXTERNAL_HEADERS


CT_BEGIN_NAMESPACE

typedef pthread_mutex_t* MutexPointer;

CT_END_NAMESPACE

#	endif	// defined (CT_COMPL_MSVC)
#endif	// defined (CT_C11_THREADS_H_AVAILABLE)

#if defined (CT_MUTEX_AVAILABLE)

CT_BEGIN_NAMESPACE

// C++11 provides RAII mutex. Don't use these functions in the C++11 based program

extern MutexPointer createMutex(void) CT_NOEXCEPT;

// MutexPointer on Windows is not a true pointer. Therefore double pointer us used here
extern void releaseMutex(MutexPointer* mutexDoublePointer) CT_NOEXCEPT;

extern void lockMutex(MutexPointer mutexPointer) CT_NOEXCEPT;
extern void unLockMutex(MutexPointer mutexPointer) CT_NOEXCEPT;

CT_END_NAMESPACE

#endif	// defined (CT_MUTEX_AVAILABLE)
