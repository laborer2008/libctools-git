/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 * This file helps to use std::min() and std::max() with Visual C++
 */

#pragma once

#include "ctools/predef/compiler.h"
#include "ctools/predef/external_headers.h"


#if defined (CT_COMPL_MSVC) && defined (__cplusplus)

#	if !defined (NOMINMAX)
#		define NOMINMAX
#	endif

CT_BEGIN_EXTERNAL_HEADERS
#		include <windows.h>
CT_END_EXTERNAL_HEADERS

#	undef NOMINMAX

#else

CT_BEGIN_EXTERNAL_HEADERS
#	include <windows.h>
CT_END_EXTERNAL_HEADERS

#endif	// defined (CT_COMPL_MSVC) && defined (__cplusplus)
