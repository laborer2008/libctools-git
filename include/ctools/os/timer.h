/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 * This module contains functions which should be used for relative time measure.
 * Typical use case:
 *   - create RelativeTimePair variable (declare it);
 *   - call initTimer() to initialize it;
 *   - call startTimer() to start time measurement;
 *   - do something;
 *   - call stoptTimer() to stop time measurement;
 *   - call getElapsedTimeMSec() or getElapsedTimeSec() to get difference
 *     between two points in time.
 * ATTENTION! Return value has uint32_t type and can be easily overflowed.
 * For getElapsedTimeMSec(): UINT32_MAX/(kMilliSecsInSec * kSecsInMinute * kMinsInHour * kHoursInDay) ~ 54 days.
 * For getElapsedTimeSec(): 54 days * kMilliSecsInSec ~ 150 years.
 * Keep that in mind.
 * Absolute time measurement is not recommended here:
 *   - on Unix RelativeTimePair holds number of milliseconds/seconds since "the Epoch" (00:00:00 UTC, January 1, 1970);
 *   - on Windows RelativeTimePair holds number  of milliseconds/seconds since startup (uptime).
 * Also see getTickCount() from OpenCV:
 * https://github.com/Itseez/opencv/blob/7854ef3840b8216396b6138020404ea1e207d671/modules/core/src/system.cpp#L395-L435
 * TODO: Starting from Windows Vista GetTickCount64() could be used instead of GetTickCount().
 */

#pragma once

#include "ctools/predef/cxx11_attributes.h"
#include "ctools/predef/os.h"
#include "ctools/std/stdint.h"
#include "ctools/namespace.h"


#ifdef CT_TIMER_AVAILABLE
#	error CT_TIMER_AVAILABLE must be undefined
#endif

#if !defined (CT_NO_OS)
#	if defined (CT_OS_UNIX)
#		define CT_TIMER_AVAILABLE
#		include "ctools/predef/external_headers.h"


CT_BEGIN_EXTERNAL_HEADERS

//			Define timespec for C99 mode
#			ifndef __USE_POSIX199309
#				define __USE_POSIX199309
#				define CT_POSIX1993_WAS_DEFINED__
#			endif

#			include <time.h>


#			ifdef CT_POSIX1993_WAS_DEFINED__
#				undef __USE_POSIX199309
#				undef CT_POSIX1993_WAS_DEFINED__
#			endif
CT_END_EXTERNAL_HEADERS


CT_BEGIN_NAMESPACE

typedef struct timespec AbsoluteTimePoint;

CT_END_NAMESPACE

#	elif defined (CT_OS_WINDOWS)
#		define CT_TIMER_AVAILABLE

CT_BEGIN_NAMESPACE

typedef uint32_t AbsoluteTimePoint;

CT_END_NAMESPACE

#	endif
#endif	// !defined (CT_NO_OS)


#if defined (CT_TIMER_AVAILABLE)

CT_BEGIN_NAMESPACE

typedef struct
{
	AbsoluteTimePoint start;
	AbsoluteTimePoint end;
} RelativeTimePair;

extern void initTimer(RelativeTimePair* relativeTimePair) CT_NOEXCEPT;
extern void startTimer(RelativeTimePair* relativeTimePair) CT_NOEXCEPT;
extern void stopTimer(RelativeTimePair* relativeTimePair) CT_NOEXCEPT;

uint32_t getElapsedTimeInMilliSeconds(RelativeTimePair* relativeTimePair) CT_NOEXCEPT;
uint32_t getElapsedTimeInSeconds(RelativeTimePair* relativeTimePair) CT_NOEXCEPT;

CT_END_NAMESPACE

#endif	// defined (CT_TIMER_AVAILABLE)
