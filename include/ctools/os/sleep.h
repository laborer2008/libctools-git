/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 * Sleep function(accepts only milliseconds). It doesn't make sense to support here micro- or nanoseconds.
 * Such as accuracy is unachieved for ordinary processes working under control of modern OSes.
 * Hardware-specific methods should be used for that.
 */

#pragma once

#include "ctools/predef/c11_threads.h"
#include "ctools/predef/os.h"

#ifdef CT_SLEEP_AVAILABLE
#	error CT_SLEEP_AVAILABLE must be undefined
#endif

#if defined (CT_C11_THREADES_H_AVAILABLE)
#	define CT_SLEEP_AVAILABLE
#elif defined (CT_OS_UNIX)
#	define CT_SLEEP_AVAILABLE
#elif defined (CT_OS_WINDOWS)
#	define CT_SLEEP_AVAILABLE
#endif

#if defined (CT_SLEEP_AVAILABLE)

#	include "ctools/predef/cxx11_attributes.h"
#	include "ctools/std/stdint.h"
#	include "ctools/namespace.h"


CT_BEGIN_NAMESPACE

// Represents number of milliseconds
typedef uint32_t mseconds_t;

extern void msleep(const mseconds_t msec) CT_NOEXCEPT;

CT_END_NAMESPACE

#endif	// defined (CT_SLEEP_AVAILABLE)
