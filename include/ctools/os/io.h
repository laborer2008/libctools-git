/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#pragma once

#include "ctools/predef/os.h"

#if !defined (CT_NO_OS)

#	include "ctools/predef/external_headers.h"

CT_BEGIN_EXTERNAL_HEADERS
#		include <stddef.h>
CT_END_EXTERNAL_HEADERS

#	include "ctools/predef/cxx11_attributes.h"

#	include "ctools/std/sys/types.h"
#	include "ctools/std/stdbool.h"

#	include "ctools/namespace.h"


CT_BEGIN_NAMESPACE

/* *INDENT-OFF* */
#	if defined (CT_OS_UNIX)
		enum {kDirSeparator = '/'};
#	elif defined (CT_OS_WINDOWS)
		enum {kDirSeparator = '\\'};
#	else
#		error Unsupported platform
#	endif
/* *INDENT-ON* */

extern bool isFileExists(const char* const name) CT_NOEXCEPT;

/**
 * @return size of the file @param name in bytes.
 * Or -1 if some error happened
 */
extern ssize_t getFileSize(const char* const name) CT_NOEXCEPT;

/**
 * @param path source string
 * @param resolvedPath output string
 * @return the canonicalized absolute pathname
 *
 * The resulting path will have no symbolic link, /./ or /../ components.
 * It may be placed in the external memory resolvedPath
 * (at least PATH_MAX on Unix and  _MAX_PATH on Windows bytes long)
 * or allocated by malloc if resolvedPath == NULL. In that case the caller
 * should deallocate that buffer using free().
 * On Unix path should exists.
 */
extern char* getAbsolutePath(const char* const path, char* resolvedPath, const size_t bufferSize) CT_NOEXCEPT;

/**
 * @brief Create new directory. Top-level (parent) directory must exists.
 * @return false if directory was created or already exists
 * If some file with the same name already exists it considers as error
 */
extern bool createDir(const char* const path, const bool informAboutError) CT_NOEXCEPT;

/**
 * @brief Create sets of new directories. From root to leafs. Counterpart of "mkdir -p".
 * @return false if all directories was created or already exist
 * If some file with the same name in the path already exists it considers as error.
 * Supposedly @param path can be both absolute and relative. TODO: test it
 */
extern bool createPathDirs(const char* const path, const bool informAboutError) CT_NOEXCEPT;


CT_ENUM_CLASS AsciiTest
{
	atFileBinary,
	atFileAscii,
	atErrorHappened
};

/**
 * @brief Try to determine whether @param buffer (can be empty) contains ascii text or not
 * Unicode is not supported here. That's quite a task to solve simular problem for unicode files.
 * @return true if buffer is supposed to contain ascii text
 */
extern bool isBufferContainsAsciiText(const char* const buffer, const size_t bufferSize) CT_NOEXCEPT;

/**
 * @brief Read file @param path and try to determine whether it contains ascii text or not
 * Unicode is not supported here. That's quite a task to solve simular problem for unicode files.
 */
extern enum AsciiTest isFileContainsAsciiText(const char* const path) CT_NOEXCEPT;

CT_END_NAMESPACE

#endif	// !defined (CT_NO_OS)
