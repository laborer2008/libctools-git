/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#pragma once

#include "ctools/predef/cxx11_attributes.h"
#include "ctools/common_errors.h"
#include "ctools/namespace.h"


CT_BEGIN_NAMESPACE

extern enum CommonError initializeCtools(void) CT_NOEXCEPT;
extern enum CommonError finalizeCtools(void) CT_NOEXCEPT;

CT_END_NAMESPACE
