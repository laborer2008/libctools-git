/* *INDENT-OFF* */
/**************************************************************************
//
// Copyright 2013 Kangmo Kim, Nanolat Software.
//
// e-mail : kangmo@nanolat.com
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// c-callstack.h : Show Java-like callstack in C/C++ projects.
//
***************************************************************************/
/* *INDENT-ON* */

#pragma once

#include "ctools/predef/attributes.h"
#include "ctools/predef/build_type.h"
#include "ctools/trace.h"


#ifdef CT_RETURN
#	error CT_RETURN must be undefined
#endif

#if defined (CT_BT_RELEASE)
#	define CT_RETURN(code)		do {return (code);} while (CT_FALSE)
#elif defined (CT_BT_DEBUG)

/*
 * TODO: Print CT_ENDL for top-level functions
 */

/* *INDENT-OFF* */
#	define CT_RETURN(code)\
		{\
			if (code) {\
				CT_TRACE_ERRORN("Function \"%s\" error (return code: %d)", CT_FUNCTION, (int)(code));\
			}\
			return (code);\
		}
/* *INDENT-ON* */

#else
#	error Invalid build type
#endif	// defined (CT_BT_RELEASE)
