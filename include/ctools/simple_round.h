/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 * Counterparts of lround() function family from standard math.h
 * Can be used when they are unavailable on some platform
 */

#pragma once

#include "ctools/std/stdint.h"


#ifdef CT_SIMPLE_ROUND__
#	error CT_SIMPLE_ROUND__ must be undefined
#endif

#ifdef CT_SIMPLE_LROUND
#	error CT_SIMPLE_LROUND must be undefined
#endif

#ifdef CT_SIMPLE_LROUNDF
#	error CT_SIMPLE_LROUNDF must be undefined
#endif

#ifdef CT_SIMPLE_LROUNDL
#	error CT_SIMPLE_LROUNDL must be undefined
#endif

#ifdef CT_SIMPLE_LLROUND
#	error CT_SIMPLE_LLROUND must be undefined
#endif

#ifdef CT_SIMPLE_LLROUNDF
#	error CT_SIMPLE_LLROUNDF must be undefined
#endif

#ifdef CT_SIMPLE_LLROUNDL
#	error CT_SIMPLE_LLROUNDL must be undefined
#endif

#ifdef CT_SIMPLE_ROUNDF8
#	error CT_SIMPLE_ROUNDF8 must be undefined
#endif

#ifdef CT_SIMPLE_UROUNDF8
#	error CT_SIMPLE_UROUNDF8 must be undefined
#endif

#ifdef CT_SIMPLE_ROUNDF16
#	error CT_SIMPLE_ROUNDF16 must be undefined
#endif

#ifdef CT_SIMPLE_UROUNDF16
#	error CT_SIMPLE_UROUNDF16 must be undefined
#endif

#ifdef CT_SIMPLE_ROUNDF32
#	error CT_SIMPLE_ROUNDF32 must be undefined
#endif

#ifdef CT_SIMPLE_UROUNDF32
#	error CT_SIMPLE_UROUNDF32 must be undefined
#endif

#ifdef CT_SIMPLE_ROUNDF64
#	error CT_SIMPLE_ROUNDF64 must be undefined
#endif

#ifdef CT_SIMPLE_UROUNDF64
#	error CT_SIMPLE_UROUNDF64 must be undefined
#endif

#ifdef CT_SIMPLE_ROUND8
#	error CT_SIMPLE_ROUND8 must be undefined
#endif

#ifdef CT_SIMPLE_UROUND8
#	error CT_SIMPLE_UROUND8 must be undefined
#endif

#ifdef CT_SIMPLE_ROUND16
#	error CT_SIMPLE_ROUND16 must be undefined
#endif

#ifdef CT_SIMPLE_UROUND16
#	error CT_SIMPLE_UROUND16 must be undefined
#endif

#ifdef CT_SIMPLE_ROUND32
#	error CT_SIMPLE_ROUND32 must be undefined
#endif

#ifdef CT_SIMPLE_UROUND32
#	error CT_SIMPLE_UROUND32 must be undefined
#endif

#ifdef CT_SIMPLE_ROUND64
#	error CT_SIMPLE_ROUND64 must be undefined
#endif

#ifdef CT_SIMPLE_UROUND64
#	error CT_SIMPLE_UROUND64 must be undefined
#endif


#define CT_SIMPLE_ROUND__(tFloat, tInt, x)		((tInt)( (x) < (tFloat)0 ? (x) - (tFloat)0.5 : (x) + (tFloat)0.5))

#define CT_SIMPLE_LROUND(x)						CT_SIMPLE_ROUND__(double, long int, x)
#define CT_SIMPLE_LROUNDF(x)					CT_SIMPLE_ROUND__(float, long int, x)
#define CT_SIMPLE_LROUNDL(x)					CT_SIMPLE_ROUND__(long double, long int, x)

#define CT_SIMPLE_LLROUND(x)					CT_SIMPLE_ROUND__(double, long long int, x)
#define CT_SIMPLE_LLROUNDF(x)					CT_SIMPLE_ROUND__(float, long long int, x)
#define CT_SIMPLE_LLROUNDL(x)					CT_SIMPLE_ROUND__(long double, long long int, x)


// Non standard extensions

#define CT_SIMPLE_ROUNDF8(x)					CT_SIMPLE_ROUND__(float, int8_t, x)
#define CT_SIMPLE_UROUNDF8(x)					CT_SIMPLE_ROUND__(float, uint8_t, x)

#define CT_SIMPLE_ROUNDF16(x)					CT_SIMPLE_ROUND__(float, int16_t, x)
#define CT_SIMPLE_UROUNDF16(x)					CT_SIMPLE_ROUND__(float, uint16_t, x)

#define CT_SIMPLE_ROUNDF32(x)					CT_SIMPLE_ROUND__(float, int32_t, x)
#define CT_SIMPLE_UROUNDF32(x)					CT_SIMPLE_ROUND__(float, uint32_t, x)

#define CT_SIMPLE_ROUNDF64(x)					CT_SIMPLE_ROUND__(float, int64_t, x)
#define CT_SIMPLE_UROUNDF64(x)					CT_SIMPLE_ROUND__(float, uint64_t, x)


#define CT_SIMPLE_ROUND8(x)						CT_SIMPLE_ROUND__(double, int8_t, x)
#define CT_SIMPLE_UROUND8(x)					CT_SIMPLE_ROUND__(double, uint8_t, x)

#define CT_SIMPLE_ROUND16(x)					CT_SIMPLE_ROUND__(double, int16_t, x)
#define CT_SIMPLE_UROUND16(x)					CT_SIMPLE_ROUND__(double, uint16_t, x)

#define CT_SIMPLE_ROUND32(x)					CT_SIMPLE_ROUND__(double, int32_t, x)
#define CT_SIMPLE_UROUND32(x)					CT_SIMPLE_ROUND__(double, uint32_t, x)

#define CT_SIMPLE_ROUND64(x)					CT_SIMPLE_ROUND__(double, int64_t, x)
#define CT_SIMPLE_UROUND64(x)					CT_SIMPLE_ROUND__(double, uint64_t, x)
