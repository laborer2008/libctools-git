/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 * Binary constant generator macros evaluating to compile-time constants.
 * Based on work by Tom Torfs donated to the public domain.
 * See:
 *   - https://bytes.com/topic/c/answers/216333-binary-constant-macros
 *   - http://docs.ros.org/fuerte/api/sr_external_dependencies/html/binary_8h_source.html
 */


#pragma once

#include "ctools/std/stdint.h"


#ifdef CT_B8
#	error CT_B8 must be undefined
#endif

#ifdef CT_B16
#	error CT_B16 must be undefined
#endif

#ifdef CT_B32
#	error CT_B32 must be undefined
#endif

#ifdef CT_B64
#	error CT_B64 must be undefined
#endif

/*
 * For up to 8-bit binary constants
 */
#define CT_B8(b)		((uint8_t)CT_B8__(CT_HEX__(b)))

/*
 * For up to 16-bit binary constants, MSB first
 */
#define CT_B16(b1, b0)	(((uint16_t)CT_B8(b1) << 8) | CT_B8(b0))

/*
 * For up to 32-bit binary constants, MSB first
 */
#define CT_B32(b3, b2, b1, b0)	(((uint32_t)CT_B8(b3) << 24) | ((uint32_t)CT_B8(b2) << 16) |\
								 ((uint32_t)CT_B8(b1) << 8) | CT_B8(b0))

/*
 * For up to 64-bit binary constants, MSB first
 */
#define CT_B64(b7, b6, b5, b4, b3, b2, b1, b0)\
	(  ((uint64_t)CT_B8(b7) << 56) | ((uint64_t)CT_B8(b6) << 48)\
	 | ((uint64_t)CT_B8(b5) << 40) | ((uint64_t)CT_B8(b4) << 32)\
	 | ((uint64_t)CT_B8(b3) << 24) | ((uint64_t)CT_B8(b2) << 16)\
	 | ((uint64_t)CT_B8(b1) << 8)  | CT_B8(b0))

#ifdef CT_HEX__
#	error CT_HEX__ must be undefined
#endif

#ifdef CT_B8__
#	error CT_B8__ must be undefined
#endif

/*
 * Helper macros not to be used directly
 */
#define CT_HEX__(n)		0x ## n ## UL

/* *INDENT-OFF* */
#define CT_B8__(x)		(((x & 0x0000000FUL) ? 0x01 : 0) | ((x & 0x000000F0UL) ? 0x02 : 0) | ((x & 0x00000F00UL) ? 0x04 : 0) |\
						 ((x & 0x0000F000UL) ? 0x08 : 0) | ((x & 0x000F0000UL) ? 0x10 : 0) | ((x & 0x00F00000UL) ? 0x20 : 0) |\
						 ((x & 0x0F000000UL) ? 0x40 : 0) | ((x & 0xF0000000UL) ? 0x80 : 0))
/* *INDENT-ON* */
