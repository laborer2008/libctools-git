/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#pragma once

#include "ctools/predef/compiler.h"


#ifdef CT_LIKELY
#	error CT_LIKELY must be undefined
#endif

#ifdef CT_UNLIKELY
#	error CT_UNLIKELY must be undefined
#endif


/* *INDENT-OFF* */
#ifdef CT_COMPL_GCC_EMULATION
//	!! - double inversion to guarantee that !0 == 1
#	define CT_LIKELY(x)		(__builtin_expect(!!(x), 1))
#	define CT_UNLIKELY(x)	(__builtin_expect(!!(x), 0))
#else
#	define CT_LIKELY(x)		(x)
#	define CT_UNLIKELY(x)	(x)
#endif
/* *INDENT-ON* */
