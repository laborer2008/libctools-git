/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 * Time units conversion constants. They are useful to avoid using of magic constants
 */

#pragma once

#include "ctools/namespace.h"
#include "ctools/std/stdint.h"


/*
 * Derivative constants, all of uint32_t type.
 * It's impossible in the pure C to define enum's underlying type.
 * So we have to use defines here instead of enums
 */
#define CT_NANO_SECS_IN_MILLI_SEC\
	(((uint32_t)CT_NAMESPACED(kNanoSecsInMicroSec)) * ((uint16_t)CT_NAMESPACED(kMicroSecsInMilliSec)))

#define CT_NANO_SECS_IN_SEC\
	(CT_NANO_SECS_IN_MILLI_SEC * ((uint16_t)CT_NAMESPACED(kMilliSecsInSec)))

#define CT_MICRO_SECS_IN_SEC\
	(((uint32_t)CT_NAMESPACED(kMicroSecsInMilliSec)) * ((uint16_t)CT_NAMESPACED(kMilliSecsInSec)))


CT_BEGIN_NAMESPACE

/* *INDENT-OFF* */
// Base constants
enum {kPicoSecsInNanoSec	= 1000};
enum {kNanoSecsInMicroSec	= 1000};
enum {kMicroSecsInMilliSec	= 1000};
enum {kMilliSecsInSec		= 1000};
enum {kSecsInMinute			= 60};
enum {kMinsInHour			= 60};
enum {kHoursInDay			= 24};
enum {kMaxDaysInMonth		= 31};
enum {kMonthsInYear			= 12};

enum {kMinDay				= 1};
enum {kMinMonth				= 1};
/* *INDENT-ON* */

CT_END_NAMESPACE
