/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#pragma once

#if !defined (CT_NO_DYNAMIC_MEMORY)
#	include "ctools/predef/external_headers.h"

CT_BEGIN_EXTERNAL_HEADERS
#		include <stdlib.h>
CT_END_EXTERNAL_HEADERS

#endif	// !defined (CT_NO_DYNAMIC_MEMORY)

#include "ctools/predef/language.h"
#include "ctools/bool_consts.h"
#include "ctools/trace.h"


// See https://qt.gitorious.org/qt/qtbase/commit/51f7ef800ee84f9cbc0e1f742c2d869f887bd5ed

#ifdef CT_NULL
#	error CT_NULL must be undefined
#endif

/* *INDENT-OFF* */
#ifdef CT_LANG_CXX11
#	define CT_NULL		nullptr
#else
#	define CT_NULL		NULL

//	Fixed warning from clang static analyzer
#	ifdef CT_COMPL_CLANG
#		define __null	0
#	endif
#endif
/* *INDENT-ON* */


#ifdef CT_CHECK_PTR
#	error CT_CHECK_PTR must be undefined
#endif

#define CT_CHECK_PTR(pointer)\
	do\
	{\
		if (CT_UNLIKELY(!(pointer)))\
			CT_TRACE_ERRORN("Null pointer detected");\
	}\
	while (CT_FALSE)


#ifdef CT_CHECK_PTR_AND_RETURN
#	error CT_CHECK_PTR_AND_RETURN must be undefined
#endif

// Useful for controlling pointers at the function's beginning that returns void
#define CT_CHECK_PTR_AND_RETURN(pointer)\
	do\
	{\
		CT_CHECK_PTR(pointer);\
		if (CT_UNLIKELY(!(pointer)))\
			return;\
	}\
	while (CT_FALSE)


#ifdef CT_CHECK_PTR_AND_RETURN_RES
#	error CT_CHECK_PTR_AND_RETURN_RES must be undefined
#endif

// Useful for controlling pointers at the function's beginning that returns something
#define CT_CHECK_PTR_AND_RETURN_RES(pointer, result)\
	do\
	{\
		CT_CHECK_PTR(pointer);\
		if (CT_UNLIKELY(!(pointer)))\
			return (result);\
	}\
	while (CT_FALSE)


#ifdef CT_CHECK_PTR_AND_BREAK
#	error CT_CHECK_PTR_AND_BREAK must be undefined
#endif

// Useful for controlling pointers inside loops
/* *INDENT-OFF* */
#define CT_CHECK_PTR_AND_BREAK(pointer)\
	{\
		CT_CHECK_PTR(pointer);\
		if (CT_UNLIKELY(!(pointer)))\
			break;\
	}
/* *INDENT-ON* */

#if !defined (CT_NO_DYNAMIC_MEMORY)

#	ifdef CT_FREE_CLEAR_PTR
#		error CT_FREE_CLEAR_PTR must be undefined
#	endif

/* *INDENT-OFF* */
//	This macros avoids appearance of dangling pointers (memory is free but pointer is not)
#	define CT_FREE_CLEAR_PTR(pointer)\
		do\
		{\
			free(pointer);\
			pointer = CT_NULL;\
		}\
		while (CT_FALSE)
/* *INDENT-ON* */

#endif	// !defined (CT_NO_DYNAMIC_MEMORY)


#ifdef CT_CLEAR
#	error CT_CLEAR must be undefined
#endif

/// Clear an object
#define CT_CLEAR(x)							memset(&(x), 0, sizeof(x))
