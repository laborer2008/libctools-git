/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#pragma once

#include "ctools/std/sys/types.h"
#include "ctools/bool_consts.h"


#ifdef CT_POW2
#	error CT_POW2 must be undefined
#endif

/* *INDENT-OFF* */
/* Squaring */
#define CT_POW2(a)			((a) * (a))
/* *INDENT-ON* */

CT_BEGIN_NAMESPACE

/*
 * @brief Logarithm for integers
 */
static inline uint iLog2(const uint value) CT_NOEXCEPT
{
	uint l = 0;
	uint shifted = value;

	do
	{
		shifted >>= 1;

		if (shifted > 0)
			l++;
		else
			break;
	}
	while (CT_TRUE);

	return l;
}

CT_END_NAMESPACE
