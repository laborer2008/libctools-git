/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#pragma once

#include "ctools/predef/compiler.h"


#ifdef CT_COMPL_BCC
//	Auto-generated file
#	include "ctools/trace_bcc.h"
#else
#	include "ctools/trace_ext.h"
#endif
