/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 * generate_test_runner.rb won't compile appropriate test c file.
 * So ambiguous function moved here
 */

#include <ctools/predef/attributes.h>
#include <ctools/func_default.h>
#include <ctools/unused.h>


static CT_FORCEINLINE void testFunction(const int argument CT_FUNC_DEFAULT(0))
{
	CT_UNUSED(argument);
}
