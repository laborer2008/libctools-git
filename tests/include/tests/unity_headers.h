/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 * Borrowed from ctools/predef/external_headers.h
 */

#include <ctools/predef/compiler.h>


#if defined (CT_COMPL_MINGW)

#	define CT_BEGIN_UNITY_EXTERNAL_HEADERS
#	define CT_END_UNITY_EXTERNAL_HEADERS

#elif defined (CT_COMPL_CLANG)

#	define CT_BEGIN_UNITY_EXTERNAL_HEADERS
#	define CT_END_UNITY_EXTERNAL_HEADERS

#elif defined (CT_COMPL_ICC)

#	define CT_BEGIN_UNITY_EXTERNAL_HEADERS
#	define CT_END_UNITY_EXTERNAL_HEADERS

#elif defined (CT_COMPL_GCC)

#	define CT_BEGIN_UNITY_EXTERNAL_HEADERS
#	define CT_END_UNITY_EXTERNAL_HEADERS

#elif defined (CT_COMPL_MSVC)

#	define CT_BEGIN_UNITY_EXTERNAL_HEADERS\
		__pragma(warning(push, 0))\
		__pragma(warning(disable:4668))
#	define CT_END_UNITY_EXTERNAL_HEADERS\
		__pragma(warning(pop))

#else

#	define CT_BEGIN_UNITY_EXTERNAL_HEADERS
#	define CT_END_UNITY_EXTERNAL_HEADERS

#endif

CT_BEGIN_UNITY_EXTERNAL_HEADERS
#	include "unity.h"
CT_END_UNITY_EXTERNAL_HEADERS
