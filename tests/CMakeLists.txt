# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


cmake_minimum_required(VERSION 2.8.3)

set(PROJECT_NAME ctools_tests)
project(${PROJECT_NAME} ${CT_LANG})

include(${CMAKE_CURRENT_SOURCE_DIR}/../cmake/cmake_tools/tools/python_binary.cmake)

execute_process(COMMAND ${CMT_PYTHON_BINARY} ${CMAKE_CURRENT_LIST_DIR}/generate_runners.py)

set(EXECUTABLE_OUTPUT_PATH ${CT_TESTS_BIN_DIR})

include_directories(../include)
include_directories(include)
include_directories(unity/src)

function(generateTest headers dirs)
	#message("headers:")
	#message(${headers})

	#message("dirs:")
	#message(${dirs})

	set(DIRS_LIST ${dirs})

	set(RELATIVE_PATH "")
	set(RELATIVE_NAME "")
	foreach(DIR ${DIRS_LIST})
		#message("dirs list item:")
		#message(${DIR})
		set(RELATIVE_PATH "${RELATIVE_PATH}/${DIR}")
		set(RELATIVE_NAME "${RELATIVE_NAME}_${DIR}")
	endforeach()

	#message("relative path:")
	#message(${RELATIVE_PATH})

	#message("relative name:")
	#message(${RELATIVE_NAME})

	foreach(HEADER ${headers})
		get_filename_component(TEST_NAME ${HEADER} NAME_WE)
		#message("test name:")
		#message(${TEST_NAME})

		set(RUNNER_NAME "${CMAKE_CURRENT_LIST_DIR}/../build/runners${RELATIVE_PATH}/test_${TEST_NAME}_runner.c")
		#message("runner name:")
		#message(${RUNNER_NAME})

		set(TEST_C_NAME "${CMAKE_CURRENT_LIST_DIR}/${RELATIVE_PATH}/test_${TEST_NAME}.c")
		#message("test c name:")
		#message(${TEST_C_NAME})

		# Don't encumber to library to get linked
		if (${TEST_NAME} STREQUAL "ctools")
			set(TEST_NAME "${TEST_NAME}m")
		endif()

		if (NOT ${RELATIVE_NAME} STREQUAL "")
			string(SUBSTRING ${RELATIVE_NAME} 1 -1 RELATIVE_PREFIX)
			#message("relative prefix:")
			#message(${RELATIVE_PREFIX})

			set(TEST_NAME "${RELATIVE_PREFIX}_${TEST_NAME}")
			#message("test name:")
			#message(${TEST_NAME})
		endif()

		add_executable(${TEST_NAME} ${UNITY_SRC} ${RUNNER_NAME} ${TEST_C_NAME})
		target_link_libraries(${TEST_NAME} ctools)
	endforeach()
endfunction(generateTest)

file(GLOB_RECURSE UNITY_SRC unity/src/*.c)


set(TESTS_BUILD_INFO_INCLUDE_DIR "../build_info/include/ctools")

file(GLOB TESTS_INCLUDE			"${TESTS_BUILD_INFO_INCLUDE_DIR}/*.h")
generateTest("${TESTS_INCLUDE}" "build_info")


set(TESTS_INCLUDE_DIR "../include/ctools")

file(GLOB TESTS_INCLUDE			"${TESTS_INCLUDE_DIR}/*.h")
generateTest("${TESTS_INCLUDE}" "src")

file(GLOB TESTS_INCLUDE			"${TESTS_INCLUDE_DIR}/mcu/*.h")
generateTest("${TESTS_INCLUDE}" "src;mcu")

file(GLOB TESTS_INCLUDE			"${TESTS_INCLUDE_DIR}/os/*.h")
generateTest("${TESTS_INCLUDE}" "src;os")

file(GLOB TESTS_INCLUDE			"${TESTS_INCLUDE_DIR}/predef/*.h")
generateTest("${TESTS_INCLUDE}" "src;predef")

file(GLOB TESTS_INCLUDE			"${TESTS_INCLUDE_DIR}/std/*.h")
generateTest("${TESTS_INCLUDE}" "src;std")

file(GLOB TESTS_INCLUDE			"${TESTS_INCLUDE_DIR}/std/sys/*.h")
generateTest("${TESTS_INCLUDE}" "src;std;sys")

if (WIN32)
	file(GLOB TESTS_INCLUDE			"${TESTS_INCLUDE_DIR}/os/windows/*.h")
	generateTest("${TESTS_INCLUDE}" "src;os;windows")
endif()

set(CMAKE_VERBOSE_MAKEFILE true)
