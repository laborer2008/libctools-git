#!/usr/bin/env python3

"""
This script generates "runners". Runner is a small program that executes
all defined tests.

"""

__author__ = 'Sergey Gusarov'
__license__ = 'MPL 2.0, see LICENSE'


import fnmatch
import os
import subprocess
import sys

sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', 'pyrepo'))
import fs


scriptDir = fs.getScriptDir()

def kParentDir():
	return os.path.join(scriptDir, '..')

def kTestsSrcDir():
	return os.path.join(scriptDir, 'src')

def kTestsBuildInfoDir():
	return os.path.join(scriptDir, 'build_info')

def kCtoolsBuildDir():
	return os.path.join(kParentDir(), 'build')

def kTestsRunnersSrcDir():
	return os.path.join(kCtoolsBuildDir(), 'runners')

# -----------------------------------------------------------------------------------------------------------------------

def kIncludeCtoolsDir():
	return os.path.join(parentDir, 'include')

def kUnityDir():
	return os.path.join(scriptDir, 'unity')

def kUnityAutoDir():
	return os.path.join(kUnityDir(), 'auto')

def kUnitySrcDir():
	return os.path.join(kUnityDir(), 'src')

def kGeneratorScript():
	return os.path.join(kUnityAutoDir(), 'generate_test_runner.rb')

# ----------------------------------------------------------------------------------------------------------------------

def main():
	fs.cleanDir(kTestsRunnersSrcDir())
	fs.makeSurePathExists(kTestsRunnersSrcDir())

	for dirToWalk in [kTestsSrcDir(), kTestsBuildInfoDir()]:
		for dirPath, dirNames, files in os.walk(dirToWalk):
			for fileName in fnmatch.filter(files, '*'):
				#print('dirPath = ' + dirPath)
				#print('fileName = '+ fileName)

				#commonPrefix = os.path.commonprefix([dirPath, dirToWalk])
				#print('commonPrefix = ' + commonPrefix)

				relativePath = dirPath[len(scriptDir):]
				#print('relativePath = ' + relativePath)

				fullFileName = os.path.join(dirPath, fileName)

				if sys.platform == 'win32':
					cFullFileName = fullFileName.replace('\\', '\\\\')
				else:
					cFullFileName = fullFileName

				#print('cfullFileName = '+ cfullFileName)

				justFileName = os.path.splitext(fileName)[0]
				#print('justFileName = '+ justFileName)

				runnerFileName = justFileName + '_runner.c'
				runnerInDir = kTestsRunnersSrcDir() + relativePath

				if len(relativePath) > 0:
					fs.makeSurePathExists(runnerInDir)

				runnerInDir = os.path.join(runnerInDir, runnerFileName)
				#print('runnerInDir = '+ runnerInDir)

				subprocess.check_call('ruby ' + kGeneratorScript() + ' ' + cFullFileName + ' ' + runnerInDir, shell = True)

main()
