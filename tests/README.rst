===========================================================================
Тестирование `libctools <https://bitbucket.org/laborer2008/libctools>`_
===========================================================================

TODO

General Information
-------------------

It's supposed that simple C testing framework should be used.
Unity_ was chosen.

Dependencies
------------

Ruby_ programming language should be installed on the testing machine.


.. _Ruby : http://www.ruby-lang.org
.. _Unity : http://www.throwtheswitch.org/unity
