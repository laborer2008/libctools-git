/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "tests/unity_headers.h"

#include <ctools/struct.h>


void setUp(void)
{
}

void tearDown(void)
{
}

void testStructMemberSize(void)
{
	enum {kNumberOfItems = 256};

	typedef struct
	{
		float f;
		char c[kNumberOfItems];
		int i;
	} SomeStruct;

	TEST_ASSERT_EQUAL_UINT(CT_STRUCT_MEMBER_SIZE(SomeStruct, i), sizeof(int));
	TEST_ASSERT_EQUAL_UINT(CT_STRUCT_MEMBER_SIZE(SomeStruct, c), sizeof(char) * kNumberOfItems);
}
