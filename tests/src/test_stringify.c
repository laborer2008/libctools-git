/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "tests/unity_headers.h"

#include <ctools/predef/external_headers.h>

CT_BEGIN_EXTERNAL_HEADERS
#	include <string.h>
CT_END_EXTERNAL_HEADERS

#include <ctools/stringify.h>


void setUp(void)
{
}

void tearDown(void)
{
}

#define STR(a, b)	CT_STRINGIFY(a) "." CT_STRINGIFY(b)
#define NUM(a, b)	CT_NUMBERIZE(a) . CT_NUMBERIZE(b)

void testCompile(void)
{
	TEST_ASSERT_TRUE(strcmp(STR(0, 1), "0.1") == 0);
	TEST_ASSERT_TRUE(strcmp(CT_STRINGIFY(NUM(0, 1)), "0 . 1") == 0);
}
