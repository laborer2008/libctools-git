/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "tests/unity_headers.h"

#include <ctools/math.h>


void setUp(void)
{
}

void tearDown(void)
{
}

void testPow2(void)
{
	TEST_ASSERT_EQUAL_UINT(CT_POW2(5), 25);
}

void testIlog2(void)
{
	TEST_ASSERT_EQUAL_UINT(iLog2(0), 0);
	TEST_ASSERT_EQUAL_UINT(iLog2(1), 0);
	TEST_ASSERT_EQUAL_UINT(iLog2(2), 1);
	TEST_ASSERT_EQUAL_UINT(iLog2(3), 1);
	TEST_ASSERT_EQUAL_UINT(iLog2(4), 2);
	TEST_ASSERT_EQUAL_UINT(iLog2(5), 2);
	TEST_ASSERT_EQUAL_UINT(iLog2(6), 2);
	TEST_ASSERT_EQUAL_UINT(iLog2(7), 2);
	TEST_ASSERT_EQUAL_UINT(iLog2(8), 3);
}
