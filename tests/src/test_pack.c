/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "tests/unity_headers.h"

#include <ctools/pack.h>

#include <ctools/std/stdint.h>
#include <ctools/struct.h>


void setUp(void)
{
}

void tearDown(void)
{
}

void testCompile(void)
{
#if defined (CT_PACK_SUPPORTED)
	CT_PACK(
	struct Struct1__
	{
		int a;
		int8_t b;
	});

	typedef struct Struct1__ Struct1;

	TEST_ASSERT_EQUAL_UINT(sizeof(Struct1), CT_STRUCT_MEMBER_SIZE(Struct1, a) + CT_STRUCT_MEMBER_SIZE(Struct1, b));
#endif
}
