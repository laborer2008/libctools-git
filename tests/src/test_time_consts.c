/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "tests/unity_headers.h"

#include <ctools/time_consts.h>

#include <ctools/namespace.h>
#include <ctools/unused.h>


void setUp(void)
{
}

void tearDown(void)
{
}

void testCompile(void)
{
	uint32_t b;
	int a;

	CT_USING_CT_NAMESPACE

	a = kPicoSecsInNanoSec;
	CT_UNUSED(a);

	a = kNanoSecsInMicroSec;
	CT_UNUSED(a);

	a = kMicroSecsInMilliSec;
	CT_UNUSED(a);

	a = kMilliSecsInSec;
	CT_UNUSED(a);

	a = kSecsInMinute;
	CT_UNUSED(a);

	a = kMinsInHour;
	CT_UNUSED(a);

	a = kHoursInDay;
	CT_UNUSED(a);

	a = kMaxDaysInMonth;
	CT_UNUSED(a);

	a = kMonthsInYear;
	CT_UNUSED(a);

	a = kMinDay;
	CT_UNUSED(a);

	a = kMinMonth;
	CT_UNUSED(a);

	b = CT_NANO_SECS_IN_MILLI_SEC;
	CT_UNUSED(b);

	b = CT_NANO_SECS_IN_SEC;
	CT_UNUSED(b);

	b = CT_MICRO_SECS_IN_SEC;
	CT_UNUSED(b);
}
