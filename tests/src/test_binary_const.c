/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "tests/unity_headers.h"

#include <ctools/binary_const.h>
#include <ctools/std/stdint.h>


void setUp(void)
{
}

void tearDown(void)
{
}

void testAll(void)
{
	const uint8_t k8 = CT_B8(01010101);
	const uint16_t k16 = CT_B16(10101010, 01010101);
	const uint32_t k32 = CT_B32(10000000, 11111111, 10101010, 01010101);
	const uint64_t k64 = CT_B64(10001000, 10000000, 00000000, 10000000, 10000000, 11111111, 10101010, 01010101);

	TEST_ASSERT_TRUE(k8 == 85);
	TEST_ASSERT_TRUE(k16 == 43605);
	TEST_ASSERT_TRUE(k32 == 2164238933);
	TEST_ASSERT_TRUE(k64 == 0x8880008080FFAA55);
}
