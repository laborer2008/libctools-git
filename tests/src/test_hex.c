/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "tests/unity_headers.h"

#include <ctools/hex.h>
#include <ctools/string.h>


void setUp(void)
{
}

void tearDown(void)
{
}

void testHexCharacterToUint(void)
{
	const char kHexString1[] = "0123456789ABCDEF";
	const char kHexString2[] = "abcdef";
	size_t i;

	for (i = 0; i < sizeof(kHexString1) - 1; i++)
		TEST_ASSERT_EQUAL_UINT8(i, hexCharacterToUint(kHexString1[i]));

	for (i = 0; i < sizeof(kHexString2) - 1; i++)
		TEST_ASSERT_EQUAL_UINT8(i + 10, hexCharacterToUint(kHexString2[i]));

	TEST_ASSERT_EQUAL_UINT8(kInvalidHexCharacter, hexCharacterToUint('Z'));
	TEST_ASSERT_EQUAL_UINT8(kInvalidHexCharacter, hexCharacterToUint('<'));
	TEST_ASSERT_EQUAL_UINT8(kInvalidHexCharacter, hexCharacterToUint('.'));
}

void testHexPairToByte(void)
{
	TEST_ASSERT_EQUAL_UINT8(0, hexPairToByte('0', '0'));
	TEST_ASSERT_EQUAL_UINT8(26, hexPairToByte('A', '1'));
	TEST_ASSERT_EQUAL_UINT8(255, hexPairToByte('F', 'F'));

	TEST_ASSERT_EQUAL_UINT8(kInvalidHexPair, hexPairToByte('0', '.'));
	TEST_ASSERT_EQUAL_UINT8(kInvalidHexPair, hexPairToByte('G', 'A'));
}

void testCorrectString(void)
{
	#define HEX_STRING__		"04110430"
	#define HEX_STRING_LENGTH__	(CT_STRLEN(HEX_STRING__))

	uint8_t array[HEX_STRING_LENGTH__ / 2];
	bool isHexStringNotCorrect;

	isHexStringNotCorrect = hexStringToArray(array, HEX_STRING__, HEX_STRING_LENGTH__);

	TEST_ASSERT_FALSE(isHexStringNotCorrect);

	TEST_ASSERT_EQUAL_UINT8(array[0], 0x04);
	TEST_ASSERT_EQUAL_UINT8(array[1], 0x11);
	TEST_ASSERT_EQUAL_UINT8(array[2], 0x04);
	TEST_ASSERT_EQUAL_UINT8(array[3], 0x30);

	#undef HEX_STRING__
	#undef HEX_STRING_LENGTH__
}

void testIncorrectString(void)
{
	#define HEX_STRING__		"FF00Lm112233445566778899"
	#define HEX_STRING_LENGTH__	(CT_STRLEN(HEX_STRING__))

	uint8_t array[HEX_STRING_LENGTH__ / 2];
	bool isHexStringNotCorrect;

	isHexStringNotCorrect = hexStringToArray(array, HEX_STRING__, HEX_STRING_LENGTH__);

	TEST_ASSERT_TRUE(isHexStringNotCorrect);

	TEST_ASSERT_EQUAL_UINT8(array[0], 0xFF);
	TEST_ASSERT_EQUAL_UINT8(array[1], 0x00);

	#undef HEX_STRING__
	#undef HEX_STRING_LENGTH__
}
