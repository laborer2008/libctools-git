/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 * Complete test of all mutex functions should be done after all threads functions are implemented
 */

#include "tests/unity_headers.h"

#include <ctools/os/mutex.h>


void setUp(void)
{
}

void tearDown(void)
{
}

CT_USING_CT_NAMESPACE

void testCompile(void)
{
#ifdef CT_MUTEX_AVAILABLE
	MutexPointer mutexPointer;

	mutexPointer = createMutex();
	lockMutex(mutexPointer);
	unLockMutex(mutexPointer);
	releaseMutex(&mutexPointer);
#endif
}
