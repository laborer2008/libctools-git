/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "tests/unity_headers.h"

#include <ctools/os/sleep.h>


void setUp(void)
{
}

void tearDown(void)
{
}

void testCompile(void)
{
// See also test_timer

#ifdef CT_SLEEP_AVAILABLE
	CT_USING_CT_NAMESPACE

	msleep(100);
#endif
}
