/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "tests/unity_headers.h"

#include <ctools/os/timer.h>

#ifdef CT_TIMER_AVAILABLE
#	include <ctools/os/sleep.h>
#	include <ctools/time_consts.h>
#	include <ctools/trace.h>

CT_USING_CT_NAMESPACE
#endif


void setUp(void)
{
#if !defined (CT_NOTRACE) && (CT_DYN_TRACES == 1)
#	ifdef CT_TIMER_AVAILABLE
	traceLevel = CT_TRACE_LEVEL_DEBUG;
#	endif
#endif
}

void tearDown(void)
{
}

void testCompile(void)
{
#ifdef CT_TIMER_AVAILABLE
	const mseconds_t kMsSleepTime = 100;
	const int ksSleepTime = 3;

	uint32_t tick1;
	uint32_t tick2;
	uint32_t tickS1;
	uint32_t tickS2;

	RelativeTimePair relativeTimePair;

	initTimer(&relativeTimePair);

	startTimer(&relativeTimePair);
	stopTimer(&relativeTimePair);
	tick1 = getElapsedTimeInMilliSeconds(&relativeTimePair);
	tickS1 = getElapsedTimeInSeconds(&relativeTimePair);

	msleep(kMsSleepTime);
	stopTimer(&relativeTimePair);
	tick2 = getElapsedTimeInMilliSeconds(&relativeTimePair);

	//CT_TRACE_DEBUG("tick1=%u, tick2=%u", tick1, tick2);

	TEST_ASSERT_TRUE(tick2 >= tick1 + (kMsSleepTime - 1));

	msleep(ksSleepTime * kMilliSecsInSec);
	stopTimer(&relativeTimePair);
	tickS2 = getElapsedTimeInSeconds(&relativeTimePair);

	//CT_TRACE_DEBUG("ticks1=%u, ticks2=%u", tickS1, tickS2);

	TEST_ASSERT_TRUE(tickS2 >= tickS1 + (ksSleepTime - 1));
#endif
}
