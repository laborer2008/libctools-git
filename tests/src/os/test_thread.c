/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 * Complete test of all threads functions should be done after all threads functions are implemented
 */

#include "tests/unity_headers.h"

#include <ctools/os/thread.h>

#ifdef CT_THREAD_AVAILABLE
#	include <ctools/pointer.h>
#endif

#ifdef CT_KILL_THREAD_AVAILABLE
#	include <ctools/unused.h>
#endif


void setUp(void)
{
}

void tearDown(void)
{
}

CT_USING_CT_NAMESPACE

void testCompile(void)
{
#ifdef CT_THREAD_AVAILABLE
	// TODO spawn a thread here
	ThreadPointer threadPointer = CT_NULL;
#endif

#ifdef CT_KILL_THREAD_AVAILABLE
	const bool kStatus = killThread(&threadPointer);

	CT_UNUSED(kStatus);
#endif
}
