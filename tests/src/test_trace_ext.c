/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "tests/unity_headers.h"

#include <ctools/trace_ext.h>

#include <ctools/std/stdbool.h>


void setUp(void)
{
}

void tearDown(void)
{
}

CT_USING_CT_NAMESPACE

void testCompile(void)
{
	CT_TRACE_DEBUG_IF(true, "debug message");
	CT_TRACE_DEBUG_WP_IF(true, "debug message");

	CT_TRACE_INFO_IF(true, "info message");
	CT_TRACE_INFO_WP_IF(true, "info message");

	CT_TRACE_WARNING_IF(true, "warning message");
	CT_TRACE_WARNING_WP_IF(true, "warning message");

	CT_TRACE_ERRORN_IF(true, "error message");
	CT_TRACE_ERRORN_WP_IF(true, "error message");

	CT_TRACE_ERRORC_IF(true, "error message with errno code");
	CT_TRACE_ERRORC_WP_IF(true, "error message with errno code");

#ifdef CT_OS_WINDOWS
	CT_TRACE_ERRORW("error message with getLastErrorCode");
	CT_TRACE_ERRORW_WP("error message with getLastErrorCode");
#endif

	// Fatal messages will terminate test. Can't do it

	CT_TRACE_PERM_IF(true, "perm message");
	CT_TRACE_PERM_WP_IF(true, "perm message");
}
