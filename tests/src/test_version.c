/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "tests/unity_headers.h"

#include <ctools/predef/external_headers.h>

CT_BEGIN_EXTERNAL_HEADERS
#	include <string.h>
CT_END_EXTERNAL_HEADERS

#include <ctools/version.h>


void setUp(void)
{
}

void tearDown(void)
{
}

void testCompile(void)
{
	const char* const kVersion = CT_VER_STR_DOT(0, 1, 20);

	TEST_ASSERT_TRUE(strcmp(kVersion, "0.1.20") == 0);
}
