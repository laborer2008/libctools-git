/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "tests/unity_headers.h"

#include <ctools/likely.h>

#include <ctools/std/stdbool.h>


void setUp(void)
{
}

void tearDown(void)
{
}

/*
 * Tests only macros compilation. It's difficult to predict their true result
 */
void testLikelyCompile(void)
{
/*
 * We test here CT_LIKELY/CT_UNLIKELY but not their arguments
 */
#if defined (CT_COMPL_MSVC)
#	pragma warning(disable : 4127)
#endif

	/* *INDENT-OFF* */
	if (CT_LIKELY(true))
	{
		TEST_ASSERT(true);
	}

	if (CT_UNLIKELY(false))
	{
		TEST_ASSERT(true);
	}
	/* *INDENT-ON* */
}
