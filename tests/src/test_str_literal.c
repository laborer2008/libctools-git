/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "tests/unity_headers.h"

#if defined (CT_LANG_C11) || defined (CT_LANG_CXX11)
#	include "ctools/str_literal.h"

#	include "ctools/unused.h"
#endif


void setUp(void)
{
}

void tearDown(void)
{
}

void testCompile(void)
{
#if defined (CT_LANG_C11) || defined (CT_LANG_CXX11)
	const char str[] = CT_U8("Русский текст");

	CT_UNUSED(str);
#endif
}
