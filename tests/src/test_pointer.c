/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "tests/unity_headers.h"

#include <ctools/pointer.h>


void setUp(void)
{
}

void tearDown(void)
{
}

void testClear(void)
{
	enum {kNumberOfItems = 10000};

	// Stack can be filled by random values sometimes
	int array[kNumberOfItems];
	int i;
	int sum = 0;

	array[kNumberOfItems/10] = 10;
	array[kNumberOfItems/2] = 20;

	CT_CLEAR(array);

	for (i = 0; i < kNumberOfItems; i++)
		sum += array[i];

	TEST_ASSERT_EQUAL_INT(sum, 0);
}
