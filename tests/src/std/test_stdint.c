/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "tests/unity_headers.h"

#include <ctools/std/stdint.h>

#include <ctools/unused.h>


void setUp(void)
{
}

void tearDown(void)
{
}

void testCompile(void)
{
	const int8_t a1 = 0;
	const uint8_t a2 = 0;

	const int16_t a3 = 0;
	const uint16_t a4 = 0;

	const int32_t a5 = 0;
	const uint32_t a6 = 0;

	const int64_t a7 = 0;
	const uint64_t a8 = 0;

	CT_UNUSED(a1);
	CT_UNUSED(a2);
	CT_UNUSED(a3);
	CT_UNUSED(a4);
	CT_UNUSED(a5);
	CT_UNUSED(a6);
	CT_UNUSED(a7);
	CT_UNUSED(a8);
}
