/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "tests/unity_headers.h"

#include <ctools/std/math.h>

#include <ctools/predef/os.h>

#include <ctools/unused.h>


void setUp(void)
{
}

void tearDown(void)
{
}

void testCompilePi(void)
{
#if defined (CT_NO_OS)
	// Usually it is more appropriate
	const float kPi = M_PI;
#else
	const double kPi = M_PI;
#endif

	CT_UNUSED(kPi);
}

void testCompileStandardRound(void)
{
	TEST_ASSERT_EQUAL_INT64(lround(1.5), 2);
	TEST_ASSERT_EQUAL_INT64(lroundf(1.5f), 2);
	TEST_ASSERT_EQUAL_INT64(lroundl(1.5), 2);

	TEST_ASSERT_EQUAL_INT64(llround(1.5), 2);
	TEST_ASSERT_EQUAL_INT64(llroundf(1.5f), 2);
	TEST_ASSERT_EQUAL_INT64(llroundl(1.5), 2);
}

void testCompileExtensionRound(void)
{
	TEST_ASSERT_EQUAL_INT8(roundf8(1.5f), 2);
	TEST_ASSERT_EQUAL_UINT8(uroundf8(1.5f), 2U);

	TEST_ASSERT_EQUAL_INT16(roundf16(1.5f), 2);
	TEST_ASSERT_EQUAL_UINT16(uroundf16(1.5f), 2U);

	TEST_ASSERT_EQUAL_INT32(roundf32(1.5f), 2);
	TEST_ASSERT_EQUAL_UINT32(uroundf32(1.5f), 2U);

	TEST_ASSERT_EQUAL_INT64(roundf64(1.5f), 2);
	TEST_ASSERT_EQUAL_UINT64(uroundf64(1.5f), 2U);


	TEST_ASSERT_EQUAL_INT8(round8(1.5), 2);
	TEST_ASSERT_EQUAL_UINT8(uround8(1.5), 2U);

	TEST_ASSERT_EQUAL_INT16(round16(1.5), 2);
	TEST_ASSERT_EQUAL_UINT16(uround16(1.5), 2U);

	TEST_ASSERT_EQUAL_INT32(round32(1.5), 2);
	TEST_ASSERT_EQUAL_UINT32(uround32(1.5), 2U);

	TEST_ASSERT_EQUAL_INT64(round64(1.5), 2);
	TEST_ASSERT_EQUAL_UINT64(uround64(1.5), 2U);
}
