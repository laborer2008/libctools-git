/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "tests/unity_headers.h"

#include <ctools/std/stdbool.h>

#include <ctools/unused.h>


void setUp(void)
{
}

void tearDown(void)
{
}

void testCompile(void)
{
	const bool kA = false;
	const bool kB = true;

	CT_UNUSED(kA);
	CT_UNUSED(kB);
}
