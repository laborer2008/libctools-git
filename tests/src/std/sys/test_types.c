/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "tests/unity_headers.h"

#include <ctools/std/sys/types.h>
#include <ctools/namespace.h>
#include <ctools/unused.h>


void setUp(void)
{
}

void tearDown(void)
{
}

CT_USING_CT_NAMESPACE

void testCompile(void)
{
#ifdef CT_OS_LINUX
	const pid_t kTid = gettid();

	CT_UNUSED(kTid);
#endif

	const uint kA = 0;
	const size_t kB = 0;

	CT_UNUSED(kA);
	CT_UNUSED(kB);
}
