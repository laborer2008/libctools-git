/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "tests/unity_headers.h"

#include <ctools/warning.h>


void setUp(void)
{
}

void tearDown(void)
{
}

void testCompile(void)
{
#ifdef CT_COMPILE_MESSAGE_AVAILABLE
	CT_COMPILE_MESSAGE(test compile message)
#endif

#ifdef CT_COMPILE_WARNING_AVAILABLE
	CT_COMPILE_WARNING(test compile warning)
#endif
}
