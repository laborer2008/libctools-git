/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "tests/unity_headers.h"

#include <ctools/min_max.h>


void setUp(void)
{
}

void tearDown(void)
{
}

static const int a = -10;
static const int b = 3;

void testMin(void)
{
	const int c = CT_MIN2(a, b);

	TEST_ASSERT_EQUAL_INT(c, a);
}

void testMax(void)
{
	const int d = CT_MAX2(a, b);

	TEST_ASSERT_EQUAL_INT(d, b);
}
