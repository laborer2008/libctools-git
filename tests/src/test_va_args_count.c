/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "tests/unity_headers.h"

#include <ctools/va_args_count.h>


void setUp(void)
{
}

void tearDown(void)
{
}

void testCompile(void)
{
#ifndef CT_COMPL_MSVC
	TEST_ASSERT_EQUAL_UINT(CT_VA_ARGS_COUNT(), 0);
	TEST_ASSERT_EQUAL_UINT(CT_VA_ARGS_COUNT(""), 1);
#endif

	TEST_ASSERT_EQUAL_UINT(CT_VA_POSITIVE_ARGS_COUNT(""), 1);
	TEST_ASSERT_EQUAL_UINT(CT_VA_POSITIVE_ARGS_COUNT('a'), 1);
	TEST_ASSERT_EQUAL_UINT(CT_VA_POSITIVE_ARGS_COUNT('q', 23, -0, "4"), 4);

	TEST_ASSERT_EQUAL_UINT(CT_VA_POSITIVE_ARGS_COUNT(CT_VA_ARGS_COUNT_VALUES), 63);
}
