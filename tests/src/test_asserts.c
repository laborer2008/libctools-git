/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "tests/unity_headers.h"

#include <ctools/asserts.h>


void setUp(void)
{
}

void tearDown(void)
{
}

void testAssert(void)
{
	const bool kDontTriggerAssert = true;

	CT_ASSERT(kDontTriggerAssert, "", "1");
}

void testCompileTimeAssert(void)
{
	CT_COMPILE_TIME_ASSERT(true, "");
}
