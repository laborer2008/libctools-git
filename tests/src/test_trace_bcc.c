/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 * Just a stub module since trace_bcc is a copy of trace_ext
 */

#include "tests/unity_headers.h"

#include <ctools/trace.h>


void setUp(void)
{
}

void tearDown(void)
{
}

void testCompile(void)
{
}
