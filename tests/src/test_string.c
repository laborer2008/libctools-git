/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "tests/unity_headers.h"

#include <ctools/string.h>


void setUp(void)
{
}

void tearDown(void)
{
}

void testStrLen(void)
{
	TEST_ASSERT_EQUAL_UINT(CT_STRLEN(""), 0);
	TEST_ASSERT_EQUAL_UINT(CT_STRLEN("1"), 1);
	TEST_ASSERT_EQUAL_UINT(CT_STRLEN("123"), 3);
}

#if defined (CT_PACK_STRING_TO_UINT_AVAILABLE)

void testPackStringToUint(void)
{
	uint8_t a8;
	uint8_t b8;

	uint16_t a16;
	uint16_t b16;

	uint32_t a32;
	uint32_t b32;

	uint64_t a64;
	uint64_t b64;

	/*
	 * CT_PACK_STRING_TO_UINT for C returns 8 byte long value.
	 * And here we also check how it's truncated to smaller variables
	 */

	a8 = CT_PACK_STRING_TO_UINT("");
	b8 = CT_PACK_STRING_TO_UINT_EMPTY_STRING;
	TEST_ASSERT_EQUAL_UINT(a8, b8);

	a8 = CT_PACK_STRING_TO_UINT("A");
	b8 = 'A';
	TEST_ASSERT_EQUAL_UINT(a8, b8);

	a16 = CT_PACK_STRING_TO_UINT("ET");
	b16 = 'T' << 8 | 'E';
	TEST_ASSERT_EQUAL_UINT(a16, b16);

	a32 = CT_PACK_STRING_TO_UINT("123");
	b32 = 0x333231;
	TEST_ASSERT_EQUAL_UINT(a32, b32);

	a32 = CT_PACK_STRING_TO_UINT("1234");
	b32 = 0x34333231;
	TEST_ASSERT_EQUAL_UINT(a32, b32);

	a64 = CT_PACK_STRING_TO_UINT("12345");
	b64 = 0x3534333231;
	TEST_ASSERT_EQUAL_UINT(a64, b64);

	a64 = CT_PACK_STRING_TO_UINT("123456");
	b64 = 0x363534333231;
	TEST_ASSERT_EQUAL_UINT(a64, b64);

	a64 = CT_PACK_STRING_TO_UINT("1234567");
	b64 = 0x37363534333231;
	TEST_ASSERT_EQUAL_UINT(a64, b64);

	a64 = CT_PACK_STRING_TO_UINT("12345678");
	b64 = 0x3837363534333231;
	TEST_ASSERT_EQUAL_UINT(a64, b64);

	a64 = CT_PACK_STRING_TO_UINT("1234567890");
	b64 = CT_PACK_STRING_TO_UINT_TOO_LONG_STRING;
	TEST_ASSERT_EQUAL_UINT(a64, b64);
}

#endif	 // CT_PACK_STRING_TO_UINT_AVAILABLE
