/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "tests/unity_headers.h"

#include <ctools/predef/external_headers.h>

CT_BEGIN_EXTERNAL_HEADERS
#	include <limits.h>
CT_END_EXTERNAL_HEADERS

#include <ctools/bits.h>
#include <ctools/binary_const.h>
#include <ctools/std/stdint.h>


void setUp(void)
{
}

void tearDown(void)
{
}

void testBitSet(void)
{
	uint16_t a = 0;
	uint32_t b = 0xffffffff;

	CT_BIT_SET(a, 15);
	TEST_ASSERT_EQUAL_UINT16(0x8000, a);

	CT_BIT_SET(a, 0);
	TEST_ASSERT_EQUAL_UINT16(0x8001, a);

	CT_BIT_SET(b, 1);
	TEST_ASSERT_EQUAL_UINT32(0xffffffff, b);

#if UINT_MAX >= UINT32_MAX
	b = 0;
	CT_BIT_SET(b, 31);
	TEST_ASSERT_EQUAL_UINT32(0x80000000, b);
#endif

	b = 0;
	CT_BIT_SET(b, 15);
	TEST_ASSERT_EQUAL_UINT32(0x8000, b);
}

void testBitClear(void)
{
	uint32_t a = 0xffffffff;

	CT_BIT_CLEAR(a, 0);
	TEST_ASSERT_EQUAL_UINT32(0xfffffffe, a);

	CT_BIT_CLEAR(a, 0);
	TEST_ASSERT_EQUAL_UINT32(0xfffffffe, a);

	CT_BIT_CLEAR(a, 15);
	TEST_ASSERT_EQUAL_UINT32(0xffff7ffe, a);

#if UINT_MAX >= UINT32_MAX
	CT_BIT_CLEAR(a, 31);
	TEST_ASSERT_EQUAL_UINT32(0x7fff7ffe, a);
#endif
}

void testBitFlip(void)
{
	uint32_t a = 0xffffffff;

	CT_BIT_FLIP(a, 15);
	TEST_ASSERT_EQUAL_UINT32(0xffff7fff, a);

	CT_BIT_FLIP(a, 15);
	TEST_ASSERT_EQUAL_UINT32(0xffffffff, a);

#if UINT_MAX >= UINT32_MAX
	CT_BIT_FLIP(a, 31);
	TEST_ASSERT_EQUAL_UINT32(0x7fffffff, a);
#endif

	a = CT_B8(100);

	CT_BIT_FLIP(a, 2);
	TEST_ASSERT_EQUAL_UINT32(0, a);
}

void testBitCheck(void)
{
	uint32_t a;

	a = CT_B8(100);
	TEST_ASSERT_TRUE(CT_BIT_CHECK(a, 2));

	a = 0x8101;
	TEST_ASSERT_TRUE(CT_BIT_CHECK(a, 15));

#if UINT_MAX >= UINT32_MAX
	a = 0x80000101;
	TEST_ASSERT_TRUE(CT_BIT_CHECK(a, 31));
#endif
}

void testPopCount(void)
{
	const uint x0 = 0;
	const uint x1 = CT_B8(1);
	const uint x2 = CT_B8(101010);
	const uint x3 = 0xffff;
	const uint32_t x4 = 0xffffffff;

	TEST_ASSERT_TRUE(0 == popCount(x0));
	TEST_ASSERT_TRUE(1 == popCount(x1));
	TEST_ASSERT_TRUE(3 == popCount(x2));
	TEST_ASSERT_TRUE(16 == popCount(x3));
	TEST_ASSERT_TRUE(32 == popCount(x4));
}
