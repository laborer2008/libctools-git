/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "tests/unity_headers.h"

#include <ctools/array.h>


void setUp(void)
{
}

void tearDown(void)
{
}

void testArraySize(void)
{
	enum {kNumberOfItems = 2};
	int16_t array[kNumberOfItems];

	TEST_ASSERT_EQUAL_UINT(CT_ARRAY_SIZE(array), kNumberOfItems);
}
