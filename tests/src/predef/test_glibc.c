/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "tests/unity_headers.h"

#define CT_GLIBC_NEED_BSD_DEFAULT_SOURCE
#define CT_GLIBC_NEED_SVID_DEFAULT_SOURCE
#include <ctools/predef/glibc.h>


#if defined (CT_LIBC_GLIBC)

#	if !defined(_BSD_SOURCE) && !defined(_DEFAULT_SOURCE) && !defined(CT_GLIBC_BSD_DEFAULT_SOURCE_WAS_DEFINED)
#		error (_BSD_SOURCE or _DEFAULT_SOURCE) and CT_GLIBC_BSD_DEFAULT_SOURCE_WAS_DEFINED must be defined
#	endif

#	if !defined(_SVID_SOURCE) && !defined(_DEFAULT_SOURCE) && !defined(CT_GLIBC_SVID_DEFAULT_SOURCE_WAS_DEFINED)
#		error (_SVID_SOURCE or _DEFAULT_SOURCE) and CT_GLIBC_SVID_DEFAULT_SOURCE_WAS_DEFINED must be defined
#	endif


#	undef _BSD_SOURCE
#	undef _SVID_SOURCE
#	undef _DEFAULT_SOURCE
#	undef CT_GLIBC_BSD_DEFAULT_SOURCE_WAS_DEFINED
#	undef CT_GLIBC_NEED_BSD_DEFAULT_SOURCE

#endif	// defined (CT_LIBC_GLIBC)


void setUp(void)
{
}

void tearDown(void)
{
}

void testCompile(void)
{
}
