/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "tests/unity_headers.h"

#include <ctools/predef/cxx11_attributes.h>
#include <ctools/unused.h>


CT_ENUM_CLASS SomeEnum
{
	seValue1 = 0,
	seValue2
};

void setUp(void)
{
}

void tearDown(void)
{
}

void testCompile(void) CT_NOEXCEPT
{
	enum SomeEnum someEnum = seValue1;

	CT_UNUSED(someEnum);
}

#ifdef CT_COMPILER_NOEXCEPT
void testCtNoExcept(void) noexcept
{
}
#else
void testCtNoExcept(void)
{
}
#endif

void testEnumClass(void)
{
#ifdef CT_COMPILER_CLASS_ENUM
	enum class SomeEnumClass
	{
		secValue1 = 0,
		secValue2
	};

	enum SomeEnumClass someEnumClass = secValue1;

	CT_UNUSED(someEnumClass);
#endif
}
