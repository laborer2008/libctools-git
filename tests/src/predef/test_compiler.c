/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "tests/unity_headers.h"

#include <ctools/predef/compiler.h>

#include <ctools/unused.h>


void setUp(void)
{
}

void tearDown(void)
{
}

void testCompilerMacro(void)
{
#if defined (CT_COMPL_MINGW)
	int a = 0;
#elif defined (CT_COMPL_CLANG)
	int a = 1;
#elif defined (CT_COMPL_GCC)
	int a = 2;
#elif defined (CT_COMPL_MSVC)
	int a = 3;
#elif defined (CT_COMPL_ICC)
	int a = 4;
#elif defined (CT_COMPL_ARMCC)
	int a = 5;
#elif defined (CT_COMPL_BCC)
	int a = 6;
#elif defined (CT_COMPL_SDCC)
	int a = 7;
#endif

#ifdef CT_COMPL_MSVC_RC
	int b = 0;

	CT_UNUSED(b);
#endif

	CT_UNUSED(a);
}

void testGccEmulationMacro(void)
{
#ifdef CT_COMPL_GCC_EMULATION
	int a;

	CT_UNUSED(a);
#endif
}

void testMsvcEmulationMacro(void)
{
#ifdef CT_COMPL_MSVC_EMULATION
	int a;

	CT_UNUSED(a);
#endif
}

void testGccVersionMacro(void)
{
#ifdef CT_COMPL_GCC_EMULATION
#	if CT_GCC_VERSION >= 40000
	int a;

	CT_UNUSED(a);
#	endif
#endif
}

void testClangVersionMacro(void)
{
#ifdef CT_COMPL_CLANG
#	if CT_CLANG_VERSION >= 29000
	int a;

	CT_UNUSED(a);
#	endif
#endif
}
