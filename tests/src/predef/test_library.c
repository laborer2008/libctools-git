/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "tests/unity_headers.h"

#include <ctools/predef/library.h>
#include <ctools/unused.h>


void setUp(void)
{
}

void tearDown(void)
{
}

void testCompile(void)
{
#if defined (CT_LIBC_UCLIBC)
	int a = 1;
	int b = CT_LIBC_UCLIBC_VERSION;

	CT_UNUSED(b);
#elif defined (CT_LIBC_GLIBC)
	int a = 2;
#elif defined (CT_LIBC_MINGW)
	int a = 3;
#elif defined (CT_LIBC_MSVCRT)
	int a = 4;
#elif defined (CT_LIBC_BIONIC)
	int a = 5;
#elif defined (CT_LIBC_SDCC)
	int a = 6;
#endif

	CT_UNUSED(a);
}
