/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "tests/unity_headers.h"

#include <ctools/predef/cxx14_attributes.h>


void setUp(void)
{
}

void tearDown(void)
{
}

void func1(void) CT_NOEXCEPT_UNTIL_CXX14
{
}

void func2(void) CT_NOEXCEPT_SINCE_CXX14
{
}
