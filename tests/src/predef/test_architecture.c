/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "tests/unity_headers.h"

#include <ctools/predef/architecture.h>
#include <ctools/unused.h>


void setUp(void)
{
}

void tearDown(void)
{
}

void testCompile(void)
{
#if defined (CT_ARCH_X86)
	int a = 0;
#elif defined (CT_ARCH_X64)
	int a = 1;
#elif defined (CT_ARCH_ARM)
	int a = 2;
#elif defined (CT_ARCH_ARM64)
	int a = 3;
#elif defined (CT_ARCH_STM8)
	int a = 4;
#endif

	CT_UNUSED(a);
}
