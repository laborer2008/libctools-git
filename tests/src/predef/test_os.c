/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "tests/unity_headers.h"

#include <ctools/predef/os.h>
#include <ctools/unused.h>


void setUp(void)
{
}

void tearDown(void)
{
}

void testCompile(void)
{
#if defined (CT_OS_WINDOWS)
	int a = 0;
#elif defined (CT_OS_UNIX)
	int a = 1;
#elif defined (CT_OS_LINUX)
	int a = 2;
#elif defined (CT_OS_ANDROID)
	int a = 3;
#elif defined (CT_OS_MACOS)
	int a = 4;
#elif defined (CT_NO_OS)
	int a = 5;
#endif

	CT_UNUSED(a);
}
