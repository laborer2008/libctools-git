/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "tests/unity_headers.h"

#include <ctools/predef/attributes.h>

#include <ctools/std/sys/types.h>
#include <ctools/struct.h>
#include <ctools/trace.h>

#if !defined (CT_COMPL_GCC_EMULATION)
#	include <ctools/unused.h>
#endif


void setUp(void)
{
}

void tearDown(void)
{
}


CT_DLL_EXPORT void exportedFunction(void)
{
}

// Virtually we don't have it, so don't call it
CT_DLL_IMPORT void importedFunction(void);

CT_DLL_HIDDEN void hiddenFunction(void)
{
}

static CT_FORCEINLINE void inlinedFunction(void)
{
}

// Don't call it
static void CT_NORETURN noreturnedFunction(void)
{
  for (;;)
	;
}

// TODO: warning: ‘stdcall’ attribute ignored [-Wattributes] on Linux
void CT_STDCALL stdcallFunction(void)
{
}

// TODO: warning: ‘fastcall’ attribute ignored [-Wattributes] on Linux
void CT_FASTCALL fastcallFunction(void)
{
}

// TODO: warning: ‘cdecl’ attribute ignored [-Wattributes] on Linux
void CT_CDECL cdeclFunction(void)
{
}

CT_PURE_FUNC void pureFunction(void)
{
}

CT_CONST_FUNC void constFunction(void)
{
}

CT_DEPRECATED_FUNC void deprecatedFunction(void)
{
}

void testCompile(void)
{
	exportedFunction();
	hiddenFunction();
	inlinedFunction();

	stdcallFunction();
	fastcallFunction();
	cdeclFunction();

	pureFunction();
	constFunction();
	deprecatedFunction();
}

#define ALIGN_VALUE		512

void testAligned(void)
{
	CT_ALIGNED(ALIGN_VALUE) int a;

	TEST_ASSERT_TRUE(((uint64_t)(&a)) % ALIGN_VALUE == 0);
}

void testFunction(void)
{
	CT_TRACE_INFO("%s\n", CT_FUNCTION);
}

void testPacked(void)
{
#ifdef CT_COMPL_GCC_EMULATION
	typedef struct CT_GCC_PACKED
	{
		char a;
		int b;
	} PackedStruct;

	const size_t kPackedFieldsSize = CT_STRUCT_MEMBER_SIZE(PackedStruct, a) + CT_STRUCT_MEMBER_SIZE(PackedStruct, b);

	TEST_ASSERT_EQUAL_UINT(kPackedFieldsSize, sizeof(PackedStruct));
#endif
}

static void unusedFunction(CT_GCC_UNUSED int a)
{
#if !defined (CT_COMPL_GCC_EMULATION)
	CT_UNUSED(a);
#endif
}

void testUnused(void)
{
	unusedFunction(0);
}
