/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "tests/unity_headers.h"

#include <ctools/predef/language.h>

#include <ctools/predef/compiler.h>
#include <ctools/unused.h>


void setUp(void)
{
}

void tearDown(void)
{
}

void testCompile(void)
{
#if defined (CT_LANG_C94)
	int a = 0;
#elif defined (CT_LANG_C99)
	int a = 1;
#elif defined (CT_LANG_C11)
	int a = 2;
#elif defined (CT_LANG_CXX11)
	int a = 3;
#elif defined (CT_LANG_CXX14)
	int a = 4;
#elif defined (CT_LANG_CXX17)
	int a = 5;
#elif defined (CT_LANG_OBJC)
	int a = 6;
#elif defined (CT_COMPL_MSVC) || defined (CT_COMPL_BCC)
//	These compilers don't support some of above mentioned standards
	int a = 7;
#endif

	CT_UNUSED(a);
}
