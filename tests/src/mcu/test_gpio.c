/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "tests/unity_headers.h"

#include <ctools/mcu/gpio.h>

#include <ctools/std/stdbool.h>
#include <ctools/namespace.h>


void setUp(void)
{
}

void tearDown(void)
{
}

CT_USING_CT_NAMESPACE

void testCompile(void)
{
#if defined (CT_PACK_SUPPORTED)
	PinDescription pinDescription;
	PinID pinID;

	const bool kIsPinIdCorrect = sizeof(pinID) == 1;
	const bool kIsPinDescriptionCorrect = sizeof(pinDescription) == 2;

	TEST_ASSERT_TRUE(kIsPinIdCorrect);
	TEST_ASSERT_TRUE(kIsPinDescriptionCorrect);
#endif
}
