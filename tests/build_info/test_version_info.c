/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "tests/unity_headers.h"

#include <ctools/unused.h>
#include <ctools/version_info.h>


void setUp(void)
{
}

void tearDown(void)
{
}

void testCompile(void)
{
	const int kMajorVersion = CT_MAJOR_VERSION;
	const int kMinorVersion = CT_MINOR_VERSION;
	const int kBuildRevision = CT_BUILD_REVISION;

	const bool kInternalBuild = CT_INTERNAL_BUILD;

	const char* const kVersion =  CT_K_VERSION;

	CT_UNUSED(kMajorVersion);
	CT_UNUSED(kMinorVersion);
	CT_UNUSED(kBuildRevision);

	CT_UNUSED(kInternalBuild);

	CT_UNUSED(kVersion);
}
