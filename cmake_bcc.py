#!/usr/bin/env python3


__author__ = 'Sergey Gusarov'
__license__ = 'MPL 2.0, see LICENSE'


import os
import sys


sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'pyrepo'))
import fs

sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'cmake', 'cmake_tools', 'scripts', 'build'))
import common

sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'cmake', 'cmake_tools', 'scripts', 'build', 'bcc'))
import default


def main():
	projectDir = os.path.dirname(os.path.realpath(__file__))
	ctoolsIncludeDir = os.path.join(projectDir, 'include', 'ctools')
	inputFile = os.path.join(ctoolsIncludeDir, 'trace_ext.h')
	outputFile = os.path.join(ctoolsIncludeDir, 'trace_bcc.h')

	fs.replaceStringsInFile(inputFile, outputFile, ['## __VA_ARGS__'], ['__VA_ARGS__'])

	default.build(os.path.dirname(os.path.realpath(__file__)), 'CT', common.Language.c)

main()
