#!/usr/bin/env python3

"""Manual cleaning of all generated files."""


__author__ = 'Sergey Gusarov'
__license__ = 'MPL 2.0, see LICENSE'


import os
import subprocess
import sys

sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', 'pyrepo'))
import common_clean
import fs


def main():
	scriptDir = os.path.dirname(os.path.realpath(__file__))
	parentDir = os.path.join(scriptDir, '..')

	common_clean.clean()

	# build_info on non-standard path
	fs.removeFile(os.path.join(parentDir, 'build_info', 'include', 'ctools', 'version_info.h'))

	fs.removeFile(os.path.join(parentDir, 'include', 'ctools', 'trace_bcc.h'))
	fs.cleanDir(os.path.join(parentDir, 'build', 'runners'))

	subprocess.check_call(os.path.join(parentDir, 'cmake', 'cmake_tools', 'scripts', 'cleanall.py'), shell = True)

main()
