#!/usr/bin/env python3

"""Wrapper above cppcheck utility."""


__author__ = 'Sergey Gusarov'
__license__ = 'MPL 2.0, see LICENSE'


import os
import sys

sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', 'pyrepo'))
import cppcheck


def main():
	cppcheck.runForStandardSources()

main()
