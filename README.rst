=====================================================================
Repository `libctools <https://bitbucket.org/laborer2008/libctools>`_
=====================================================================

General information
-------------------

This library contains code that extends and simplifies different operations
for C language based programs. In other words it contains some programming tools
that can be used inside every quite advanced C program. All code as much as possible
is compatible with C99, C++98 and C++2011 standards and therefore can be easy integrated
with C++ code (with proper optimizations). It's very important to stick around C99.
As of 2015 year C11 is not supported well around the world. Besides, there are other
interesting standard like OpenCL which compatible with C99.
The library is supposed to be used for development for both bare-metal and
complicated code that run under any kind of operating system, for any processor architecture
with any machine's word length.
The main purpose of library's existence is to provide handy layer between operating systems
(they usually have C API), compilers (with some useful extensions) and application programs developed with C/C++.

Macros and preprocessor definitions exported by library have CT prefix. Internal definitions
(and so types and functions) have ``__`` postfix (e.g. ``CT_B8__``).
All variables, constants and functions for C++ programs belong to ctools namespace.

Content
-------

* Definitions of compilers, system libraries, operating systems and unification of their extensions;
* Runtime and compiletime asserts support;
* Binary constants and bitwise operations;
* Adjustable compiletime and runtime traces;
* Preprocessor calculations;
* Working with pointers;
* Enums with extended facilities;
* Various constants.

Library's interface (include/ctools):

* mcu - contains primitives for microcontrollers. Perhaps will be moved to another standalone library;
* os - not aimed for projects with bare-metal target platform;
* predef - see "Rationale" sections;
* std - replacement for standard include headers. They allow workaround limitations of some
  platforms, compilers and bring handy extensions to non-native configurations. Some modules
  contain "wrappers" over library functions. The only reason for their existence is to provide
  logging of operations.

Other known related projects
----------------------------

* boost/predef
* predef_
* Qt/qtbase
* P99_
* libowfat_

All these projects is a good source of various ideas. But they most likely have incompatible license.
Library is supposed to be minimalistic. That's why lots of dependencies are not welcome.
Some ideas for predef could be borrowed from boost/predef, predef_ and Qt/qtbase.
Those projects could be integrated here as subprojects but:

* These subprojects should become a part of the ctools interface. If an external program
  use for example boost on its own then we have 2 boost's version and probably a conflict;
* predef_ code from sourceforge.net isn't supported and improved ever since 2005.
  But wiki is still up to date;
* Qt/qtbase is a C++ library, not our case.

Target platforms
----------------

Any that support C99, C++98 (and higher) compatible compilers with any machine's word length and
any endianness. Some useful not completely C99-compatible compilers like MS VC++ are allowed as exceptions.

Tested mainly though:

* Linux x86_64 + gcc;
* Linux x86_64 + icc;
* Linux x86_64 + clang;
* Windows XP + minwg;
* Windows XP + MSVC2010.

The library must be compatible both with resources constrained platfoms (bare metal without OS)
and with modern OS. Library should work at large all the time, although some features can not be available on the particular platform
(e.g. realization of mutexes or msleep() function based on system calls).
It is supposed that any definitions and declarations from library's interface can be freely used in any combinations.
To achieve that they split into independent modules as much as possible.
When this library is used for constrained platforms developers should be cautious
and check all the things to be sure that everything is doing right.

Dependencies
------------

All scripts are written with Python_ programming (Python_ 3.3 and above is recommended).
The project could be built with CMake_ building system. CMake 2.8.3 and above is needed in that case.
Both dependencies are not mandatory.

The scripts/cmake_build.py script depends on flufl.enum module. See README from pyrepo_ about
installation details.

pyrepo_'s dependencies also must be installed in the system.

Building
--------

There are 4 ways of how the library can be used:

* Building is not taking place. A project that is going to use ctools
  should worry about a building process. include and build_info/include directories should be passed to the compiler
  as directories of headers files. All the modules from src and its subdirectories should be
  linked to the output binary (if it is required). In some cases preprocessor definitions
  should be set up to choose mode of various subsystems.
  As the library evolving these build rules should be maintained. All the latest information
  about those rules can be borrowed from CMakeLists.txt files of libctools_ и libcpptools_ projects.
  This case can be recommended only when the others are unimplementable or unpreferable for some reasons.
* Building with CMakeLists.txt . A hierarchy of such files is formed.
  An uplevel project should import libctools_ like this:

  add_subdirectory(libctools)

  Some other cmake's definitions or environment variables can be set up to tune the process.
  An example of such hierarchy is a libctools_'s CMakeLists.txt;
* Building with the single CMakeLists.txt . The majority of library features is turned off at the building.
  This case is useful for editing the project in some IDEs;
* Building is performed by using cmake_*.py scripts. The built debug or release library is placed
  to the appropriate build subdirectory.

Some preprocessor identifiers which could be defined for MCU-targeted builds are described below:

* CT_NO_DYNAMIC_MEMORY - library will lack of dynamic memory support
  and some other inherent things;
* CT_RETARGET_PUTCHAR - provide to the linker code of standard putchar() function. It fixes linking errors
  for standalone builds. Any project-user have to implement this function if necessary.

Library initialization
----------------------

Sometimes some actions need to be done right after loading of the library. Otherwise some subsystems may not work correctly.
If an external project just use some common macros then initialization is not required. Otherwise these is recommended to call:

* initializeCtools() before using of library;
* finalizeCtools() after of ceasing any work with library (unloading dynamic library from the memory or program termination).

Testing
-------

See tests subfolder.

TODO
----

.. _CMake : http://www.cmake.org
.. _Python : http://www.python.org
.. _libcpptools : https://bitbucket.org/laborer2008/libcpptools
.. _pyrepo : https://bitbucket.org/laborer2008/pyrepo
.. _predef : https://sourceforge.net/p/predef/wiki/Home
.. _libowfat : http://www.fefe.de/libowfat
.. _P99 : http://p99.gforge.inria.fr
.. _Mercurial : https://www.mercurial-scm.org
.. _Git : https://git-scm.com
