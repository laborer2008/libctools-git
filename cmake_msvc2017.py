#!/usr/bin/env python3


__author__ = 'Sergey Gusarov'
__license__ = 'MPL 2.0, see LICENSE'


import os
import sys


sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'cmake', 'cmake_tools', 'scripts', 'build'))
import common

sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'cmake', 'cmake_tools', 'scripts', 'build', 'msvc'))
import v2017


def main():
	v2017.build(os.path.dirname(os.path.realpath(__file__)), 'CT', common.Language.c)

main()
