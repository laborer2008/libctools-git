# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


cmake_minimum_required(VERSION 2.8.3)

if (${CMAKE_MAJOR_VERSION}.${CMAKE_MINOR_VERSION}.${CMAKE_PATCH_VERSION} VERSION_GREATER 2.8.10)
	cmake_policy(SET CMP0020 OLD)
endif()

if (${CMAKE_MAJOR_VERSION}.${CMAKE_MINOR_VERSION}.${CMAKE_PATCH_VERSION} VERSION_GREATER 2.8.11)
	cmake_policy(SET CMP0023 OLD)
endif()

# Fix warnings about manually set variables which are unused.
# Typically these variables appear in this project from other cmake files
set(CT_IGNORE_ME__ "${CMAKE_CXX_COMPILER}${QT_QMAKE_EXECUTABLE}")
unset(CT_IGNORE_ME__)


include(${CMAKE_CURRENT_SOURCE_DIR}/cmake/cmake_tools/tools/python_binary.cmake)

execute_process(COMMAND ${CMT_PYTHON_BINARY} ${CMAKE_CURRENT_LIST_DIR}/pyrepo/gen_build_info.py
	"CT" ${CMAKE_CURRENT_LIST_DIR}/build_info/include/ctools
)

if (NOT PROJECT_NAME AND NOT CT_STANDALONE_BUILD)
	# Pure CMake build(suitable also for IDE's parsers)
	set(CT_STANDALONE_BUILD		1)
	set(CT_BIN_OUTPUT_PATH		"${CMAKE_CURRENT_LIST_DIR}/build/cmake_standalone")
	set(CT_LANG					C)
endif()

if (CT_STANDALONE_BUILD)
	include(${CMAKE_CURRENT_SOURCE_DIR}/cmake/cmake_tools/tools/check_variables.cmake)
	include(${CMAKE_CURRENT_SOURCE_DIR}/cmake/cmake_tools/tools/language.cmake)

	if ("${CMAKE_BUILD_TYPE}" STREQUAL "Debug")
		add_definitions(-DCT_TRACE_TERMINAL)
		add_definitions(-DCT_DYN_TRACES=1)
		add_definitions(-DCT_USE_RUNTIME_ASSERTS)
	else()
		if (WIN32)
			add_definitions(-DCT_TRACE_LEVEL_NO_TRACE)
		else()
			add_definitions(-DCT_TRACE_TERMINAL)
			add_definitions(-DCT_TRACE_LEVEL=CT_TRACE_LEVEL_INFO)
		endif()

		# See man 3 assert
		add_definitions(-DNDEBUG)
	endif()

	if (CT_RETARGET_PUTCHAR)
		add_definitions(-DCT_RETARGET_PUTCHAR=1)
	endif()
else()
	# Definitions from above-mentioned files should provide top level script
endif()

cmtAssertVariableNotEmpty(CT_LANG)
cmtEnableLanguage(${CT_LANG})

cmtAssertVariableNotEmpty(CT_BIN_OUTPUT_PATH)

set(PROJECT_NAME ctools)
project(${PROJECT_NAME} ${CT_LANG}) # This operation cleans compiler flags

if (CT_STANDALONE_BUILD)
	set(CMT_LANG "${CT_LANG}")
	include(${CMAKE_CURRENT_SOURCE_DIR}/cmake/compilers.cmake)
else()
	# Definitions from above-mentioned files should provide top level script

	# Don't inherit
	set(CMAKE_AUTOMOC OFF)
endif()

file(GLOB_RECURSE SRCS1 src/*.c)

file(GLOB_RECURSE INC1 src/*.h)
file(GLOB_RECURSE INC2 include/*.h)
file(GLOB_RECURSE INC3 build_info/include/*.h)

if (WIN32)
	set(RC_INCLUDE "${CMAKE_CURRENT_SOURCE_DIR}/build_info")
	set(RC_FILES "${RC_INCLUDE}/file_property.rc")

	# windres.exe for mingw, rc.exe for ms vc++
	# 65001 == utf8
	set(CMAKE_RC_FLAGS "-v -c 65001 -I${RC_INCLUDE} -Iinclude")
else()
	set(RC_FILES)
endif()

set(SOURCES ${SRCS1} ${INC1} ${INC2} ${INC3} ${RC_FILES})

include_directories(include)
include_directories(src)
include_directories(build_info/include)

set(LIBRARY_OUTPUT_PATH ${CT_BIN_OUTPUT_PATH})

add_library(${PROJECT_NAME} ${SOURCES})

include(${CMAKE_CURRENT_SOURCE_DIR}/cmake/cmake_tools/tools/pthread.cmake)
cmtLinkPthread(${PROJECT_NAME})

include(${CMAKE_CURRENT_SOURCE_DIR}/cmake/cmake_tools/tools/library.cmake)

if (("${CMAKE_SYSTEM}" MATCHES "Linux"))
	cmtGlibcVersion(${CT_BIN_OUTPUT_PATH})

	if ("${CMT_GLIBC_VERSION}" VERSION_LESS "2.17")
		include(${CMAKE_CURRENT_SOURCE_DIR}/cmake/cmake_tools/tools/link_public.cmake)

		target_link_libraries(${PROJECT_NAME} ${CMT_LINK_PUBLIC} rt)
	endif()
endif()

set(CMAKE_VERBOSE_MAKEFILE true)

if (CT_BUILD_TESTS)
	cmtAssertVariableNotEmpty(CT_TESTS_BIN_DIR)
	add_subdirectory(tests)
endif()
