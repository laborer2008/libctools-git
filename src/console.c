/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 * This file was taken from the at91lib_20100901_softpack_1_9_v_1_0_svn_v15011.zip file
 * (latest available at91lib from Atmel's site). Many things are not actual for new projects.
 * Because of this file was completely reworked. C++ support is added.
 * Now it requires C99/C++98 compalible compiler.
 */

/* ---------------------------------------------------------------------------------------------------------------------
 *         ATMEL Microcontroller Software Support
 * ---------------------------------------------------------------------------------------------------------------------
 * Copyright (c) 2008, Atmel Corporation
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer below.
 *
 * Atmel's name may not be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * DISCLAIMER: THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * ---------------------------------------------------------------------------------------------------------------------
 */

#include "ctools/console.h"

#include "ctools/pointer.h"
#include "ctools/trace.h"


CT_BEGIN_NAMESPACE

bool consoleGetInteger(uint* const pValue) CT_NOEXCEPT
{
	unsigned char nbNb = 0;
	uint value = 0;

	CT_CHECK_PTR_AND_RETURN_RES(pValue, false);

	while (CT_TRUE)
	{
		const unsigned char key = (unsigned char)getchar();

		// putchar(key); // echo
		if (key >= '0' && key <= '9')
		{
			value = (value * 10) + (key - '0');
			nbNb++;
		}
		else if (key == 0x0D || key == ' ')
		{
			if (nbNb == 0)
			{
				printf(CT_ENDL "Write a number and press ENTER or SPACE!" CT_ENDL);

				return false;
			}
			else
			{
				printf(CT_ENDL);
				*pValue = value;

				return true;
			}
		}
		else
		{
			printf(CT_ENDL "'%c' not a number!" CT_ENDL, key);

			return false;
		}
	}
}

bool consoleGetIntegerMinMax(uint* const pValue, const uint min, const uint max) CT_NOEXCEPT
{
	uint value = 0;

	CT_CHECK_PTR_AND_RETURN_RES(pValue, false);

	if (!consoleGetInteger(&value))
		return false;

	if (value < min || value > max)
	{
		printf(CT_ENDL "The number have to be between %d and %d" CT_ENDL, (int)min, (int)max);

		return false;
	}

	printf(CT_ENDL);
	*pValue = value;

	return true;
}

bool consoleGetHexa32(uint* const pValue) CT_NOEXCEPT
{
	uint value = 0;
	uint i;

	CT_CHECK_PTR_AND_RETURN_RES(pValue, false);

	for (i = 0; i < 8; i++)
	{
		const unsigned char key = (const unsigned char)getchar();

		putchar(key);
		if (key >= '0' && key <= '9')
		{
			value = (value * 16) + (key - '0');
		}
		else if (key >= 'A' && key <= 'F')
		{
			value = (value * 16) + (key - 'A' + 10);
		}
		else if (key >= 'a' && key <= 'f')
		{
			value = (value * 16) + (key - 'a' + 10);
		}
		else
		{
			printf(CT_ENDL "It is not a hexa character!" CT_ENDL);

			return false;
		}
	}

	printf(CT_ENDL);
	*pValue = value;

	return true;
}

CT_END_NAMESPACE
