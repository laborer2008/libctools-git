/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 * This file was taken from the at91lib_20100901_softpack_1_9_v_1_0_svn_v15011.zip file
 * (latest available at91lib from Atmel's site). Many things are not actual for new projects.
 * Because of this file was completely reworked. C++ support is added.
 * Now it requires C99/C++98 compalible compiler.
 */

/* ---------------------------------------------------------------------------------------------------------------------
 *         ATMEL Microcontroller Software Support
 * ---------------------------------------------------------------------------------------------------------------------
 * Copyright (c) 2008, Atmel Corporation
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer below.
 *
 * Atmel's name may not be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * DISCLAIMER: THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * ---------------------------------------------------------------------------------------------------------------------
 */

// ---------------------------------------------------------------------------------------------------------------------
// Headers
// ---------------------------------------------------------------------------------------------------------------------

#include "ctools/trace.h"

CT_BEGIN_EXTERNAL_HEADERS
#	include <stdarg.h>
#	include <stdlib.h>	// abort()
CT_END_EXTERNAL_HEADERS

#include "ctools/predef/attributes.h"
#include "ctools/pointer.h"
#include "ctools/unused.h"


CT_BEGIN_NAMESPACE

// ---------------------------------------------------------------------------------------------------------------------
// Internal variables
// ---------------------------------------------------------------------------------------------------------------------

/// Trace level can be set at applet initialization
#if !defined (CT_NOTRACE) && (CT_DYN_TRACES == 1)
uint traceLevel = CT_TRACE_LEVEL;
#endif

// ---------------------------------------------------------------------------------------------------------------------
// Local Functions
// ---------------------------------------------------------------------------------------------------------------------

void traceFatalAction(void) CT_NOEXCEPT
{
#if !defined (CT_NO_OS)
	abort();
#endif

	// Block further execution in any case
	while (CT_TRUE)
		;
}

CommonSystemErrorCode getSystemErrorCode(const enum SystemErrCode systemErrCode) CT_NOEXCEPT
{
	CommonSystemErrorCode errCode;

	/* *INDENT-OFF* */
	if (systemErrCode == secNo)
	{
		errCode = 0;
	}
	else
	{
#ifdef CT_OS_WINDOWS
		if (systemErrCode == secGetLastError)
			errCode = (CommonSystemErrorCode)GetLastError();
		else
#endif
			errCode = (CommonSystemErrorCode)errno;
	}
	/* *INDENT-ON* */

	return errCode;
}

char* getErrorMsg(const enum SystemErrCode systemErrCode, const CommonSystemErrorCode upErrorCode) CT_NOEXCEPT
{
	/*
	 * Here, in this function we can't use CT_TRACE_* functions with the same systemErrCode because of recursive call
	 */

	char* message;

	/* *INDENT-OFF* */
	if (systemErrCode == secNo)
	{
		message = CT_NULL;
	}
	else
	{
#ifdef CT_OS_WINDOWS
		if (systemErrCode == secGetLastError)
		{
			wchar_t* string = CT_NULL;
			char* ansiString = CT_NULL;

			message = CT_NULL;

			do
			{
				const size_t kAnsiStringSize = 100;
				int convertStatus;
				DWORD status;

				status = FormatMessage(
					FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,	// Flags
					CT_NULL,														// lpSource
					upErrorCode,													// dwMessageId
					MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),						// dwLanguageId
					(LPWSTR)&string,												// lpBuffer
					0,																// nSize
					CT_NULL															// Arguments
				);

				if ((status == 0) || string == CT_NULL)
				{
					CT_TRACE_ERRORN("FormatMessage() failed: GetLastError()=%d", GetLastError());
					break;
				}

				ansiString = (char*)malloc(kAnsiStringSize);

				if (!ansiString)
				{
					CT_TRACE_ERRORC("malloc() failed");
					break;
				}

				convertStatus = WideCharToMultiByte(
					CP_ACP,					// CodePage
					0,						// dwFlags
					string,					// lpWideCharStr
					-1,						// cchWideChar
					ansiString,				// lpMultiByteStr
					(int)kAnsiStringSize,	// cbMultiByte
					CT_NULL,				// lpDefaultChar
					CT_NULL					// lpUsedDefaultChar
				);

				if (convertStatus == 0)
					CT_TRACE_ERRORN("WideCharToMultiByte() failed: GetLastError()=%d, convertStatus=%d",
						GetLastError(), convertStatus
					);
				else
					message = ansiString;
			}
			while (CT_FALSE);

			if (string)
			{
				const HLOCAL kFreeStatus = LocalFree((HLOCAL)string);

				if (kFreeStatus != CT_NULL)
					CT_TRACE_ERRORN("LocalFree() failed: GetLastError()=%d", GetLastError());
			}

			if (!message && ansiString)
				free(ansiString);
		}
		else
#endif
			message = strerror(upErrorCode);
	}
	/* *INDENT-ON* */

	return message;
}

void freeErrorMsgBuf(const enum SystemErrCode systemErrCode, char* message) CT_NOEXCEPT
{
#ifdef CT_OS_WINDOWS
	if (systemErrCode == secGetLastError)
		free(message);
#else
	CT_UNUSED(systemErrCode);
	CT_UNUSED(message);
#endif
}

const char* getCurrentFile(const char* fileName) CT_NOEXCEPT
{
	/*
	 * Full file name path is not required in the traces.
	 * Relative path from the sources root will suffice.
	 * TODO: Here can be done some string truncation
	 */

	return fileName;
}

void streamedPrintf(FILE* stream, const char* format, ...) CT_NOEXCEPT
{
	va_list ap;

	va_start(ap, format);

#if defined (CT_COMPL_ARMCC) || defined (CT_COMPL_SDCC)

	CT_UNUSED(stream);

	/*
	 * There are linking troubles when fprintf, flush and other such functions are used
	 */
#	if defined (CT_TRACE_TERMINAL) || defined (CT_TRACE_SYSLOG_AND_TERMINAL)
	vprintf(format, ap);
#	else
	CT_UNUSED(format);
#	endif

#else
	// !(defined (CT_COMPL_ARMCC)|| defined (CT_COMPL_SDCC))

#	if defined (CT_TRACE_TERMINAL) || defined (CT_TRACE_SYSLOG_AND_TERMINAL)
	// Perphaps mutex is required here for multithreading logging

	vfprintf(stream, format, ap);
	fflush(stream);

#	else
	CT_UNUSED(stream);
#	endif

#endif

	va_end(ap);
}

void traceDumpFrame(const unsigned char* const pFrame, const uint size) CT_NOEXCEPT
{
	uint i;

	CT_TRACE_DEBUG(CT_ENDL);

	for (i = 0; i < size; i++)
	{
		CT_TRACE_DEBUG("%02X ", pFrame[i]);
	}

	CT_TRACE_DEBUG(CT_ENDL);
}

/**
 * Print char if printable. If not print a point
 * \param c char to
 */
static CT_FORCEINLINE void printChar(const unsigned char c) CT_NOEXCEPT
{
	if ((/*c >= 0x00 && */ c <= 0x1F) || (c >= 0xB0 && c <= 0xDF))
		printf(".");
	else
		printf("%c", c);
}

// TODO: Port this function to log support
void traceDumpMemory(unsigned char* pBuffer, const uint size, const uint address) CT_NOEXCEPT
{
	uint i, j;
	unsigned char* pTmp;

	for (i = 0; i < (size / 16); i++)
	{
		printf("0x%08X (%04x): ", address + (i * 16), (i * 16));
		pTmp = (unsigned char*)&pBuffer[i * 16];
		for (j = 0; j < 4; j++)
		{
			printf("%02X%02X%02X%02X ", pTmp[0], pTmp[1], pTmp[2], pTmp[3]);
			pTmp += 4;
		}

		pTmp = (unsigned char*)&pBuffer[i * 16];
		for (j = 0; j < 16; j++)
		{
			printChar(*pTmp++);
		}

		printf(CT_ENDL);
	}

	if ((size % 16) != 0)
	{
		uint lastLineStart;

		lastLineStart = size - (size % 16);
		printf("0x%08X: ", address + lastLineStart);

		for (j = lastLineStart; j < lastLineStart + 16; j++)
		{
			if ((j != lastLineStart) && (j % 4 == 0))
				printf(" ");
			if (j < size)
				printf("%02X", pBuffer[j]);
			else
				printf("  ");
		}

		printf(" ");
		for (j = lastLineStart; j < size; j++)
		{
			printChar(pBuffer[j]);
		}

		printf(CT_ENDL);
	}
}

enum CommonError initializeTraces(void) CT_NOEXCEPT
{
	// TODO
	return ceOk;
}

enum CommonError finalizeTraces(void) CT_NOEXCEPT
{
	streamedPrintf(stderr, CT_ENDL);
	streamedPrintf(stdout, CT_ENDL);

	// TODO
	return ceOk;
}

CT_END_NAMESPACE
