/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "ctools/os/sleep.h"

#include "ctools/predef/external_headers.h"

#if defined (CT_SLEEP_AVAILABLE)
#	if defined (CT_C11_THREADS_H_AVAILABLE)

CT_BEGIN_EXTERNAL_HEADERS
#		include <threads.h>
CT_END_EXTERNAL_HEADERS

#		include "ctools/std/stdbool.h"
#		include "ctools/std/sys/types.h"
#		include "ctools/time_consts.h"
#		include "ctools/trace.h"


CT_BEGIN_NAMESPACE

void msleep(const mseconds_t msec) CT_NOEXCEPT
{
	CT_USING_CT_NAMESPACE

	if (CT_LIKELY(msec > 0))
	{
		const uint32_t kRemained = msec % kMilliSecsInSec;

		struct timespec timeSpec;

		timeSpec.tv_sec = msec / kMilliSecsInSec;
		timeSpec.tv_nsec = kRemained * (kNanoSecsInMicroSec * kMicroSecsInMilliSec);

		struct timespec remained;
		bool needContinueSleeping;

		do
		{
			// CT_TRACE_DEBUG("thrd_sleep() will sleep for %u s and %u ms", timeSpec.tv_sec, timeSpec.tv_nsec);

			const int kResult = thrd_sleep(&timeSpec, &remained);
			const int kThrdSleepOk = 0;
			const int kThrdSleepInterrupted = -1;

			switch (kResult)
			{
				case kThrdSleepOk:
					needContinueSleeping = false;
					break;

				case kThrdSleepInterrupted:
					CT_TRACE_WARNING("thrd_sleep() was interrupted, continue sleep for %u s and %u ms",
						(uint)remained.tv_sec, (uint)remained.tv_nsec
					);
					timeSpec = remained;
					needContinueSleeping = true;
					break;

				default:
					CT_TRACE_ERRORC("thrd_sleep() failed, result = %d", kResult);
					needContinueSleeping = false;
			}
		}
		while (needContinueSleeping && ((remained.tv_sec > 0) || (remained.tv_nsec > 0)));
	}
}

CT_END_NAMESPACE

#	elif defined (CT_OS_UNIX)

CT_BEGIN_EXTERNAL_HEADERS

//		Define nanosleep and friends for C99 mode
#		ifndef __USE_POSIX199309
#			define __USE_POSIX199309
#		endif

#		include <time.h>
CT_END_EXTERNAL_HEADERS

#		include "ctools/std/stdbool.h"
#		include "ctools/std/sys/types.h"
#		include "ctools/time_consts.h"
#		include "ctools/trace.h"


CT_BEGIN_NAMESPACE

void msleep(const mseconds_t msec) CT_NOEXCEPT
{
	CT_USING_CT_NAMESPACE

	if (CT_LIKELY(msec > 0))
	{
		const uint32_t kRemained = msec % kMilliSecsInSec;

		struct timespec timeSpec;

		timeSpec.tv_sec = msec / kMilliSecsInSec;
		timeSpec.tv_nsec = kRemained * (kNanoSecsInMicroSec * kMicroSecsInMilliSec);

		struct timespec remained;
		bool needContinueSleeping;

		do
		{
			// CT_TRACE_DEBUG("nanosleep() will sleep for %u s and %u ms", timeSpec.tv_sec, timeSpec.tv_nsec);

			const int kResult = nanosleep(&timeSpec, &remained);
			const int kNanoSleepOk = 0;

			if (kResult != kNanoSleepOk)
			{
				const int kErrno = errno;

				CT_TRACE_ERRORC("nanosleep() failed, result = %d", kResult);

				needContinueSleeping = kErrno == EINTR;
				if (needContinueSleeping)
				{
					CT_TRACE_WARNING("nanosleep() was interrupted, continue sleep for %u s and %u ms",
						(uint)remained.tv_sec, (uint)remained.tv_nsec
					);
					timeSpec = remained;
				}
			}
			else
			{
				needContinueSleeping = false;
			}
		}
		while (needContinueSleeping && ((remained.tv_sec > 0) || (remained.tv_nsec > 0)));
	}
}

CT_END_NAMESPACE

#	elif defined (CT_OS_WINDOWS)

CT_BEGIN_EXTERNAL_HEADERS
#		include <windows.h>
CT_END_EXTERNAL_HEADERS

#		include "ctools/likely.h"


CT_BEGIN_NAMESPACE

void msleep(const mseconds_t msec) CT_NOEXCEPT
{
	if (CT_LIKELY(msec > 0))
	{
		mseconds_t sleepTime;

		if (CT_UNLIKELY(msec == INFINITE))
		{
			// Eternal sleep is not supported
			Sleep(1);
			sleepTime = INFINITE - 1;
		}
		else
		{
			sleepTime = msec;
		}

		Sleep((DWORD)sleepTime);
	}
}

CT_END_NAMESPACE

#	else
#		error Unimplemented at all
#	endif
#else
enum {getRidOfSdccIsoCforbidsAnEmptySourceFileWarning = 0};
#endif	// defined (CT_SLEEP_AVAILABLE)
