/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "ctools/os/mutex.h"

#include "ctools/predef/external_headers.h"

CT_BEGIN_EXTERNAL_HEADERS
//	malloc()
#	include <stdlib.h>
CT_END_EXTERNAL_HEADERS

#include "ctools/std/stdbool.h"
#include "ctools/pointer.h"
#include "ctools/trace.h"


CT_BEGIN_NAMESPACE

#if defined (CT_MUTEX_AVAILABLE)
#	if defined (CT_C11_THREADES_H_AVAILABLE)

MutexPointer createMutex(void) CT_NOEXCEPT
{
	MutexPointer mutexPointer;

	do
	{
		mutexPointer = (MutexPointer)malloc(sizeof(*mutexPointer));

		if (!mutexPointer)
		{
			CT_TRACE_ERRORC("malloc() failed");
			break;
		}

		if (mtx_init(mutexPointer, mtx_plain) != thrd_success)
		{
			CT_TRACE_ERRORC("mtx_init() failed");
			CT_FREE_CLEAR_PTR(mutexPointer);
		}
	}
	while (CT_FALSE);

	return mutexPointer;
}

void releaseMutex(MutexPointer* mutexDoublePointer) CT_NOEXCEPT
{
	CT_CHECK_PTR_AND_RETURN(mutexDoublePointer);
	CT_CHECK_PTR_AND_RETURN(*mutexDoublePointer);

	mtx_destroy(*mutexDoublePointer);
	free(*mutexDoublePointer);
	*mutexDoublePointer = CT_NULL;
}

void lockMutex(MutexPointer mutexPointer) CT_NOEXCEPT
{
	CT_CHECK_PTR_AND_RETURN(mutexPointer);

	if (mtx_lock(mutexPointer) != thrd_success)
		CT_TRACE_ERRORC("mtx_lock() failed");
}

void unLockMutex(MutexPointer mutexPointer) CT_NOEXCEPT
{
	CT_CHECK_PTR_AND_RETURN(mutexPointer);

	if (mtx_unlock(mutexPointer) != thrd_success)
		CT_TRACE_ERRORC("mtx_unlock() failed");
}

#	else
#		if defined (CT_COMPL_MSVC)

MutexPointer createMutex(void) CT_NOEXCEPT
{
	MutexPointer mutexPointer = CreateMutex(
		CT_NULL,	// Default security attributes
		FALSE,		// Initially not owned
		CT_NULL		// Unnamed mutex
	);

	if (mutexPointer == CT_NULL)
		CT_TRACE_ERRORW("CreateMutex() failed");

	return mutexPointer;
}

void releaseMutex(MutexPointer* mutexDoublePointer) CT_NOEXCEPT
{
	enum {kCloseHandleFail = 0};
	BOOL errCode;

	CT_CHECK_PTR_AND_RETURN(mutexDoublePointer);
	CT_CHECK_PTR_AND_RETURN(*mutexDoublePointer);

	errCode = CloseHandle(*mutexDoublePointer);
	if (errCode != kCloseHandleFail)
		*mutexDoublePointer = CT_NULL;	// CreateMutex() error
	else
		CT_TRACE_ERRORW("CloseHandle() failed: %d", (int)errCode);
}

void lockMutex(MutexPointer mutexPointer) CT_NOEXCEPT
{
	DWORD errCode;

	CT_CHECK_PTR_AND_RETURN(mutexPointer);

	errCode = WaitForSingleObject(mutexPointer, INFINITE);
	if (errCode != WAIT_OBJECT_0)
	{
		enum {kMessageLimit = 20};
		char message[kMessageLimit];

		switch (errCode)
		{
			case WAIT_ABANDONED:
				strncpy(message, "WAIT_ABANDONED", kMessageLimit);
				break;

			case WAIT_TIMEOUT:
				strncpy(message, "WAIT_TIMEOUT", kMessageLimit);
				break;

			case WAIT_FAILED:
				strncpy(message, "WAIT_FAILED", kMessageLimit);
				break;

			default:
				strncpy(message, "unknown", kMessageLimit);
		}

		CT_TRACE_ERRORW("WaitForSingleObject() failed (%s: %d)", message, errCode);
	}
}

void unLockMutex(MutexPointer mutexPointer) CT_NOEXCEPT
{
	enum {kReleaseMutexFail = 0};
	BOOL errCode;

	CT_CHECK_PTR_AND_RETURN(mutexPointer);

	errCode = ReleaseMutex(mutexPointer);

	if (errCode == kReleaseMutexFail)
		CT_TRACE_ERRORW("ReleaseMutex() failed");
}

#		elif defined (CT_COMPL_GCC_EMULATION)

MutexPointer createMutex(void) CT_NOEXCEPT
{
	MutexPointer mutexPointer;

	do
	{
		mutexPointer = (MutexPointer)malloc(sizeof(*mutexPointer));

		if (!mutexPointer)
		{
			CT_TRACE_ERRORC("malloc() failed");
			break;
		}

		const int kErrCode = pthread_mutex_init(mutexPointer, CT_NULL);
		const int kOk = 0;	// The function always return it(should)

		if (CT_UNLIKELY(kErrCode != kOk))
		{
			CT_TRACE_ERRORC("pthread_mutex_init() failed: %d", kErrCode);
			CT_FREE_CLEAR_PTR(mutexPointer);
		}
	}
	while (CT_FALSE);

	return mutexPointer;
}

void releaseMutex(MutexPointer* mutexDoublePointer) CT_NOEXCEPT
{
	CT_CHECK_PTR_AND_RETURN(mutexDoublePointer);
	CT_CHECK_PTR_AND_RETURN(*mutexDoublePointer);

	const int kErrCode = pthread_mutex_destroy(*mutexDoublePointer);
	const int kOk = 0;	// The function always return it(should)

	if (CT_UNLIKELY(kErrCode != kOk))
		CT_TRACE_ERRORC("pthread_mutex_destroy() failed: %d", kErrCode);

	free(*mutexDoublePointer);
	*mutexDoublePointer = CT_NULL;
}

void lockMutex(MutexPointer mutexPointer) CT_NOEXCEPT
{
	CT_CHECK_PTR_AND_RETURN(mutexPointer);

	const int kErrCode = pthread_mutex_lock(mutexPointer);
	const int kOk = 0;	// The function always return it(should)

	if (CT_UNLIKELY(kErrCode != kOk))
		CT_TRACE_ERRORC("pthread_mutex_lock() failed: %d", kErrCode);
}

void unLockMutex(MutexPointer mutexPointer) CT_NOEXCEPT
{
	CT_CHECK_PTR_AND_RETURN(mutexPointer);

	const int kErrCode = pthread_mutex_unlock(mutexPointer);
	const int kOk = 0;	// The function always return it(should)

	if (CT_UNLIKELY(kErrCode != kOk))
		CT_TRACE_ERRORC("pthread_mutex_unlock() failed: %d", kErrCode);
}

#		endif	// defined (CT_COMPL_MSVC)
#	endif	// defined (CT_C11_THREADES_H_AVAILABLE)
#endif	// defined (CT_MUTEX_AVAILABLE)

CT_END_NAMESPACE
