/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "ctools/os/timer.h"

#include "ctools/time_consts.h"

#if defined (CT_TIMER_AVAILABLE)

#	include "ctools/predef/external_headers.h"
#	include "ctools/likely.h"
#	include "ctools/pointer.h"

#	if defined (CT_OS_UNIX)

#		include "ctools/std/stdbool.h"


CT_BEGIN_NAMESPACE

void initTimer(RelativeTimePair* relativeTimePair) CT_NOEXCEPT
{
	CT_CHECK_PTR_AND_RETURN(relativeTimePair);

	relativeTimePair->start.tv_sec = 0;
	relativeTimePair->start.tv_nsec = 0;

	relativeTimePair->end.tv_sec = 0;
	relativeTimePair->end.tv_nsec = 0;
}

static void executeTimeApi(AbsoluteTimePoint* absoluteTimePoint) CT_NOEXCEPT
{
	CT_CHECK_PTR_AND_RETURN(absoluteTimePoint);

	/*
	 * Consider to replace CLOCK_MONOTONIC by CLOCK_MONOTONIC_RAW or CLOCK_BOOTTIME for Linux
	 * in the future (when 2.6.28 will be too old)
	 */

	const int kClockGetTimeResult = clock_gettime(CLOCK_MONOTONIC, absoluteTimePoint);
	const int kOk = 0;

	if (CT_UNLIKELY(kClockGetTimeResult != kOk))
	{
		CT_TRACE_ERRORC("clock_gettime() failed: %d", kClockGetTimeResult);
		absoluteTimePoint->tv_sec = 0;
		absoluteTimePoint->tv_nsec = 0;
	}
}

void startTimer(RelativeTimePair* relativeTimePair) CT_NOEXCEPT
{
	CT_CHECK_PTR_AND_RETURN(relativeTimePair);

	executeTimeApi(&relativeTimePair->start);
}

void stopTimer(RelativeTimePair* relativeTimePair) CT_NOEXCEPT
{
	CT_CHECK_PTR_AND_RETURN(relativeTimePair);

	executeTimeApi(&relativeTimePair->end);
}

static bool isRelativeTimePairCorrect(RelativeTimePair* relativeTimePair) CT_NOEXCEPT
{
	bool isCorrect = false;

	CT_CHECK_PTR_AND_RETURN_RES(relativeTimePair, isCorrect);

	do
	{
		const bool kIsNotCorrectStartTimePoint = ((relativeTimePair->start.tv_sec == 0)
			&& (relativeTimePair->start.tv_nsec == 0)
		);

		if (CT_UNLIKELY(kIsNotCorrectStartTimePoint))
		{
			CT_TRACE_ERRORN("relativeTimePair: incorrect start time");
			break;
		}

		const bool kIsNotCorrectEndTimePoint = ((relativeTimePair->end.tv_sec == 0)
			&& (relativeTimePair->end.tv_nsec == 0)
		);

		if (CT_UNLIKELY(kIsNotCorrectEndTimePoint))
		{
			CT_TRACE_ERRORN("relativeTimePair: incorrect end time");
			break;
		}

		const bool kIsNotCorrectStartNs = relativeTimePair->start.tv_nsec >= (long)CT_NANO_SECS_IN_SEC;

		if (CT_UNLIKELY(kIsNotCorrectStartNs))
		{
			CT_TRACE_ERRORN("relativeTimePair: incorrect nanoseconds field of start time: %u",
				(uint)relativeTimePair->start.tv_nsec
			);
			break;
		}

		const bool kIsNotCorrectEndNs = relativeTimePair->end.tv_nsec >= (long)CT_NANO_SECS_IN_SEC;

		if (CT_UNLIKELY(kIsNotCorrectEndNs))
		{
			CT_TRACE_ERRORN("relativeTimePair: incorrect nanoseconds field of end time: %u",
				(uint)relativeTimePair->end.tv_nsec
			);
			break;
		}

		if (CT_UNLIKELY(relativeTimePair->start.tv_sec > relativeTimePair->end.tv_sec))
		{
			CT_TRACE_ERRORN("relativeTimePair: start time cannot be larger than end time");
			break;
		}

		if (relativeTimePair->start.tv_sec == relativeTimePair->end.tv_sec)
		{
			if (CT_UNLIKELY(relativeTimePair->end.tv_nsec < relativeTimePair->start.tv_nsec))
			{
				CT_TRACE_ERRORN("relativeTimePair: start time(%u) cannot be larger than end time(%u)",
					(uint)relativeTimePair->start.tv_nsec, (uint)relativeTimePair->end.tv_nsec
				);
				break;
			}
		}

		isCorrect = true;
	}
	while (CT_FALSE);

	return isCorrect;
}

uint32_t getElapsedTimeInMilliSeconds(RelativeTimePair* relativeTimePair) CT_NOEXCEPT
{
	uint32_t difference = 0;

	CT_CHECK_PTR_AND_RETURN_RES(relativeTimePair, difference);

	if (isRelativeTimePairCorrect(relativeTimePair))
	{
		const long kNsDifference = relativeTimePair->end.tv_nsec - relativeTimePair->start.tv_nsec;
		const time_t kEnd = relativeTimePair->end.tv_sec - relativeTimePair->start.tv_sec;

		difference = (kEnd * kMilliSecsInSec) + (kNsDifference / (long)CT_NANO_SECS_IN_MILLI_SEC);

		// CT_TRACE_DEBUG("kNsDifference=%ld, difference=%u", kNsDifference, difference);
	}

	return difference;
}

uint32_t getElapsedTimeInSeconds(RelativeTimePair* relativeTimePair) CT_NOEXCEPT
{
	uint32_t difference = 0;

	CT_CHECK_PTR_AND_RETURN_RES(relativeTimePair, difference);

	// Don't check overflow here. It's practically unattainable(See module's comment)

	if (isRelativeTimePairCorrect(relativeTimePair))
		difference = relativeTimePair->end.tv_sec - relativeTimePair->start.tv_sec;

	return difference;
}

CT_END_NAMESPACE

#	elif defined (CT_OS_WINDOWS)

CT_BEGIN_EXTERNAL_HEADERS
#		include <windows.h>
CT_END_EXTERNAL_HEADERS


CT_BEGIN_NAMESPACE

void initTimer(RelativeTimePair* relativeTimePair) CT_NOEXCEPT
{
	CT_CHECK_PTR_AND_RETURN(relativeTimePair);

	relativeTimePair->start = 0;
	relativeTimePair->end = 0;
}

void startTimer(RelativeTimePair* relativeTimePair) CT_NOEXCEPT
{
	CT_CHECK_PTR_AND_RETURN(relativeTimePair);

	relativeTimePair->start = (uint32_t)GetTickCount();
}

void stopTimer(RelativeTimePair* relativeTimePair) CT_NOEXCEPT
{
	CT_CHECK_PTR_AND_RETURN(relativeTimePair);

	relativeTimePair->end = (uint32_t)GetTickCount();
}

uint32_t getElapsedTimeInMilliSeconds(RelativeTimePair* relativeTimePair) CT_NOEXCEPT
{
	uint32_t difference;

	CT_CHECK_PTR_AND_RETURN_RES(relativeTimePair, 0);

	if (CT_LIKELY(relativeTimePair->start <= relativeTimePair->end))
	{
		difference = relativeTimePair->end - relativeTimePair->start;
	}
	else
	{
		// relativeTimePair->start can be from 1 to UINT32_MAX.
		difference = ((UINT32_MAX - relativeTimePair->start) + 1) + relativeTimePair->end;
	}

	return difference;
}

uint32_t getElapsedTimeInSeconds(RelativeTimePair* relativeTimePair) CT_NOEXCEPT
{
	CT_CHECK_PTR_AND_RETURN_RES(relativeTimePair, 0);

	return getElapsedTimeInMilliSeconds(relativeTimePair) / kMilliSecsInSec;
}

CT_END_NAMESPACE

#	else
#		error Unimplemented at all
#	endif
#else
enum {getRidOfSdccIsoCforbidsAnEmptySourceFileWarning = 0};
#endif	// defined (CT_TIMER_AVAILABLE)
