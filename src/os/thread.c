/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "ctools/os/thread.h"

#include "ctools/std/stdbool.h"
#include "ctools/pointer.h"
#include "ctools/trace.h"


CT_BEGIN_NAMESPACE

#if defined (CT_KILL_THREAD_AVAILABLE)
#	if defined (CT_COMPL_MSVC)

bool killThread(ThreadPointer* threadDoublePointer) CT_NOEXCEPT
{
	BOOL errCode;
	bool status;

	CT_CHECK_PTR_AND_RETURN_RES(threadDoublePointer, false);

	// Check zero handle here. Seems values like IVALID_HANDLE_VALUE don't make sense here
	CT_CHECK_PTR_AND_RETURN_RES(*threadDoublePointer, false);

	errCode = TerminateThread(*threadDoublePointer, kKilledThreadExitCode);
	status = errCode != 0;

	if (status)
	{
		CT_TRACE_WARNING("Thread 0x%p was killed", *threadDoublePointer);
		*threadDoublePointer = CT_NULL;	// CreateThread() error
	}
	else
	{
		CT_TRACE_ERRORW("CloseHandle() failed: %d", (int)errCode);
	}

	return status;
}

#	elif defined (CT_COMPL_GCC_EMULATION)

bool killThread(ThreadPointer* threadDoublePointer) CT_NOEXCEPT
{
	CT_CHECK_PTR_AND_RETURN_RES(threadDoublePointer, false);
	CT_CHECK_PTR_AND_RETURN_RES(*threadDoublePointer, false);

	const int kErrCode = pthread_cancel(**threadDoublePointer);
	const bool kStatus = kErrCode == 0;

	if (kStatus)
	{
		CT_TRACE_WARNING("Thread %lu was killed", (long unsigned int)**threadDoublePointer);
		**threadDoublePointer = 0;
		*threadDoublePointer = CT_NULL;
	}
	else
	{
		CT_TRACE_ERRORC("pthread_cancel() failed: %d", kErrCode);
	}

	return kStatus;
}

#	else
#		error Unimplemented at all
#	endif	// defined (CT_COMPL_MSVC)
#endif	// defined (CT_KILL_THREAD_AVAILABLE)

CT_END_NAMESPACE
