/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "ctools/os/io.h"

#if !defined (CT_NO_OS)

#	include "ctools/predef/compiler.h"

#	if defined (CT_OS_WINDOWS) && defined (CT_COMPL_MINGW)
//		For _MAX_PATH
#		undef __STRICT_ANSI__
#		include <stdlib.h>
#	endif

#	include "ctools/std/string.h"

//	fopen(), fseek(), ftell(), fclose(). string.h must be before it
#	include "ctools/std/stdio.h"

#	include "ctools/std/sys/stat.h"
#	include "ctools/std/inttypes.h"
#	include "ctools/bool_consts.h"
#	include "ctools/pointer.h"
#	include "ctools/trace.h"
#	include "ctools/unused.h"

#	if defined (CT_OS_UNIX)

CT_BEGIN_EXTERNAL_HEADERS
//		PATH_MAX
#		include <linux/limits.h>
CT_END_EXTERNAL_HEADERS


CT_BEGIN_NAMESPACE

/*
 * We could include stdlib.h for realpath() but on some systems(i.e. Ubuntu 14.04)
 * stdlib.h(->features.h) undefines everything
 */
extern char* realpath(const char* __restrict __name, char* __restrict __resolved) __THROW __wur;

char* getAbsolutePath(const char* const path, char* resolvedPath, const size_t bufferSize) CT_NOEXCEPT
{
	char* kAbsPath = CT_NULL;

	do
	{
		if (resolvedPath && (bufferSize < PATH_MAX))
		{
			// Also that recommends glibc 2.13 and higher
			CT_TRACE_ERRORN("Invalid buffer size: got %zu, must be at least %zu bytes", bufferSize, (size_t)PATH_MAX);
			break;
		}

		if (!isFileExists(path))
		{
			// TODO: Find out it for directories
			CT_TRACE_WARNING("path \"%s\" is not exists", path);
		}

		kAbsPath = realpath(path, resolvedPath);

		if (kAbsPath == CT_NULL)
			CT_TRACE_ERRORC("realpth() failed");
	}
	while (false);

	return kAbsPath;
}

CT_END_NAMESPACE

#	elif defined (CT_OS_WINDOWS)

CT_BEGIN_EXTERNAL_HEADERS
//		_fullpath()
#		include <windows.h>
CT_END_EXTERNAL_HEADERS


CT_BEGIN_NAMESPACE

char* getAbsolutePath(const char* const path, char* resolvedPath, const size_t bufferSize) CT_NOEXCEPT
{
	/*
	 * We here use _fullpath().
	 * Alternatives: GetFullPathName() and  GetFullPathNameTransacted()
	 * according to documentation more limited:
	 * The GetFullPathName function is not recommended for multithreaded applications or shared library code.
	 * TxF may not be available in future versions of Microsoft Windows.
	 */

	char* kAbsPath = CT_NULL;

	do
	{
		if (resolvedPath && (bufferSize < _MAX_PATH))
		{
			CT_TRACE_ERRORN("Invalid buffer size: got %zu, must be at least %zu bytes", bufferSize, (size_t)_MAX_PATH);
			break;
		}

		kAbsPath = _fullpath(resolvedPath, path, bufferSize);

		if (kAbsPath == CT_NULL)
			CT_TRACE_ERRORC("_fullpath() failed");
	}
	while (CT_FALSE);

	return kAbsPath;
}

CT_END_NAMESPACE

#	else
#		error Unimplemented at all
#	endif

CT_BEGIN_NAMESPACE

ssize_t getFileSize(const char* const name) CT_NOEXCEPT
{
	/*
	 * There is an alternative way to get size of the file using stat().
	 * See: http://stackoverflow.com/questions/238603/how-can-i-get-a-files-size-in-c
	 */
	ssize_t fileSize = -1;
	FILE* file = CT_NULL;
	int closeStatus;

	do
	{
		CT_CHECK_PTR_AND_BREAK(name);

		file = fopen(name, "br");
		if (!file)
		{
			CT_TRACE_ERRORC("fopen() failed");
			break;
		}

		if (fseek(file, 0, SEEK_END) == -1)
		{
			CT_TRACE_ERRORC("fseek() failed");
			break;
		}

		fileSize = (ssize_t)ftell(file);
		CT_TRACE_ERRORC_IF(fileSize == -1, "ftell() failed");
	}
	while (CT_FALSE);

	closeStatus = fcloseWrapper(file);

	// Shut up release warning
	CT_UNUSED(closeStatus);

	return fileSize;
}

bool isFileExists(const char* const name) CT_NOEXCEPT
{
	struct stat buffer;

	return stat(name, &buffer) == 0;
}

static CT_FORCEINLINE char* copyPathString(const char* const path) CT_NOEXCEPT
{
#	ifdef CT_OS_WINDOWS
	const size_t kLimit = _MAX_PATH;
#	elif defined CT_OS_UNIX
	const size_t kLimit = PATH_MAX;
#	else
#		error Unimplemented at all
#	endif

	CT_USING_CT_NAMESPACE

	char* kPathCopy = strndup(path, kLimit);

	return kPathCopy;
}

bool createDir(const char* const path, const bool informAboutError) CT_NOEXCEPT
{
	bool status = true;

	do
	{
		CT_USING_CT_NAMESPACE

#	ifdef CT_OS_UNIX
		// Access rights are unlimited
		const mode_t kMode = S_IRWXU | S_IRWXG | S_IRWXO;
#	else
		const mode_t kMode = 0;
#	endif

		CT_CHECK_PTR_AND_BREAK(path);

		status = mkDirWrapper(path, kMode, informAboutError) != 0;

		if (status)
		{
			if (errno == EEXIST)
			{
				/*
				 * There is a bug for mingw:
				 * http://sourceforge.net/tracker/?func=detail&atid=983356&aid=3124373&group_id=202880
				 * Perphaps visual c++ also affected.
				 * Chop the last character from string anyway
				 */

				char* const pathCopy = copyPathString(path);
				const size_t kPathLen = ctStrLen(pathCopy);
				struct stat info;

				CT_TRACE_INFO("Directory (or file) \"%s\" already exists, not created", path);

				if (CT_LIKELY((kPathLen > 0) && pathCopy))
					if (pathCopy[kPathLen - 1] == kDirSeparator)
						pathCopy[kPathLen - 1] = 0;

				status = statWrapper(pathCopy, &info) != kStatOkResult;
				if (!status)
				{
					if (!(info.st_mode & S_IFDIR))
					{
						errno = EEXIST;
						status = true;
					}
				}

				free(pathCopy);
			}
		}
	}
	while (CT_FALSE);

	return status;
}

bool createPathDirs(const char* const path, const bool informAboutError) CT_NOEXCEPT
{
	bool status = true;

	char* copyPath = copyPathString(path);
	const size_t kPathLength = ctStrLen(copyPath);
	size_t i = 0;

#	ifdef CT_OS_WINDOWS
	// TODO: test it
	const char kPathBegining[] = ":\\";

	if (strncmp(path + 1, kPathBegining, sizeof(kPathBegining)) == 0)
		i += (1 + sizeof(kPathBegining));
#	elif defined CT_OS_UNIX
	i++;
#	else
#		error Unimplemented at all
#	endif

	CT_CHECK_PTR_AND_RETURN_RES(copyPath, status);

	while (i < kPathLength)
	{
		if (copyPath[i] == kDirSeparator)
		{
			copyPath[i] = 0;
			status = createDir(copyPath, informAboutError);
			if (status)
				break;

			copyPath[i] = kDirSeparator;
		}

		i++;
	}

	if (i >= kPathLength)
	{
		// Loop correctly finished. Create the tail
		status = createDir(path, informAboutError);
	}

	free(copyPath);

	return status;
}

bool isBufferContainsAsciiText(const char* const buffer, const size_t bufferSize) CT_NOEXCEPT
{
	bool status = true;
	size_t i;

	if (bufferSize)
		CT_CHECK_PTR_AND_RETURN_RES(buffer, status);

	for (i = 0; i < bufferSize; i++)
	{
		if ((buffer[i] < 9) || ((buffer[i] > 13) && (buffer[i] < 32)))
		{
			status = false;
			break;
		}
	}

	return status;
}

enum AsciiTest isFileContainsAsciiText(const char* const path) CT_NOEXCEPT
{
	enum AsciiTest status;
	FILE* file;

	file = fopenWrapper(path, "rb");
	if (file)
	{
		enum {kBufferSize = 4096};	// Memory page
		int closeStatus;
		uint8_t buffer[kBufferSize];

		status = atFileAscii;
		do
		{
			size_t read = fread(buffer, sizeof(buffer[0]), kBufferSize, file);

			if (!isBufferContainsAsciiText((const char* const)buffer, read))
				status = atFileBinary;

			if (status != atFileAscii)
				break;

			if (feof(file))
				break;

			if (ferror(file))
			{
				status = atErrorHappened;
				break;
			}
		}
		while (CT_TRUE);

		closeStatus = fcloseWrapper(file);

		// Shut up release warning
		CT_UNUSED(closeStatus);
	}
	else
	{
		status = atErrorHappened;
	}

	return status;
}

CT_END_NAMESPACE

#else
enum {getRidOfSdccIsoCforbidsAnEmptySourceFileWarning = 0};
#endif	// !defined (CT_NO_OS)
