/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "ctools/os/yield.h"


#if defined (CT_YIELD_AVAILABLE)

#	include "ctools/predef/external_headers.h"


#	if defined (CT_C11_THREADS_H_AVAILABLE)

CT_BEGIN_EXTERNAL_HEADERS
#		include <threads.h>
CT_END_EXTERNAL_HEADERS


CT_BEGIN_NAMESPACE

void yield(void) CT_NOEXCEPT
{
	thrd_yield();
}

CT_END_NAMESPACE

#	elif defined (CT_OS_UNIX)

CT_BEGIN_EXTERNAL_HEADERS
#			include <sched.h>
CT_END_EXTERNAL_HEADERS

#		if defined (CT_OS_LINUX)
#			include "ctools/likely.h"
#		endif

#		include "ctools/trace.h"


CT_BEGIN_NAMESPACE

void yield(void) CT_NOEXCEPT
{
	const int kYieldStatus = sched_yield();
	const int kYieldSuccessStatus = 0;

#		if defined (CT_OS_LINUX)
	// In the Linux implementation, sched_yield() always succeeds
	if (CT_UNLIKELY(kYieldStatus != kYieldSuccessStatus))
#		else
	if (kYieldStatus != kYieldSuccessStatus)
#		endif
		CT_TRACE_ERRORC("sched_yield() failed: %d", kYieldStatus);
}

CT_END_NAMESPACE

#	elif defined (CT_OS_WINDOWS)

CT_BEGIN_EXTERNAL_HEADERS
#		include <windows.h>
CT_END_EXTERNAL_HEADERS


CT_BEGIN_NAMESPACE

void yield(void) CT_NOEXCEPT
{
	Sleep(0);
}

CT_END_NAMESPACE

#	else
#		error Unimplemented at all
#	endif
#else
enum {getRidOfSdccIsoCforbidsAnEmptySourceFileWarning = 0};
#endif	// defined (CT_YIELD_AVAILABLE)
