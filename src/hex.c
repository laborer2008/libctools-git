/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "ctools/hex.h"

#include "ctools/likely.h"
#include "ctools/pointer.h"
#include "ctools/trace.h"


CT_BEGIN_NAMESPACE

uint8_t hexCharacterToUint(const char character) CT_NOEXCEPT
{
	uint8_t intValue;

	if ((character >= '0') && (character <= '9'))
		intValue = (uint8_t)(character - '0');
	else if ((character >= 'A') && (character <= 'F'))
		intValue = (uint8_t)(character - 'A' + 10);
	else if (CT_LIKELY((character >= 'a') && (character <= 'f')))
		intValue = (uint8_t)(character - 'a' + 10);
	else
		intValue = kInvalidHexCharacter;

	return intValue;
}

uint16_t hexPairToByte(const char lsb, const char msb) CT_NOEXCEPT
{
	uint16_t byteValue;
	const uint8_t kLsbHex = hexCharacterToUint(lsb);

	if (CT_LIKELY(kLsbHex != kInvalidHexCharacter))
	{
		const uint8_t kMsbHex = hexCharacterToUint(msb);

		if (CT_LIKELY(kMsbHex != kInvalidHexCharacter))
			byteValue = kLsbHex + (kMsbHex * 16);
		else
			byteValue = kInvalidHexPair;
	}
	else
	{
		byteValue = kInvalidHexPair;
	}

	return byteValue;
}

// See: http://stackoverflow.com/a/3409211/4155055 [2016/01/18]
bool hexStringToArray(uint8_t* array, const char* const hexString, const size_t hexStringSizeInBytes) CT_NOEXCEPT
{
	bool isHexStringNotCorrect = false;

	CT_CHECK_PTR_AND_RETURN_RES(array, true);
	CT_CHECK_PTR_AND_RETURN_RES(hexString, true);

	do
	{
		const size_t kArraySize = hexStringSizeInBytes / 2;
		size_t i;

		if (CT_UNLIKELY(hexStringSizeInBytes == 0))
		{
			isHexStringNotCorrect = true;
			break;
		}

		if (CT_UNLIKELY(kArraySize * 2 != hexStringSizeInBytes))
		{
			isHexStringNotCorrect = true;
			break;
		}

		for (i = 0; i < kArraySize; i++)
		{
			const uint16_t kHexByte = hexPairToByte(hexString[2 * i + 1], hexString[2 * i]);

			if (CT_UNLIKELY(kHexByte == kInvalidHexPair))
			{
				isHexStringNotCorrect = true;
				break;
			}

			array[i] = (uint8_t)(kHexByte & 0xFF);
		}
	}
	while (CT_FALSE);

	return isHexStringNotCorrect;
}

CT_END_NAMESPACE
