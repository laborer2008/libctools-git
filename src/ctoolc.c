/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "ctools/ctools.h"

#include "ctools/trace.h"


CT_BEGIN_NAMESPACE

#if defined (CT_OS_WINDOWS)
/*
 * Windows has 2 code pages: system and console. It complicates everything.
 * This function enforces system code page also for the console.
 * But appropriate font (Lucida Console) should be choosen for the console
 * Note: SetConsoleCP()/SetConsoleOutputCP() functions may conflict with setlocale()
 */
static void setConsoleCp(void) CT_NOEXCEPT
{
	const UINT kCodePage = GetACP();

	SetConsoleCP(kCodePage);
	SetConsoleOutputCP(kCodePage);
}
#endif	// defined (CT_OS_WINDOWS)

enum CommonError initializeCtools(void) CT_NOEXCEPT
{
	enum CommonError initializationStatus;

	do
	{
		initializationStatus = initializeTraces();
		if (initializationStatus != ceOk)
			break;

#ifdef CT_OS_WINDOWS
		setConsoleCp();
#endif
	}
	while (CT_FALSE);

	return initializationStatus;
}

enum CommonError finalizeCtools(void) CT_NOEXCEPT
{
	enum CommonError finalizationStatus;

	do
	{
		finalizationStatus = finalizeTraces();
		if (finalizationStatus != ceOk)
			break;

		// TODO
	}
	while (CT_FALSE);

	return finalizationStatus;
}

CT_END_NAMESPACE
