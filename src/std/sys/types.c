/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "ctools/std/sys/types.h"

#if defined (CT_OS_LINUX)

// unistd.h from glibc 2.20 wants it
#	include "ctools/std/stdint.h"

CT_BEGIN_EXTERNAL_HEADERS
#		include <sys/syscall.h>

// Activate syscall()
#		if !defined (__USE_MISC)
#			define __USE_MISC
#		endif

#		include <unistd.h>
CT_END_EXTERNAL_HEADERS

#	ifdef CT_OS_ANDROID
#		ifndef SYS_gettid
// Too old NDK's
#			define SYS_gettid __NR_gettid
#		endif
#	endif

#	include "ctools/trace.h"
#endif	// defined (CT_OS_LINUX)

#if defined (CT_OS_LINUX)


CT_BEGIN_NAMESPACE

pid_t gettid(void) CT_NOEXCEPT
{
	const long kSyscallStatus = syscall(SYS_gettid);
	const long kSyscallErrorStatus = -1;

	/*
	 * man syscall:
	 * RETURN VALUE
	 * The  return  value is defined by the system call being invoked.  In general, a 0 return value indicates success.
	 * A -1 return value indicates an error, and an error code is stored in errno.
	 *
	 * man gettid:
	 * RETURN VALUE
	 * On success, returns the thread ID of the calling process.
	 * ERRORS
	 * This call is always successful.
	 */

	if (kSyscallStatus == kSyscallErrorStatus)
		CT_TRACE_ERRORC("syscall() failed: %dl", kSyscallStatus);

	const pid_t kTid = (pid_t)kSyscallStatus;

	return kTid;
}

CT_END_NAMESPACE

#endif	// defined (CT_OS_LINUX)
