/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "ctools/std/sys/stat.h"

#if !defined (CT_NO_OS)

#	include "ctools/trace.h"

#	ifdef CT_OS_WINDOWS

CT_BEGIN_EXTERNAL_HEADERS
//		_mkdir()
#		include <direct.h>

//		_chmod()
#		include <io.h>
CT_END_EXTERNAL_HEADERS

#	endif


CT_BEGIN_NAMESPACE

int chmodWrapper(const char* path, mode_t mode) CT_NOEXCEPT
{
#	ifdef CT_OS_WINDOWS
#		define chmod(name, mode)	_chmod(name, mode)
#	endif

	int kChmodResult = chmod(path, mode);

#	ifdef CT_OS_WINDOWS
#		undef chmod
#	endif

	if (kChmodResult != kChModOkResult)
		CT_TRACE_ERRORC("chmod() failed (%d) for path \"%s\"", kChmodResult, path);

	return kChmodResult;
}

int mkDirWrapper(const char* path, mode_t mode, const bool informAboutError) CT_NOEXCEPT
{
#	ifdef CT_OS_WINDOWS
#		define mkdir(name, mode)	_mkdir(name)
#	endif

	int kMkDirResult = mkdir(path, mode);

#	ifdef CT_OS_WINDOWS
#		undef mkdir
#	endif

	/* *INDENT-OFF* */
	if (kMkDirResult == kMkDirOkResult)
		CT_TRACE_INFO("Created directory: \"%s\" with mode %u", path, (uint)mode);
	else
	{
		if (informAboutError)
			CT_TRACE_ERRORC("mkdir() failed (%d) for path \"%s\" and mode %u ", kMkDirResult, path, (uint)mode);
	}
	/* *INDENT-ON* */

	return kMkDirResult;
}

int statWrapper(const char* CT_RESTRICT path, struct stat* CT_RESTRICT buf) CT_NOEXCEPT
{
	int kStatResult = stat(path, buf);

	if (kStatResult != kStatOkResult)
		CT_TRACE_ERRORC("stat() failed (%d) for path \"%s\"", kStatResult, path);

	return kStatResult;
}

CT_END_NAMESPACE

#else
enum {getRidOfSdccIsoCforbidsAnEmptySourceFileWarning = 0};
#endif	// !defined (CT_NO_OS)
