/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "ctools/std/stdio.h"

#include "ctools/trace.h"

#if defined (CT_COMPL_SDCC)
#	include "ctools/pointer.h"
#endif


#if defined (CT_RETARGET_PUTCHAR)

#	include "ctools/unused.h"

#	if defined (CT_COMPL_SDCC)
// Prototype here is not standard
void putchar(char c)
{
	CT_UNUSED(c);
}
#	else
int putchar(int c)
{
	CT_UNUSED(c);

	return EOF;
}
#	endif
#endif	// defined (CT_RETARGET_PUTCHAR)

#if defined (CT_COMPL_SDCC)
// sdcc don't use these values. Real values are not required
FILE* stdout = CT_NULL;
FILE* stderr = CT_NULL;

FILE* stdin = CT_NULL;
#endif


#if !defined (CT_NO_OS)

CT_BEGIN_NAMESPACE

FILE* fopenWrapper(const char* path, const char* mode) CT_NOEXCEPT
{
	FILE* fopenResult;

	fopenResult = fopen(path, mode);
	if (!fopenResult)
		CT_TRACE_ERRORC("fopen() for \"%s\" with mode=\"%s\" failed", path, mode);

	return fopenResult;
}

int fcloseWrapper(FILE* fp) CT_NOEXCEPT
{
	int fcloseResult;

	fcloseResult = fclose(fp);
	if (fcloseResult != kFcloseOkResult)
		CT_TRACE_ERRORC("fclose() failed: %d", fcloseResult);

	return fcloseResult;
}

int removeWrapper(const char* pathName, const bool informAboutError) CT_NOEXCEPT
{
	int removeResult;

	/* *INDENT-OFF* */
	removeResult = remove(pathName);
	if (removeResult == kRemoveOkResult)
		CT_TRACE_INFO("Removed \"%s\" successfully", pathName);
	else
	{
		if (informAboutError)
			CT_TRACE_ERRORC("Failed to remove file (directory) \"%s\": %d", pathName, removeResult);
	}
	/* *INDENT-ON* */

	return removeResult;
}

int renameWrapper(const char* oldName, const char* newName) CT_NOEXCEPT
{
	int renameResult;

	renameResult = rename(oldName, newName);
	if (renameResult == kRenameOkResult)
		CT_TRACE_INFO("Renamed \"%s\" to \"%s\"", oldName, newName);
	else
		CT_TRACE_ERRORC("Failed to rename file (directory) \"%s\" to \"%s\": %d", oldName, newName, renameResult);

	return renameResult;
}

CT_END_NAMESPACE

#endif	// !defined (CT_NO_OS)
