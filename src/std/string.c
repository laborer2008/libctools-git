/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "ctools/std/string.h"

#if defined (CT_COMPL_SDCC)

#include "ctools/pointer.h"
#include "ctools/unused.h"


char* strerror(int errnum) CT_NOEXCEPT
{
	// Unsupported by library.

	CT_UNUSED(errnum);

	return CT_NULL;
}
#elif defined (CT_OS_WINDOWS)

CT_BEGIN_EXTERNAL_HEADERS
#	include <stdlib.h>
CT_END_EXTERNAL_HEADERS

#include "ctools/min_max.h"
#include "ctools/pointer.h"
#include "ctools/trace.h"


CT_BEGIN_NAMESPACE

// See also http://research.microsoft.com/en-us/um/redmond/projects/invisible/src/crt/strndup.c.htm

char* strndup(const char* s, size_t n) CT_NOEXCEPT
{
	const size_t kLen = strlen(s);
	char* newString = CT_NULL;

	if (kLen)
	{
		const size_t kFullLen = kLen + 1;
		const size_t kAllocSize = CT_MIN2(kFullLen, n);

		if (kAllocSize)
		{
			newString = (char*)malloc(kAllocSize);
			if (newString)
			{
				strncpy(newString, s, kAllocSize);
				newString[kAllocSize - 1] = 0;
			}
			else
			{
				CT_TRACE_ERRORC("malloc() failed");
			}
		}
	}

	return newString;
}

CT_END_NAMESPACE

#endif
