/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "ctools/std/unistd.h"

#if !defined (CT_NO_OS)

#	ifdef CT_OS_WINDOWS

#		include "ctools/predef/external_headers.h"

CT_BEGIN_EXTERNAL_HEADERS
//		_rmdir()
#		include <direct.h>
CT_END_EXTERNAL_HEADERS

#	endif

#	include "ctools/trace.h"


CT_BEGIN_NAMESPACE

int rmdirWrapper(const char* path) CT_NOEXCEPT
{
	int rmDirResult;

#	ifdef CT_OS_WINDOWS
#		define rmdir(path)	_rmdir(path)
#	endif

	rmDirResult = rmdir(path);

#	ifdef CT_OS_WINDOWS
#		undef rmdir
#	endif

	if (rmDirResult == kRmDirOkStatus)
		CT_TRACE_INFO("Succesfully removed unused directory \"%s\"", path);
	else
		CT_TRACE_ERRORC("Failed to remove unused directory \"%s\"", path);

	return rmDirResult;
}

#	ifdef CT_OS_UNIX

int symlinkWrapper(const char* path1, const char* path2) CT_NOEXCEPT
{
	const int kSymlinkResult = symlink(path1, path2);

	if (kSymlinkResult != kSymlinkOkStatus)
		CT_TRACE_ERRORC("Failed to call symlink(): %d", kSymlinkResult);

	return kSymlinkResult;
}

#	endif

CT_END_NAMESPACE

#else
enum {getRidOfSdccIsoCforbidsAnEmptySourceFileWarning = 0};
#endif	// !defined (CT_NO_OS)
