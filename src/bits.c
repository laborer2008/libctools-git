/*
 * @author Sergey Gusarov <laborer2008 (at) gmail.com>
 * @section LICENSE
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * @section DESCRIPTION
 *
 */

#include "ctools/bits.h"

#include "ctools/predef/compiler.h"

#if defined (CT_COMPL_GCC_EMULATION)
/*
TODO: See comment below

#elif defined (CT_COMPL_MSVC)
#	include "ctools/predef/external_headers.h"

CT_BEGIN_EXTERNAL_HEADERS
#		include <intrin.h>
CT_END_EXTERNAL_HEADERS
*/
#else

#	include "ctools/predef/external_headers.h"

CT_BEGIN_EXTERNAL_HEADERS
#		include <limits.h>
CT_END_EXTERNAL_HEADERS

#endif


CT_BEGIN_NAMESPACE

int popCount(const uint x) CT_NOEXCEPT
{
	int count;

	/*
	 * TODO: For MSVC we can use
	 * count = __popcnt(x);
	 * TODO: This function generate popcnt instruction (SSE4) that may not be implemented in some cpu
	 * See also: http://comments.gmane.org/gmane.comp.compilers.clang.devel/18531
	 */

#if defined (CT_COMPL_GCC_EMULATION)
	count = __builtin_popcount(x);
#else
	int i;

	count = 0;
	for (i = 0; i < sizeof(x) * CHAR_BIT; i++)
		if ((x >> i) & 1UL)
			count++;
#endif

	return count;
}

CT_END_NAMESPACE
