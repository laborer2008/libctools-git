#!/usr/bin/env python3

"""This script quickly creates a backup archive with proper name."""


__author__ = 'Sergey Gusarov'
__license__ = 'MPL 2.0, see LICENSE'


import os
import subprocess
import sys

sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'pyrepo'))
import common_pack
import fs


def main():
	subprocess.check_call(os.path.join(fs.getScriptDir(), 'scripts', 'cleanall.py'), shell = True)
	common_pack.pack()

main()
